# Smarteye - Wayfinding

Clone project terlebih ke direktori htdocs jika menggunakan XAMPP untuk server local (untuk server local yang lain bisa menyesuaikan)

Install [composer](https://getcomposer.org/download/)  terlebih dahulu (jika sudah, langkah selanjutnya)


Buka terminal, lalu arahkan ke root directory dari project

Kemudian jalankan command berikut

```composer install```

Setelah command diatas selesai

Buat file baru pada root directory dengan nama ```.env``` dan copy isi dari file ```.env.example``` ke dalam file ```.env```

Kemudian jalankan command

```php artisan key:generate```

>note : untuk sistem operasi berbasis linux, biasanya akan ada permissions error, tinggal lakukan chmod untuk direktori yang bersangkutan.

Setelah berhasil melakukan command diatas, jalankan command berikut

```php artisan migrate:fresh --seed```

jika semua command berhasil, langkah terakhir Anda bisa melihat project

>note : untuk xampp, harus membuat virtual host sendiri
dengan cara berikut

Buka file ```C:\Windows\System32\drivers\etc\hosts``` kemudian isikan
```127.0.0.2       dev-smarteye.com```

kemudian simpan (run as administrator)

Kemudian buka file ```C:\xampp\apache\conf\extra\httpd-vhosts.conf``` kemudian isikan 

```<VirtualHost 127.0.0.1:80>
    DocumentRoot "C:/xampp/htdocs/projects/smarteye/public"
    DirectoryIndex index.php      
    <Directory "C:/xampp/htdocs/projects/smarteye/public">
        Options All
        AllowOverride All
        Order Allow,Deny
        Allow from all
    </Directory>
</VirtualHost>
```

kemudian simpan

restart apache, kemudian buka browser dan buka halaman ```http://dev-smarteye.com/admin```

