<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePoi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('poi', function (Blueprint $table) {
            $table->increments('id');

            $table->string('airportcode', 10);
            $table->string('name', 100);
            $table->integer('categoryid');
            $table->integer('areaid');
            $table->integer('floorid');
            $table->integer('priorityid');
            $table->integer('iconid');
            $table->string('x');
            $table->string('y');
            $table->string('z');

            $table->foreign('airportcode')->references('airportcode')->on('airport')->onDelete('cascade');
            $table->foreign('categoryid')->references('id')->on('category')->onDelete('cascade');
            $table->foreign('areaid')->references('id')->on('area')->onDelete('cascade');
            $table->foreign('floorid')->references('id')->on('floor')->onDelete('cascade');
            $table->foreign('priorityid')->references('id')->on('priority')->onDelete('cascade');
            $table->foreign('iconid')->references('id')->on('icon')->onDelete('cascade');

            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('poi');
    }
}
