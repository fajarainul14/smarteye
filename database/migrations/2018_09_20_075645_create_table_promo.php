<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePromo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promo', function (Blueprint $table) {
            $table->increments('id');

            $table->string('airportcode', 10);
            $table->string('promoname');
            $table->string('promotext');
            $table->string('promoimage');
            $table->integer('floorid');
            $table->integer('iconid');
            $table->boolean('showicon')->default(false);
            $table->string('x');
            $table->string('y');
            $table->string('z');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promo');
    }
}
