<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {

            $table->increments('userid');
            $table->string('username', 25)->unique();
            $table->string('password', 100);
            $table->string('email', 25)->unique();
            $table->integer('status')->default(0)->comment('0 inactive 1 active');
            $table->string('token', 32)->comment('Token for registered user');
            $table->integer('token_status')->default(0)->comment('status token for registered user (0 inactive 1 active)');
            $table->dateTime('token_lifetime')->comment('expired date for token');
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
