<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUserRole extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_role', function (Blueprint $table) {
            $table->integer('userid');
            $table->integer('roleid');
            $table->string('airportcodes')->nullable();
            $table->string('poicategories')->nullable();
            $table->timestamps();

            $table->foreign('userid')->references('userid')->on('users')->onDelete('cascade');
            $table->foreign('roleid')->references('id')->on('role')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_role');
    }
}
