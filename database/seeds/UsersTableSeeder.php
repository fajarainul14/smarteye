<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'username' => 'adminsatu',
                'email' => 'adminsatu@email.com',
                'password' => md5('password'),
                'token' => str_random(32),
                'status' => 1,
                'token_lifetime' => '2018-10-10 12:12:12'
            ],
            [
                'username' => 'admindua',
                'email' => 'admindua@email.com',
                'password' => md5('password'),
                'token' => str_random(32),
                'status' => 1,
                'token_lifetime' => '2018-10-10 12:12:12'
            ],
            [
                'username' => 'admintiga',
                'email' => 'admintiga@email.com',
                'password' => md5('password'),
                'token' => str_random(32),
                'status' => 0,
                'token_lifetime' => '2018-10-10 12:12:12'
            ],
            [
                'username' => 'adminlima',
                'email' => 'adminlima@email.com',
                'password' => md5('password'),
                'token' => str_random(32),
                'status' => 1,
                'token_lifetime' => '2018-10-10 12:12:12'
            ],
            [
                'username' => 'adminenam',
                'email' => 'adminenam@email.com',
                'password' => md5('password'),
                'token' => str_random(32),
                'status' => 1,
                'token_lifetime' => '2018-10-10 12:12:12'
            ],
            [
                'username' => 'admintujuh',
                'email' => 'admintujuh@email.com',
                'password' => md5('password'),
                'token' => str_random(32),
                'status' => 1,
                'token_lifetime' => '2018-10-10 12:12:12'
            ],
            [
                'username' => 'admindelapan',
                'email' => 'admindelapan@email.com',
                'password' => md5('password'),
                'token' => str_random(32),
                'status' => 1,
                'token_lifetime' => '2018-10-10 12:12:12'
            ],
        ]);
    }
}
