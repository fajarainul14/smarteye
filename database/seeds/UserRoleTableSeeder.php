<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserRoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_role')->insert([
            [
                'userid' => 1,
                'roleid' => 1,
                'airportcodes' => null,
                'poicategories' => null
            ],
            [
                'userid' => 1,
                'roleid' => 2,
                'airportcodes' => null,
                'poicategories' => "1,2"
            ],
            [
                'userid' => 1,
                'roleid' => 3,
                'airportcodes' => null,
                'poicategories' => null

            ],
            [
                'userid' => 1,
                'roleid' => 4,
                'airportcodes' => null,
                'poicategories' => null
            ],
            [
                'userid' => 2,
                'roleid' => 2,
                'airportcodes' => null,
                'poicategories' => "1,2"
            ],
            [
                'userid' => 2,
                'roleid' => 3,
                'airportcodes' => null,
                'poicategories' => null
            ],
            [
                'userid' => 2,
                'roleid' => 4,
                'airportcodes' => null,
                'poicategories' => null
            ],
            [
                'userid' => 3,
                'roleid' => 3,
                'airportcodes' => null,
                'poicategories' => null
            ],
            [
                'userid' => 3,
                'roleid' => 4,
                'airportcodes' => null,
                'poicategories' => null
            ],
            [
                'userid' => 4,
                'roleid' => 1,
                'airportcodes' => null,
                'poicategories' => null
            ],

            [
                'userid' => 5,
                'roleid' => 2,
                'airportcodes' => null,
                'poicategories' => "4,5"
            ],

            [
                'userid' => 6,
                'roleid' => 3,
                'airportcodes' => null,
                'poicategories' => null
            ],

            [
                'userid' => 7,
                'roleid' => 4,
                'airportcodes' => null,
                'poicategories' => null
            ],


        ]);
    }
}
