<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PriorityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('priority')->insert([
            [
                'description' => 'selalu muncul',
                'airportcode' => 'cgk'
            ],
            [
                'description' => 'muncul ketika peta di zoom-in sedang',
                'airportcode' => 'cgk'
            ],
            [
                'description' => 'muncul ketika peta di zoom-in lebih besar',
                'airportcode' => 'dps'
            ]

        ]);
    }
}
