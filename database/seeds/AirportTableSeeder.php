<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AirportTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('airport')->insert([
            [
                'airportcode' => 'cgk',
                'airportname' => 'Soekarno Hatta',
            ],
            [
                'airportcode' => 'dps',
                'airportname' => 'Denpasar Bali',
            ],

            [
                'airportcode' => 'lop',
                'airportname' => 'Lombok Praya',
            ],

        ]);
    }
}
