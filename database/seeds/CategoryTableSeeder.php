<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('category')->insert([
            [
                'description' => 'Food & Beverage',
                'airportcode' => 'cgk'
            ],
            [
                'description' => 'Shopping',
                'airportcode' => 'cgk'
            ],
            [
                'description' => 'Services',
                'airportcode' => 'cgk'
            ],
            [
                'description' => 'General facility',
                'airportcode' => 'cgk'
            ],
            [
                'description' => 'Airport facility',
                'airportcode' => 'cgk'
            ],
            [
                'description' => 'IMF facility',
                'airportcode' => 'cgk'
            ],
            [
                'description' => 'General facility',
                'airportcode' => 'dps'
            ],
            [
                'description' => 'Airport facility',
                'airportcode' => 'dps'
            ],
            [
                'description' => 'IMF facility',
                'airportcode' => 'dps'
            ],

        ]);
    }
}
