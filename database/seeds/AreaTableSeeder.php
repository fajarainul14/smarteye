<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AreaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('area')->insert([
            [
                'description' => 'Departure Public Area',
                'airportcode' => 'cgk'
            ],
            [
                'description' => 'Departure Check-in Area',
                'airportcode' => 'cgk'
            ],
            [
                'description' => 'Departure Immigration Hall',
                'airportcode' => 'cgk'
            ],
            [
                'description' => 'Departure Commercial Area',
                'airportcode' => 'dps'
            ],
            [
                'description' => 'Departure Area',
                'airportcode' => 'dps'
            ],

        ]);
    }
}
