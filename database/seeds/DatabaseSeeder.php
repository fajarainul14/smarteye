<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(UsersTableSeeder::class);
         $this->call(RoleTableSeeder::class);
         $this->call(UserRoleTableSeeder::class);

         $this->call(AirportTableSeeder::class);
         $this->call(CategoryTableSeeder::class);
         $this->call(IconTableSeeder::class);
         $this->call(PriorityTableSeeder::class);
         $this->call(FloorTableSeeder::class);
         $this->call(AreaTableSeeder::class);
    }
}
