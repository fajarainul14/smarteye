<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class IconTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('icon')->insert([
            [
                'description' => 'Toilet',
                'airportcode' => 'cgk'
            ],
            [
                'description' => 'Nursing Room',
                'airportcode' => 'cgk'
            ],
            [
                'description' => 'Taxi',
                'airportcode' => 'cgk'
            ],
            [
                'description' => 'Food & Beverages',
                'airportcode' => 'dps'
            ],
            [
                'description' => 'Cafe',
                'airportcode' => 'dps'
            ],
            [
                'description' => 'ATM',
                'airportcode' => 'dps'
            ],

        ]);
    }
}
