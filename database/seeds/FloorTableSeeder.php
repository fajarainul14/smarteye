<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FloorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('floor')->insert([
            [
                'description' => 'Lantai 1',
                'airportcode' => 'cgk'
            ],
            [
                'description' => 'Lantai 2',
                'airportcode' => 'cgk'
            ],
            [
                'description' => 'Lantai 3',
                'airportcode' => 'dps'
            ]

        ]);
    }
}
