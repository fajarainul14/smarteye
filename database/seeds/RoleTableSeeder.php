<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('role')->insert([
            [
                'name' => 'superadmin',
                'description' => 'Manage all data',
            ],
            [
                'name' => 'adminpoi',
                'description' => 'Manage data POI',
            ],
            [
                'name' => 'adminpromo',
                'description' => 'Manage data promo',
            ],
            [
                'name' => 'adminicon',
                'description' => 'Manage data Category, Area, Floor & Icon fields',
            ],
        ]);
    }
}
