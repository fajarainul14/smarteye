<?php
return [
    'empty_field'            => 'There is an empty field.',
    'username_email_already_exists'=> 'Username or email already exists',

    'add_user_success'=> 'Add new user success',
    'add_user_failed'=> 'Add new user failed',
    'delete_user_success'=> 'Delete user success',
    'delete_user_failed'=> 'Delete user failed',
    'edit_user_success'=> 'Update user success',
    'edit_user_failed'=> 'Update user failed',

    'add_failed'=> 'Add :model failed',
    'add_success'=> 'Add :model success',
    'delete_failed'=> 'Delete :model failed : :message',
    'delete_success'=> 'Delete :model success',
    'delete_permanent_failed'=> 'Delete permanent :model failed : :message',
    'delete_permanent_success'=> 'Delete permanent :model success',
    'edit_failed'=> 'Update :model failed : :message',
    'edit_success'=> 'Update :model success',
    'restore_failed'=> 'Restore :model failed : :message',
    'restore_success'=> 'Restore :model success',

    '_already_exist'=> ':object already exists',
    '_not_found'=> ':object not found',
    '_still_used'=> ':object is still used in poi or promo',

    'error_server'           => 'Internal Server Error. Please try again later.',
    'logged_in_as'           => 'Logged in as :name.',
    'login_not_match'        => 'Username or password is not match.',
    'logout_success'         => 'Logged Out Successfully',
    'not_active'         => 'Your account is not active, please activate first',

    //middleware
    'token_expired'         => 'Token expired',
    'token_active'          => 'Token active',
    'token_not_found'       => 'Token not found',

    'not_allowed'=> 'You are not allowed to access this action',



];
