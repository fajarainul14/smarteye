<!--/**
 * Created by IntelliJ IDEA.
 * User: OSD-1
 * Date: 9/29/2018
 * Time: 4:14 PM
 */-->
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Smarteye - Wayfinding</title>

    <!-- plugins:css -->
    <link rel="stylesheet" href=" {{asset('bootstrap/vendors/iconfonts/mdi/css/materialdesignicons.min.css')}} ">
    <link rel="stylesheet" href=" {{asset('bootstrap/vendors/css/vendor.bundle.base.css')}}">
    <link rel="stylesheet" href=" {{asset('bootstrap/vendors/css/vendor.bundle.addons.css')}}">
    <!-- endinject -->
    <!-- plugin css for this page -->
    <link rel="stylesheet" href=" {{asset('bootstrap/vendors/iconfonts/font-awesome/css/font-awesome.css')}} ">
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href=" {{asset('bootstrap/css/style.css')}} ">
    <link rel="stylesheet" href=" {{asset('css/style.css')}} ">

    <!-- endinject -->
    <link rel="shortcut icon" href=" {{asset('bootstrap/images/favicon.png')}} "/>
</head>

<body>
<div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper auth-page">
        <div class="content-wrapper d-flex align-items-center auth auth-bg-1 theme-one">
            <div class="row w-100">
                <div class="col-lg-4 mx-auto">
                    <div class="auto-form-wrapper">
                        <form action="{{ action('Admin\AuthController@login') }}" method="post">

                            {{ csrf_field() }}

                            @include('form.input', ['label'=>'Username', 'name'=>'username', 'type'=>'text', 'placeholder'=>'Username', 'required'=>'true'])

                            @include('form.input', ['label'=>'Password', 'name'=>'password', 'type'=>'password', 'placeholder'=>'********', 'required'=>'true'])

                            <div class="form-group">
                                <button class="btn btn-primary submit-btn btn-block">Login</button>
                            </div>

                        </form>
                    </div>
                    <p class="footer-text text-center">Copyright © 2018 <a href="#" target="_blank">Smarteye</a></p>
                </div>
            </div>
        </div>
        <!-- content-wrapper ends -->
    </div>
    <!-- page-body-wrapper ends -->

    @include('admin/_message')

</div>
<!-- container-scroller -->
<!-- plugins:js -->
<script src="{{asset('bootstrap/vendors/js/vendor.bundle.base.js')}}"></script>
<script src="{{asset('bootstrap/vendors/js/vendor.bundle.addons.js')}}"></script>
<!-- endinject -->
<!-- inject:js -->
<script src="{{asset('bootstrap/js/off-canvas.js')}}"></script>
<script src="{{asset('bootstrap/js/misc.js')}}"></script>
<script>

    // display notification if message not null
    if ($('.pop-up').children().length > 0) {
        $('.pop-up').animate({
            'opacity': 1,
            'right': "30px"
        }, 300).animate({ 'right': "15px" }, 400);

        // close notification by time
        setTimeout(close_pop_up, 7000);
    }
    // close notification function
    function close_pop_up(){
        var width = $('.pop-up').width();
        $('.pop-up').animate({
            'opacity': 0,
            'right': -width
        }, 300, function(){
            $('.pop-up').remove();
        });
    }

    // close notification
    $('.pop-up .close').click(close_pop_up);

    // remove notification when clicked out of target
    $(document).click(function(e) {
        var pop_up = $('.pop-up');
        if (!pop_up.is(e.target)) {
            close_pop_up();
        }
    });

</script>
<!-- endinject -->
</body>

</html>
