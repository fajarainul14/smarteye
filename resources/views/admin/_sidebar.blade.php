@php
    $userdata = getAuth();
@endphp
<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
        <li class="nav-item nav-profile">
            <div class="nav-link">
                <div class="user-wrapper">
                    <div class="profile-image">
                        <img src="{{asset('images/person.png')}}" alt="profile image">
                    </div>
                    <div class="text-wrapper">
                        <p class="profile-name">{{ $userdata!=null? $userdata->username:"" }}</p>
                       <!-- <div>
                            <small class="designation text-muted">Level</small>
                            <span class="status-indicator online"></span>
                        </div>-->
                    </div>
                </div>
            </div>
        </li>

        @if(isAdminIcon())

        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#data-master" aria-expanded="false" aria-controls="data-master">
                <i class="menu-icon fa fa-table"></i>
                <span class="menu-title">Data Master</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="data-master">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item {{ Request::segment(2) === 'category' ? 'active' : '' }}">
                        <a class="nav-link" href="{{ action('Admin\CategoryController@index') }}">Category</a>
                    </li>
                    <li class="nav-item" {{ Request::segment(2) === 'priority' ? 'active' : '' }}>
                        <a class="nav-link" href="{{ action('Admin\PriorityController@index') }}">Priority</a>
                    </li>
                    <li class="nav-item {{ Request::segment(2) === 'floor' ? 'active' : '' }}">
                        <a class="nav-link" href="{{ action('Admin\FloorController@index') }}">Floor</a>
                    </li>
                    <li class="nav-item {{ Request::segment(2) === 'icon' ? 'active' : '' }}">
                        <a class="nav-link" href="{{ action('Admin\IconController@index') }}">Icon</a>
                    </li>
                    <li class="nav-item {{ Request::segment(2) === 'area' ? 'active' : '' }}">
                        <a class="nav-link" href="{{ action('Admin\AreaController@index') }}">Area</a>
                    </li>

                    @if(isAdminPusat())
                        <li class="nav-item {{ Request::segment(2) === 'airport' ? 'active' : '' }}">
                            <a class="nav-link" href="{{ action('Admin\AirportController@index') }}">Airport</a>
                        </li>
                    @endif

                </ul>
            </div>
        </li>

        @endif

        @if(isAdminPoi())

        <li class="nav-item {{ Request::segment(2) === 'poi' ? 'active' : '' }}">
            <a class="nav-link" href="{{ action('Admin\PoiController@index') }}">
                <i class="menu-icon fa fa-history"></i>
                <span class="menu-title">POI</span>
            </a>
        </li>

        @endif

        @if(isSuperAdmin() && isAdminPusat())

        <li class="nav-item {{ Request::segment(2) === 'user' ? 'active' : '' }}">
            <a class="nav-link" href="{{ action('Admin\UserController@index') }}">
                <i class="menu-icon fa fa-users"></i>
                <span class="menu-title">User Management</span>
            </a>
        </li>
        @endif

        @if(isSuperAdmin())
        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#trash" aria-expanded="false" aria-controls="trash">
                <i class="menu-icon fa fa-trash"></i>
                <span class="menu-title">Trash</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="trash">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item {{ Request::segment(2) === 'category' && Request::segment(3) === 'trash' ? 'active' : '' }}">
                        <a class="nav-link" href="{{ action('Admin\CategoryController@showTrashed') }}">Category</a>
                    </li>
                    <li class="nav-item {{ Request::segment(2) === 'priority' && Request::segment(3) === 'trash' ? 'active' : '' }}">
                        <a class="nav-link" href="{{ action('Admin\PriorityController@showTrashed') }}">Priority</a>
                    </li>
                    <li class="nav-item {{ Request::segment(2) === 'floor' && Request::segment(3) === 'trash'? 'active' : '' }}">
                        <a class="nav-link" href="{{ action('Admin\FloorController@showTrashed') }}">Floor</a>
                    </li>
                    <li class="nav-item {{ Request::segment(2) === 'category' && Request::segment(3) === 'trash'? 'active' : '' }}">
                        <a class="nav-link" href="{{ action('Admin\IconController@showTrashed') }}">Icon</a>
                    </li>
                    <li class="nav-item {{ Request::segment(2) === 'area' && Request::segment(3) === 'trash'? 'active' : '' }}">
                        <a class="nav-link" href="{{ action('Admin\AreaController@showTrashed') }}">Area</a>
                    </li>

                    @if(isAdminPusat())
                        <li class="nav-item {{ Request::segment(2) === 'airport' && Request::segment(3) === 'trash'? 'active' : '' }}">
                            <a class="nav-link" href="{{ action('Admin\AirportController@showTrashed') }}">Airport</a>
                        </li>
                    @endif

                </ul>
            </div>
        </li>

        @if(isAdminPusat())
        <li class="nav-item {{ Request::segment(2) === 'logs' ? 'active' : '' }}">
            <a class="nav-link" href="{{ action('Admin\LogController@index') }}">
                <i class="menu-icon fa fa-history"></i>
                <span class="menu-title">Logs</span>
            </a>
        </li>
        @endif

        @endif


        @if(isAdminPromo())

        <li class="nav-item {{ Request::segment(2) === 'promo' ? 'active' : '' }}">
            <a class="nav-link" href="{{ action('Admin\PromoController@index') }}">
                <i class="menu-icon fa fa-tag"></i>
                <span class="menu-title">Promo</span>
            </a>
        </li>

        @endif

    </ul>
</nav>
