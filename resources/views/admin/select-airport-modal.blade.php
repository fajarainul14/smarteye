<!-- Modal -->
<div class="modal fade" id="selectAirportModal" tabindex="-1" role="dialog" aria-labelledby="selectAirportModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="selectAirportModalLabel">Select Airport</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="row">

                    <div class="col-md-6">

                        <input type="text" placeholder="Airport" class="form-control col-md-12" id="filter-airport"/>

                    </div>
                    <div class="clearfix"></div>

                    <div class="col-md-12" style="margin-top: 24px;" id="checkbox-container">

                        <div id="please-wait">
                            <span><p class="text-center">Please wait...</p></span>
                        </div>

                        <div class="row">

                        </div>

                    </div>

                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Save</button>
            </div>
        </div>
    </div>
</div>

