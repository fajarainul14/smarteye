@extends('admin/_layout')

@section('title', 'Smarteye - User List')

@section('css')
<style>
    ul {
        list-style: none;
    }

    li{
        margin-top : 6px
    }

    .modal-content {
        background-color: #fff;
    }
</style>
@endsection

@section('content')
<div class="content-wrapper">
    <h3 class="page-heading mb-4">User</h3>

    <div class="row">
        <div class="col-lg-6 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">

                    <div style="margin-bottom: 20px;">
                        <a href="#" class="btn btn-primary btn-md" id="button-add">Add User</a>
                    </div>

                    <div class="table-responsive">
                        <table class="table center-aligned-table data-table table-fit">
                            <thead>
                            <tr class="text-primary">
                                <th>Userid</th>
                                <th>Username</th>
                                <th>Email</th>
                                <th>Status</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($users as $key => $user)
                                @if($user->poicategories)
                                    @php
                                        $poicategories = implode(',', $user->poicategories);
                                    @endphp
                                @else
                                    @php
                                        $poicategories = "";
                                    @endphp
                                @endif

                                @if($user->airportcodes)
                                    @php
                                        $airports = implode(',', $user->airportcodes);
                                    @endphp
                                @else
                                    @php
                                        $airports = "";
                                    @endphp
                                @endif

                                @php
                                    $userroles = "";
                                @endphp

                                @foreach($user->roles as $key => $role)
                                    @php
                                        $userroles = $role->id.",".$userroles;
                                    @endphp

                                @endforeach


                            <tr class="item-user">
                                <td data-poicategories="{{$poicategories}}" data-roles="{{$userroles}}" data-airports="{{$airports}}">{{ $user->userid }}</td>
                                <td>{{ $user->username }}</td>
                                <td>{{ $user->email }}</td>
                                @if($user->status==0)
                                <td><span class="badge badge-warning">Inactive</span></td>
                                @elseif($user->status==1)
                                <td><span class="badge badge-success">Active</span></td>
                                @elseif($user->status==-1)
                                <td><span class="badge badge-success">Deleted</span></td>
                                @endif
                            </tr>

                            @endforeach

                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>

        <div class="col-lg-6 grid-margin stretch-card" id="form-container">
            <div class="card">
                <div class="card-body">
                    <h1 class="card-title" style="font-size: 20px">Add User</h1>
                    <form method="POST" action="#" id="form-add-user">
                    {{ csrf_field() }}
                    @include('admin.user-form-datauser')
                    @include('admin.user-form-roles')
                    @include('admin.user-form-airports')
                    @include('admin.user-form-categories')

                    </form>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection

@section('js')
<script type="text/javascript">
    var listAirport = [];
    var listCategory = [];
    var listRoles = [];
    var filteredAirport = [];
    var listSelectedAirport = [];
    var listSelectedCategory = [];
    var isAdminPoi = false;
    var isAdminPusat = false;

    $(document).ready(function () {

        $('#form-container').hide();
        $('#form-datauser').show();
        $('#form-roles').hide();
        $('#form-airports').hide();
        $('#form-categories').hide();


        // if($("input.roles[type=checkbox][value=2]").is(':checked')){
        //     $('#categories-checkbox').show();
        // }else{
        //     $('#categories-checkbox').hide();
        // }

        $('#button-add').click(function () {
            resetForm();

            $('input:checkbox').prop('checked',false);

            $('#form-container input[name="password"]').parent().show();
            $('#form-container input[name="password"]').prop('required',true);

            $('#form-container input[name="username"]').val('');
            $('#form-container input[name="email"]').val('');

            $('#form-container input[name="username"]').prop("disabled", false);
            $('#form-container input[name="email"]').prop("disabled", false);

            $('#form-container input[name="password"]').val('');
            $('#form-container input[name="userid"]').val('');

            $('#form-container').show();
            $('#form-container h1.card-title').text('Add User');

            $('#button-delete').hide();

            $('#form-container form').attr('action', "{{ action('Admin\UserController@store') }}");
        });

        $('#button-form-datauser-next').click(function(e){
            var isValid = $("#form-add-user").valid();

            e.preventDefault();

            if(isValid) {
                $('#form-datauser').hide();
                $('#form-roles').show();
            }
            return false;
        });

        $('#button-form-roles-prev').click(function(){

            $('#form-datauser').show();
            $('#form-roles').hide();

            return false;

        });

        $('#button-form-roles-next').click(function(){

            $('#form-roles').hide();
            $('#form-airports').show();

            if(listAirport.length<=0){
                getAirport();
            }

            if(!isAdminPoi || isAdminPusat){
                $('#button-form-airports-next span').text("Finish")
            }else{
                $('#button-form-airports-next span').text("Next")
            }

            return false;

        });

        $('#button-form-airports-prev').click(function(){

            $('#form-roles').show();
            $('#form-airports').hide();

            return false;

        });

        $('#button-form-airports-next').click(function(){

            if(isAdminPoi){
                if(isAdminPusat){
                    submitForm();
                }else{
                    $('#form-airports').hide();
                    $('#form-categories').show();

                    $('#checkbox-categories .row').empty();
                    getCategoryByAirport(listSelectedAirport);
                }
            }else{
                submitForm();
            }

            return false;

        });

        $('#button-form-categories-prev').click(function(){

            $('#form-airports').show();
            $('#form-categories').hide();
            return false;

        });

        $('#button-form-categories-next').click(function(){

            submitForm();
            return false;

        });

        //check if roles is checked/unchecked
        //if all checkbox unchecked, disable button next
        //if at least one checkbox is checked, enable button next
        $(document).on('change', '.roles', function(event) {
            if ($('input.roles[type=checkbox]:checked').length > 0) {
                $('#button-form-roles-next').attr('disabled', false);
            }else{
                $('#button-form-roles-next').attr('disabled', true);
            }
        });

        $(document).on('change', '.checkbox-airport', function(event) {
            if ($('input.checkbox-airport[type=checkbox]:checked').length > 0) {
                $('#button-form-airports-next').attr('disabled', false);
            }else{
                $('#button-form-airports-next').attr('disabled', true);
            }

            //uncheck check all airport
            $("#checkbox-airport-all").prop('checked', false);
        });

        $(document).on('change', '.checkbox-category', function(event) {
            if ($('input.roles[type=checkbox]:checked').length > 0) {
                $('#button-form-categories-next').attr('disabled', false);
            }else{
                $('#button-form-categories-next').attr('disabled', true);
            }
        });

        //check all airport
        $('#checkbox-airport-all').on('change', function(){
            $("input.checkbox-airport[type=checkbox]").prop('checked', this.checked);

            $('#button-form-airports-next').attr('disabled', !this.checked);
        });

        //check as admin pusat
        $('#checkbox-admin-all').change(function(){
            if(this.checked){
                $("input.checkbox-airport[type=checkbox]").prop('checked', !this.checked);
                $("#checkbox-airport-all").prop('checked', !this.checked);
                $('#button-form-airports-next span').text("Finish");
            }else{
                $('#button-form-airports-next span').text("Next");
            }

            $("input.checkbox-airport[type=checkbox]").attr('disabled', this.checked);
            $("#checkbox-airport-all").attr('disabled', this.checked);
            $("#form-airports #filter-airport").attr('disabled', this.checked);
            $('#button-form-airports-next').attr('disabled', !this.checked);

            isAdminPusat = this.checked;

        });

        //onChecked adminpoi checkbox
        $("input.roles[type=checkbox][value=2]").change(function() {
            if(this.checked) {
                isAdminPoi = true;
            }else{
                isAdminPoi = false
            }
        });

        $('.item-user').click(function () {

            resetForm();

            var id = $(this).find('td:eq(0)').text();
            var username = $(this).find('td:eq(1)').text();
            var email = $(this).find('td:eq(2)').text();
            var poicategories = $(this).find('td:eq(0)').attr("data-poicategories");
            var roles = $(this).find('td:eq(0)').attr("data-roles");
            var airports = $(this).find('td:eq(0)').attr("data-airports");

            listSelectedAirport = airports.split(",");
            listSelectedCategory = poicategories.split(",");
            listRoles = roles.split(",");

            if(jQuery.inArray(2, listRoles) !== -1){
                isAdminPoi = true;
            }

            $('.roles:checkbox').each(function () {
                var checkboxValue = $(this).val();
                if(jQuery.inArray(checkboxValue, listRoles) !== -1){
                    $(this).prop("checked", true).change();
                }else{
                    $(this).prop("checked", false).change();
                }
            });

            $('#form-container input[name="password"]').parent().hide();
            $('#form-container input[name="password"]').prop('required',false);


            $('#form-container input[name="username"]').val(username);
            $('#form-container input[name="email"]').val(email);
            //set disabled
            $('#form-container input[name="username"]').prop("disabled", true);
            $('#form-container input[name="email"]').prop("disabled", true);

            $('#form-container input[name="userid"]').val(id);

            $('#form-container').show();
            $('#form-container h1.card-title').text('Edit User');

            $('#button-delete').show();

            $('#form-container form').attr('action', "{{ action('Admin\UserController@update') }}");

            var deleteUrl = '{{ url("/admin/user/delete") }}';

            $('#button-delete').attr('href', deleteUrl+'/'+id );

        });

        //delete data
        $('#button-delete').click(function(){
            var username = $('#form-container input[name="username"]').val();
            var email = $('#form-container input[name="email"]').val();

            return confirm("Are you sure, you want to delete user ("+ username + ")("+email+")" );

        });

        //FILTER AIRPORT
        $('#form-airports #filter-airport').keyup(delay(function (e) {
            airportName = $(this).val();
            filterAirport(listAirport, airportName);
        }, 3000));

        //Oncheck airport, add to listSelectedAirport if checked
        //remove rom listSelectedAirport if unchecked
        $(document).on('change', '.checkbox-airport', function(event) {
            if(this.checked) {
                //add to list
                if(jQuery.inArray($(this).val(), listSelectedAirport) === -1){
                    listSelectedAirport.push($(this).val())
                }
            }else{
                //remove from list
                var index = listSelectedAirport.indexOf($(this).val());
                if (index !== -1) listSelectedAirport.splice(index, 1);
            }
        });

    });//END DOCUMENT READY

    function resetForm (){
        $('#form-datauser').show();
        $('#form-roles').hide();
        $('#form-airports').hide();
        $('#form-categories').hide();

        isAdminPoi = false;
        isAdminPusat = false;
        filteredAirport = [];
        listSelectedAirport = [];
        listSelectedCategory = [];
    }

    function submitForm(){
        var username = $('#form-container input[name="username"]').val();
        var email = $('#form-container input[name="email"]').val();

        var message = "Are you sure you want to save User ?\n \"(" + username + ") - (" + email + ") ?";

        if(confirm(message)){
            $('#form-add-user').submit();
        }

        return false;

    }

    function getAirport(){

        $('#form-container #please-wait').show();
        $('#checkbox-airports').hide();
        var request = $.ajax({
            url: "{{action('Admin\\MasterController@getAirport')}}",
            type: "POST",
            dataType: "json",
            data : {
                _token: '{{csrf_token()}}'
            }
        });

        request.done(function(response) {
            $('#form-container #please-wait').hide();
            $('#checkbox-airports').show();
            console.log(response);

            if(response.success){
                var dataResult = response.data.airports;
                listAirport = dataResult;
                filteredAirport = dataResult;

                setAirports(dataResult);

            }else{
                alert("Error occured, please try again.")
                console.log("error occured");
            }

        });

        request.fail(function(jqXHR, textStatus) {
            $('#form-container #please-wait').hide();
            $('#checkbox-airports').show();
            console.log( "Request failed: " + textStatus );
            console.log(jqXHR);
        });
    }

    function setAirports(data){
        $('#checkbox-airports .row').empty();
        for(var i=0; i< data.length; i++){
            var row = "<div class=\"form-check col-md-5\" style=\"padding-left: 0px\">";
            row += "<label>";
            if(listSelectedAirport.indexOf(data[i].airportcode) !== -1){
                row += "<input type=\"checkbox\" name=\"airports[]\" value=\""+data[i].airportcode+"\" class=\"checkbox-airport\" checked> <span class=\"label-text\" style=\"font-size: 16px\">"+data[i].airportname+"</span>";
            }else{
                row += "<input type=\"checkbox\" name=\"airports[]\" value=\""+data[i].airportcode+"\" class=\"checkbox-airport\"> <span class=\"label-text\" style=\"font-size: 16px\">"+data[i].airportname+"</span>";
            }

            row += "</label>";
            row += "</div>";
            $('#checkbox-airports .row').append(row);
        }
        $('.checkbox-airport').change();
    }

    function filterAirport(data, keyword){

        var result = [];

        for(var i=0; i< data.length; i++){

            if(data[i].airportname.toLowerCase().indexOf(keyword.toLowerCase()) !== -1){
                result.push(data[i])
            }
        }

        setAirports(result);
    }

    function getCategoryByAirport(airportcodes){
        console.log(airportcodes)
        $('#form-container #please-wait').show();
        var request = $.ajax({
            url: "{{action('Admin\\MasterController@getCategoryByAirport')}}",
            type: "POST",
            dataType: "json",
            data : {
                _token: '{{csrf_token()}}',
                airportcode : airportcodes,
            }
        });

        request.done(function(response) {
            $('#form-container #please-wait').hide();
            console.log(response);

            if(response.success){
                var dataResult = response.data;

                setCategories(dataResult);
            }else{
                alert("Error occured, please try again.")
                console.log("error occured");
            }

        });

        request.fail(function(jqXHR, textStatus) {
            $('#form-container #please-wait').hide();
            console.log( "Request failed: " + textStatus );
            console.log(jqXHR);
        });

    }

    function setCategories(data){
        $('#checkbox-categories .row').empty();
        currentAirport = '';
        for(var i=0; i< data.length; i++){
            var row = '';
            if(data[i].airportcode!== currentAirport){
                row += "<h4 class=\"col-md-12\">"+data[i].airport.airportname+" ("+data[i].airport.airportcode+")</h4>";

                currentAirport = data[i].airportcode;
            }
            row += "<div class=\"form-check col-md-5\" style=\"padding-left: 0px\">";
            row += "<label>";
            if(jQuery.inArray(data[i].id.toString(), listSelectedCategory) !== -1){
                row += "<input type=\"checkbox\" name=\"categories[]\" value=\""+data[i].id+"\" class=\"checkbox-category\" checked> <span class=\"label-text\" style=\"font-size: 16px\">"+data[i].description+"</span>";
            }else{
                row += "<input type=\"checkbox\" name=\"categories[]\" value=\""+data[i].id+"\" class=\"checkbox-category\"> <span class=\"label-text\" style=\"font-size: 16px\">"+data[i].description+"</span>";
            }

            row += "</label>";
            row += "</div>";
            $('#checkbox-categories .row').append(row);
        }

        $('.checkbox-category').change();
    }
</script>
@endsection
