@extends('admin/_layout')

@section('title', 'Smarteye - POI List')

@section('css')
<style>
    .form-filter {
        margin-left: 8px;
        margin-right: 8px;
    }

    #filter-div {
        margin-top: 20px;
        margin-bottom: 20px;
        text-align: center;
    }

    .modal-content {
        background-color: #fff;
    }

</style>
@endsection

@section('content')
<div class="content-wrapper">
    <h3 class="page-heading mb-4">POI</h3>

    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">

                    <form id="formDeletePoi" action="{{ action('Admin\PoiController@destroy') }}" method="post">
                        {{ csrf_field() }}

                    </form>

                        <div style="margin-bottom: 20px;">
                        <a href="#" class="btn btn-primary btn-md" id="button-add" data-toggle="modal"
                           data-target="#addPoiModal">Add POI</a>

                        <a href="#" class="btn btn-danger btn-md" id="deletePoi">Delete</a>

                        <a href="#" class="btn btn-info btn-md" id="button-filter">Filter</a>

                    </div>

                    <div class="form-row" id="filter-div">
                        <div class="col" style="margin-top: 26px">
                            @include('form.input', ['label'=>'', 'name'=>'name', 'type'=>'text', 'placeholder'=>'Name',
                            'required'=>'true', 'class'=>'col-md-12', 'id'=>'filter-name'])
                        </div>
                        <div class="col">
                            @include('form.select', ['label'=>'', 'name'=>'airportcode', 'placeholder'=>'Airport',
                            'required'=>'true', 'class'=>'col-md-12', 'options'=> convertObject($master->grantedAirport,
                            'airportcode', 'airportname'), 'id'=>'filter-airport' ])
                        </div>
                        <div class="col">
                            @include('form.select', ['label'=>'', 'name'=>'areaid', 'placeholder'=>'Area',
                            'required'=>'true', 'class'=>'col-md-12', 'id'=>'filter-area' ])
                        </div>
                        <div class="col">
                            @include('form.select', ['label'=>'', 'name'=>'categoryid', 'placeholder'=>'Category',
                            'required'=>'true', 'class'=>'col-md-12', 'id'=>'filter-category' ])
                        </div>
                        <div class="col">
                            @include('form.select', ['label'=>'', 'name'=>'floorid', 'placeholder'=>'Floor',
                            'required'=>'true', 'class'=>'col-md-12', 'id'=>'filter-floor' ])
                        </div>
                    </div>

                    <div id="please-wait">
                        <span><p class="text-center">Please wait...</p></span>
                    </div>
                    <div class="table-responsive">
                        <table class="table center-aligned-table data-table table-fit" id="tablePoi">
                            <thead>
                            <tr class="text-primary">
                                <th></th>
                                <th>ID</th>
                                <th>Airport</th>
                                <th>Name</th>
                                <th>Category</th>
                                <th>Area</th>
                                <th>Floor</th>
                                <th>Priority</th>
                                <th>Icon</th>
                                <th>X</th>
                                <th>Y</th>
                                <th>Z</th>
                            </tr>
                            </thead>
                            <tbody>

                            {{--@foreach($pois as $key => $poi)

                            <tr class="item-poi">
                                <td><input class="except"type="checkbox" value="{{$poi->id}}"/></td>
                                <td>{{ $poi->id }}</td>
                                <td>{{ $poi->airport->airportname }}</td>
                                <td>{{ $poi->name }}</td>
                                <td>{{ $poi->category->description }}</td>
                                <td>{{ $poi->area->description }}</td>
                                <td>{{ $poi->floor->description }}</td>
                                <td>{{ $poi->priority->description }}</td>
                                <td>{{ $poi->icon->description }}</td>
                                <td>{{ $poi->x }}</td>
                                <td>{{ $poi->y }}</td>
                                <td>{{ $poi->z }}</td>
                            </tr>

                            @endforeach--}}

                            </tbody>
                        </table>
                    </div>

                </div>

                <nav aria-label="pagination-poi" style="margin-right: 16px" id="pagination">
                    <span><p style="text-align: right"></p></span>
                    <ul class="pagination justify-content-end">
                        {{--@if($totalPage>1)

                            <!--@if($totalPage<=6)

                                @for($i=1; $i<= $totalPage; $i++)

                                    @if($currentPage == $i)
                                        <li class="page-item disabled"> <a data-pagenumber="{{$i}}" class="page-link" href="#" tabindex="-1">{{$i}}</a></li>
                                    @else
                                        <li class="page-item"> <a data-pagenumber="{{$i}}" class="page-link" href="#">{{$i}}</a></li>

                                    @endif

                                @endfor
                            @endif-->
                            @for($i=1; $i<= $totalPage; $i++)

                                @if($currentPage == $i)
                                    <li class="page-item disabled"> <a data-pagenumber="{{$i}}" class="page-link" href="#" tabindex="-1">{{$i}}</a></li>
                                @else
                                    <li class="page-item"> <a data-pagenumber="{{$i}}" class="page-link" href="#">{{$i}}</a></li>

                                @endif

                            @endfor

                        @endif--}}
                    </ul>
                </nav>
            </div>
        </div>

    </div>

</div>
@include('admin/poi-create-modal')
@include('admin/poi-edit-modal')

@endsection

@section('js')
<script type="text/javascript">
    var filterName = '';
    var filterAirport;
    var filterFloor;
    var filterArea;
    var filterCategory;
    var currentPage = 1;
    var pageNumber = 1;
    var selectedFloor;
    var selectedIcon;
    var selectedArea;
    var selectedCategory;
    var selectedPriority;

    $('#please-wait').hide();
    $('#filter-name').val('');
    $('#filter-category').val('');
    $('#filter-floor').val('');
    $('#filter-airport').val('');
    $('#filter-area').val('');

    $(document).ready(function () {
        $('select[name="airportcode"]').val('');

        getDataPoi();
        $('#filter-div').hide();

        $('#button-filter').click(function () {

            if ($('#filter-div').is(":visible")) {

                $('#filter-div').hide();

            } else {

                $('#filter-div').show();

            }

        });

        //confirmation before add POI
        $('#addPoiModal').submit(function () {
            var poiName = $('#addPoiModal input[name="name"]').val();
            var category = $('#addPoiModal select[name="categoryid"]').children("option").filter(":selected").text();
            var area = $('#addPoiModal select[name="areaid"]').children("option").filter(":selected").text();
            return confirm("Are you sure you want to save POI ?\n \"(" + poiName + ") - (" + category + ") - (" + area + ")\"");
        });

        //confirmation before edit POI
        $('#editPoiModal').submit(function () {
            var poiName = $('#editPoiModal input[name="name"]').val();
            var category = $('#editPoiModal select[name="categoryid"]').children("option").filter(":selected").text();
            var area = $('#editPoiModal select[name="areaid"]').children("option").filter(":selected").text();
            return confirm("Are you sure you want to save POI ?\n \"(" + poiName + ") - (" + category + ") - (" + area + ")\"");
        });

        //onclick item poi
        $(document).on('click', '.item-poi', function(event) {
            if (!$(event.target).hasClass("except")) {

                selectedFloor = '';
                selectedIcon = '';
                selectedArea = '';
                selectedCategory = '';
                selectedPriority = '';

                var id = $(this).find('td:eq(1)').text();
                var airport = $(this).find('td:eq(2)').attr('data-airportcode');
                var name = $(this).find('td:eq(3)').text();
                var category = $(this).find('td:eq(4)').attr('data-categoryid');
                var area = $(this).find('td:eq(5)').attr('data-areaid');
                var floor = $(this).find('td:eq(6)').attr('data-floorid');
                var priority = $(this).find('td:eq(7)').attr('data-priorityid');
                var icon = $(this).find('td:eq(8)').attr('data-iconid');
                var x = $(this).find('td:eq(9)').text();
                var y = $(this).find('td:eq(10)').text();
                var z = $(this).find('td:eq(11)').text();

                $('#editPoiModal').modal('show');

                $('#editPoiModal input[name="name"]').val(name);
                $('#editPoiModal input[name="id"]').val(id);
                $('#editPoiModal select[name="airportcode"]').val(airport);
                // $('#editPoiModal select[name="categoryid"]').val(category);
                // $('#editPoiModal select[name="areaid"]').val(area);
                // $('#editPoiModal select[name="floorid"]').val(floor);
                // $('#editPoiModal select[name="priorityid"]').val(priority);
                // $('#editPoiModal select[name="iconid"]').val(icon);
                $('#editPoiModal input[name="x"]').val(x);
                $('#editPoiModal input[name="y"]').val(y);
                $('#editPoiModal input[name="z"]').val(z);

                selectedFloor = floor;
                selectedIcon = icon;
                selectedArea = area;
                selectedCategory = category;
                selectedPriority = priority;

                getDataByAirport('', airport, 'modal-edit');
            }
        }) ;

        //delete poi
        $('#deletePoi').click(function () {
            var ids = [];
            var totalChecked;
            var listPoi = [];
            var listCat = [];
            var listArea = [];

            totalChecked = $('#tablePoi').find('input[type="checkbox"]:checked').length;

            $('#tablePoi').find('tr').each(function () {
                var row = $(this);
                if (row.find('input[type="checkbox"]').is(':checked')){

                    var id = row.find("input[type=checkbox]:checked").val();
                    var name = row.find("td:eq(3)").text();
                    var cat = row.find("td:eq(4)").text();
                    var area = row.find("td:eq(5)").text();
                    console.log(name);
                    ids.push(id)
                    listPoi.push(name)
                    listCat.push(cat)
                    listArea.push(area)

                }
            });

            if(ids.length>0){

                $('#formDeletePoi input[name="ids[]"]').remove();
                for (var i =0; i<ids.length ;i++){
                    $('#formDeletePoi').append("<input type=hidden  name=ids[] value="+ids[i]+" />");
                }

                console.log(totalChecked)
                if(totalChecked>2){
                    if(confirm("Are you sure you want to delete POIs")){
                        $('#formDeletePoi').submit();
                    }
                }else{
                    var message = '';

                    for (var i=0; i< totalChecked; i++ ){

                        message = message + "\""+listPoi[i] + " - " +listCat[i] + " - "+listArea[i] + "\"\n";
                    }

                    if(confirm("Are you sure you want to delete POI? \n\n"+message)){
                        $('#formDeletePoi').submit();
                    }
                }
                // $('#formDeletePoi').submit();
            }

        });

        //FILTER
        $('#filter-name').keyup(delay(function (e) {
            filterName = $('#filter-name').val();
            pageNumber = 1;
            getDataPoi();
        }, 3000));

        $('#filter-category').on('change', function() {
            filterCategory = this.value;
            pageNumber = 1;
            getDataPoi();
        });

        $('#filter-floor').on('change', function() {
            filterFloor = this.value;
            pageNumber = 1;
            getDataPoi();
        });

        $('#filter-airport').on('change', function() {
            filterAirport = this.value;
            pageNumber = 1;
            getDataPoi();
            getDataByAirport('', this.value, 'filter');

            $('#filter-div select').not(this).val('');

        });

        $('#filter-area').on('change', function() {
            filterArea = this.value;
            pageNumber = 1;
            getDataPoi();
        });

        $(document).on('click', '#pagination li a', function() {
            pageNumber = $(this).data('pagenumber');
            getDataPoi();
        }) ;

        //on change airport
        //get master
        $('#addPoiModal select[name="airportcode"]').change(function(){

            var airportcode = $(this).val();

            getDataByAirport('', airportcode, 'modal-add');

            //reset all select to default value
            $('#addPoiModal select').not(this).val('');
        });

        $('#editPoiModal select[name="airportcode"]').change(function(){

            var airportcode = $(this).val();

            getDataByAirport('', airportcode, 'modal-edit');

            //reset all select to default value
            $('#editPoiModal select').not(this).val('');
        })

    });//END DOCUMENT READY

    function getDataByAirport(masterType, airportcode, position){
        var containerFloorFilter = $('#filter-floor');
        var containerCategoryFilter = $('#filter-category');
        var containerAreaFilter = $('#filter-area');

        var containerFloor = $('#addPoiModal select[name="floorid"]');
        var defaultValueFloor = "<option value=\"\">- Select Floor -</option>";

        var containerIcon = $('#addPoiModal select[name="iconid"]');
        var defaultValueIcon = "<option value=\"\">- Select Icon -</option>";

        var containerArea = $('#addPoiModal select[name="areaid"]');
        var defaultValueArea = "<option value=\"\">- Select Area -</option>";

        var containerPriority = $('#addPoiModal select[name="priorityid"]');
        var defaultValuePriority = "<option value=\"\">- Select Priority -</option>";

        var containerCategory = $('#addPoiModal select[name="categoryid"]');
        var defaultValueCategory = "<option value=\"\">- Select Category -</option>";

        var containerFloorEdit = $('#editPoiModal select[name="floorid"]');

        var containerIconEdit = $('#editPoiModal select[name="iconid"]');

        var containerAreaEdit = $('#editPoiModal select[name="areaid"]');

        var containerPriorityEdit = $('#editPoiModal select[name="priorityid"]');

        var containerCategoryEdit = $('#editPoiModal select[name="categoryid"]');

        var data = {
            _token: '{{csrf_token()}}',
            airportcode : airportcode,
            filter : masterType
        };

        var request = $.ajax({
            url: "{{action('Admin\\MasterController@getDataByAirportCode')}}",
            type: "POST",
            data: data,
            dataType: "json"
        });

        console.log(data);

        request.done(function(response) {
            console.log(response);

            if(response.success){
                var dataFloor = response.data.floors;
                var dataIcon = response.data.icons;
                var dataArea = response.data.areas;
                var dataCategory = response.data.categories;
                var dataPriority = response.data.priorities;

                if(position==='filter'){
                    setDataMasterOption(dataFloor, containerFloorFilter, defaultValueFloor, selectedFloor);
                    setDataMasterOption(dataCategory, containerCategoryFilter, defaultValueCategory, selectedIcon);
                    setDataMasterOption(dataArea, containerAreaFilter, defaultValueArea, selectedArea);
                }else if(position === 'modal-edit'){
                    setDataMasterOption(dataFloor, containerFloorEdit, defaultValueFloor, selectedFloor);
                    setDataMasterOption(dataIcon, containerIconEdit, defaultValueIcon, selectedIcon);
                    setDataMasterOption(dataArea, containerAreaEdit, defaultValueArea, selectedArea);
                    setDataMasterOption(dataCategory, containerCategoryEdit, defaultValueCategory, selectedCategory);
                    setDataMasterOption(dataPriority, containerPriorityEdit, defaultValuePriority, selectedPriority);
                }else{
                    setDataMasterOption(dataFloor, containerFloor, defaultValueFloor);
                    setDataMasterOption(dataIcon, containerIcon, defaultValueIcon);
                    setDataMasterOption(dataArea, containerArea, defaultValueArea);
                    setDataMasterOption(dataCategory, containerCategory, defaultValueCategory);
                    setDataMasterOption(dataPriority, containerPriority, defaultValuePriority);
                }

            }else{
                alert("error occured, please try again");
                console.log("error occured");
            }

        });

        request.fail(function(jqXHR, textStatus) {
            alert("error occured, please try again");
            console.log( "Request failed: " + textStatus );
            console.log(jqXHR);
        });

    }

    function setDataMasterOption(data, container, defaultValue, selectedId){
        container.empty().append(defaultValue);
        for(var i=0; i< data.length; i++) {
            var option = "";
            if(selectedId == data[i].id){
                option += "<option value=\""+data[i].id+"\" selected>" + data[i].description + "</option>";
            }else{
                option += "<option value=\""+data[i].id+"\">" + data[i].description + "</option>";
            }

            container.append(option);
        }
    }

    function getDataPoi(){

        var data = {_token: '{{csrf_token()}}', page_number:pageNumber};
        if(filterName!==''){
            data.name = filterName;
        }

        if(filterArea!=null){
            data.areaid = filterArea;
        }

        if(filterCategory!=null){
            data.categoryid = filterCategory;
        }

        if(filterFloor!=null){
            data.floorid = filterFloor;
        }

        if(filterAirport!=null){
            data.airportcode = filterAirport;
        }

        console.log(data);

        $('#please-wait').show();
        var request = $.ajax({
            url: "{{action('Admin\\PoiController@getDataPoi')}}",
            type: "POST",
            data: data,
            dataType: "json"
        });

        request.done(function(response) {
            $('#please-wait').hide();
            console.log(response);

            if(response.success){
                var dataPoi = response.data;
                var totalPage = response.total_page;
                var currentPage = response.current_page;

                setDataPoiResponse(dataPoi);
                setPageNumber(totalPage, currentPage);

            }else{
                alert("error occured, please try again");
                console.log("error occured");
            }

        });

        request.fail(function(jqXHR, textStatus) {
            $('#please-wait').hide();
            alert("error occured, please try again");
            console.log( "Request failed: " + textStatus );
            console.log(jqXHR);
        });

    }

    function setDataPoiResponse(data){
        $('#tablePoi tbody').empty();
        for(var i=0; i< data.length; i++){
            var row = "<tr class=\"item-poi\">";
            row += "<td><input class=\"except\"type=\"checkbox\" value="+data[i].id+" style=\"position:relative;right:0px\"/></td>";
            row += "<td>"+data[i].id+"</td>";
            row += "<td data-airportcode=\""+data[i].airport.airportcode+"\">"+data[i].airport.airportname+"</td>";
            row += "<td>"+data[i].name+"</td>";
            row += "<td data-categoryid=\""+data[i].category.id+"\">"+data[i].category.description+"</td>";
            row += "<td data-areaid=\""+data[i].area.id+"\">"+data[i].area.description+"</td>";
            row += "<td data-floorid=\""+data[i].floor.id+"\">"+data[i].floor.description+"</td>";
            row += "<td data-priorityid=\""+data[i].priority.id+"\">"+data[i].priority.description+"</td>";
            row += "<td data-iconid=\""+data[i].icon.id+"\">"+data[i].icon.description+"</td>";
            row += "<td>"+data[i].x+"</td>";
            row += "<td>"+data[i].y+"</td>";
            row += "<td>"+data[i].z+"</td>";
            row += "</tr>";
            $('#tablePoi tbody').append(row)
        }
    };

    function setPageNumber(totalPage, currentPage){
        $('#pagination ul li').remove();
        $('#pagination p').text("");
        var itemPage = '';

        var count = 5;
        var startPage = Math.max(1, parseInt(currentPage) - count);
        var endPage = Math.min( totalPage, parseInt(currentPage) + count);
        console.log("startpage "+startPage);
        console.log("endpage "+endPage);
        console.log("currentpage "+currentPage);
        if(currentPage>6){
            itemPage += "<li class=\"page-item\"> <a data-pagenumber=\"1\" class=\"page-link\" href=\"#\">First Page</a></li>";
        }

        for(i = startPage; i <= endPage; i++) {
            if(currentPage == i){
                console.log("true");
                itemPage += "<li class=\"page-item disabled\"> <a data-pagenumber="+i+" class=\"page-link\" href=\"#\" tabindex=\"-1\">"+i+"</a></li>";
            }else{
                console.log("false");
                itemPage += "<li class=\"page-item\"> <a data-pagenumber="+i+" class=\"page-link\" href=\"#\">"+i+"</a></li>";
            }
        }

        if(currentPage < totalPage-6 ){
            itemPage += "<li class=\"page-item\"> <a data-pagenumber="+totalPage+" class=\"page-link\" href=\"#\">Last Page</a></li>";
        }


        $('#pagination ul').append(itemPage);
        $('#pagination p').text("Showing page "+currentPage+" of "+ totalPage +" pages");

    }

    function delay(callback, ms) {
        var timer = 0;
        return function() {
            var context = this, args = arguments;
            clearTimeout(timer);
            timer = setTimeout(function () {
                callback.apply(context, args);
            }, ms || 0);
        };
    }

</script>
@endsection
