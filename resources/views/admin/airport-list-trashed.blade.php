@extends('admin/_layout')

@section('title', 'Smarteye - Airport List Trashed')

@section('css')

@endsection

@section('content')
<div class="content-wrapper">
    <h3 class="page-heading mb-4">Airport (Trash)</h3>

    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">

                    <div class="table-responsive">
                        <table class="table center-aligned-table data-table table-fit">
                            <thead>
                            <tr class="text-primary">
                                <th>Airport Code</th>
                                <th>Airport Name</th>
                                <th>Created Date</th>
                                <th>Updated Date</th>
                                <th>Deleted Date</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($airports as $key => $airport)
                            <tr class="item-airport">
                                <td>{{ $airport->airportcode }}</td>
                                <td>{{ $airport->airportname }}</td>
                                <td>{{ $airport->created_at }}</td>
                                <td>{{ $airport->updated_at }}</td>
                                <td>{{ $airport->deleted_at }}</td>
                                <td>
                                    <a href="{{ action('Admin\AirportController@restore', $airport->airportcode) }}" data-airportcode="{{$airport->airportcode}}" data-airportname="{{$airport->airportname}}" class="btn btn-success btn-sm restore-button">Restore</a>
                                    <a href="{{ action('Admin\AirportController@destroyPermanent', $airport->airportcode) }}" data-airportcode="{{$airport->airportcode}}" data-airportname="{{$airport->airportname}}" class="btn btn-danger btn-sm delete-permanent-button">Delete Permanent</a>
                                </td>
                            </tr>

                            @endforeach

                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>

    </div>

</div>
@endsection

@section('js')
<script type="text/javascript">
    $(document).ready(function () {
        var airportcode;
        var airportname;

        $('.restore-button').on('click', function () {
            airportcode = $(this).data('airportcode');
            airportname = $(this).data('airportname');

            return confirm("Are you sure, you want to restore airport "+ airportname + "("+airportcode+")" );
        });

        //delete data
        $('.delete-permanent-button').click(function(){

            airportcode = $(this).data('airportcode');
            airportname = $(this).data('airportname');

            return confirm("Are you sure, you want to delete permanent airport "+ airportname + "("+airportcode+")" );

        })
    });//END DOCUMENT READY

</script>
@endsection
