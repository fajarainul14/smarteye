@extends('admin/_layout')

@section('title', 'Smarteye - Priority List Trashed')

@section('css')

@endsection

@section('content')
<div class="content-wrapper">
    <h3 class="page-heading mb-4">Priority (Trash)</h3>

    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">

                    <div class="row">
                        <div class="col-md-6">
                            @include('admin._filter_master')
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table class="table center-aligned-table data-table table-fit" id="tableData">
                            <thead>
                            <tr class="text-primary">
                                <th>ID</th>
                                <th>Description</th>
                                <th>Airport</th>
                                <th>Created Date</th>
                                <th>Updated Date</th>
                                <th>Deleted Date</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>

                            {{--@foreach($priorities as $key => $priority)
                            <tr class="item-priority">
                                <td>{{ $priority->id }}</td>
                                <td>{{ $priority->description }}</td>
                                <td>{{ $priority->airport->airportname }}</td>
                                <td>{{ $priority->created_at }}</td>
                                <td>{{ $priority->updated_at }}</td>
                                <td>{{ $priority->deleted_at }}</td>
                                <td>
                                    <a href="{{ action('Admin\PriorityController@restore', $priority->id) }}" data-id="{{$priority->id}}" data-description="{{$priority->description}}" class="btn btn-success btn-sm restore-button" >Restore</a>
                                    <a href="{{ action('Admin\PriorityController@destroyPermanent', $priority->id) }}" data-id="{{$priority->id}}" data-description="{{$priority->description}}" class="btn btn-danger btn-sm delete-permanent-button">Delete Permanent</a>
                                </td>
                            </tr>

                            @endforeach--}}

                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>

    </div>

</div>
@endsection

@section('js')
<script type="text/javascript">
    var id;
    var description;
    var isTrash = true;
    var url = "{{action('Admin\\PriorityController@getTrashedDataPriority')}}";
    var urlRestore = "{{ url('admin/priority/restore/') }}";
    var urlDeletePermanent = "{{ url('admin/priority/forcedelete/') }}";

    $(document).ready(function () {
        $('#please-wait').hide();
        $('#filter-description').val('');
        $('#filter-airportcode').val('');

        prepareGetData();

        $(document).on('click', '.restore-button', function(event) {
            id = $(this).data('id');
            description = $(this).data('description');

            return confirm("Are you sure, you want to restore priority "+ description + "("+id+")" );
        });

        //delete data
        $(document).on('click', '.delete-permanent-button', function(event) {

            id = $(this).data('id');
            description = $(this).data('description');

            return confirm("Are you sure, you want to delete permanent priority "+ description + "("+id+")" );

        });

    });//END DOCUMENT READY

</script>
@endsection
