<div id="form-categories">

    <h5 class="card-title">Categories</h5>
    <p>Please select category(ies) foreach airports</p>

    <div class="row">

        <div class="col-md-12" style="margin-top: 24px;" id="checkbox-categories">

            @include('admin._please_wait')

            <div class="row">

            </div>

        </div>

    </div>

    <button class="btn btn-outline-danger" id="button-form-categories-prev"><span>Prev</span>
    </button>

    <button class="btn btn-outline-primary" id="button-form-categories-next" disabled="disabled"><span>Finish</span>
    </button>

</div>
