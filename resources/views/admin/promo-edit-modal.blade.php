<!-- Modal -->
<div class="modal fade" id="editPromoModal" tabindex="-1" role="dialog" aria-labelledby="editPromoModal"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{ action('Admin\PromoController@update') }}" method="post" id="editPromoModal" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="modal-header">
                    <h5 class="modal-title" id="editPromoModal">Edit Promo</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    @include('form.input', ['label'=>'', 'name'=>'id', 'type'=>'hidden', 'placeholder'=>'', 'required'=>'true', 'class'=>'col-md-12'])
                    @include('form.select', ['label'=>'Airport *', 'name'=>'airportcode', 'required'=>'true', 'placeholder'=>'Airport', 'class'=>'col-md-12', 'options'=> convertObject($master->grantedAirport, 'airportcode', 'airportname') ])
                    @include('form.input', ['label'=>'Promo Name *', 'name'=>'promoname', 'type'=>'text', 'placeholder'=>'Promo Name', 'required'=>'true', 'class'=>'col-md-12'])
                    @include('form.input', ['label'=>'Promo Text *', 'name'=>'promotext', 'type'=>'text', 'placeholder'=>'Promo Text', 'required'=>'true', 'class'=>'col-md-12'])
                    @include('form.input-image', ['label'=>'Promo Image *', 'name'=>'promoimage', 'id'=>'promoimageedit', 'id_preview'=>'image-preview-edit', 'required'=>'false'])
                    <!--<button id="button-change-image">Change Image</button>
                    <button id="button-cancel-change-image">Cancel Change Image</button>-->

                    <div class="form-group">

                        <div class="form-check" style="padding-left: 0px;margin-top: 6px">
                            <label>
                                <input type="checkbox" name="is_change_image" value="1"> <span class="label-text" style="font-size: 16px">Change Image</span>
                            </label>
                        </div>

                    </div>

                    <figure class="img-preview text-center">
                        <img src="#" id="id_preview_edit" alt="" style="width: 100%">
                    </figure>

                    @include('form.select', ['label'=>'Floor *', 'name'=>'floorid', 'required'=>'true', 'placeholder'=>'Select Floor', 'class'=>'col-md-12' ])

                        <div class="form-group">

                            <div class="form-check" style="padding-left: 0px;margin-top: 6px">
                                <label>
                                    <input type="checkbox" name="is_show_icon" value="1"> <span class="label-text" style="font-size: 16px">Show Icon *</span>
                                </label>
                            </div>

                        </div>
                    @include('form.select', ['label'=>'Icon *', 'name'=>'iconid', 'required'=>'true', 'placeholder'=>'Select Icon', 'class'=>'col-md-12' ])
                    @include('form.input', ['label'=>'Coordinate X *', 'name'=>'x', 'type'=>'text', 'placeholder'=>'Coordinate X', 'required'=>'true', 'class'=>'col-md-12'])
                    @include('form.input', ['label'=>'Coordinate Y *', 'name'=>'y', 'type'=>'text', 'placeholder'=>'Coordinate Y', 'required'=>'true', 'class'=>'col-md-12'])
                    @include('form.input', ['label'=>'Coordinate Z *', 'name'=>'z', 'type'=>'text', 'placeholder'=>'Coordinate Z', 'required'=>'true', 'class'=>'col-md-12'])


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Close</button>
                    <button class="btn btn-outline-primary" type="submit">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
