@extends('admin/_layout')

@section('title', 'Smarteye - Log List')

@section('css')

@endsection

@section('content')
<div class="content-wrapper">
    <h3 class="page-heading mb-4">Log</h3>

    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">

                    <div class="table-responsive">
                        <table class="table center-aligned-table data-table table-fit">
                            <thead>
                            <tr class="text-primary">
                                <th>Userid</th>
                                <th>Username</th>
                                <th>Email</th>
                                <th>Action</th>
                                <th>Detail</th>
                                <th>Created at</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($logs as $key => $log)


                            <tr class="item-user">
                                <td>{{ $log->userid }}</td>
                                <td>{{ $log->user->username }}</td>
                                <td>{{ $log->user->email }}</td>
                                <td>{{ $log->action }}</td>
                                <td>{{ $log->detail }}</td>
                                <td>{{ $log->created_at }}</td>
                            </tr>

                            @endforeach

                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>

</div>
@endsection

@section('js')
<script type="text/javascript">
    $(document).ready(function () {

    });//END DOCUMENT READY

</script>
@endsection
