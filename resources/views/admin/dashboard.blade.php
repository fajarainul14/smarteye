@extends('admin/_layout')

@section('title', 'Smarteye - Dashboard')

@section('css')

@endsection

@section('content')
<div class="content-wrapper">

    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">

                    <h3>Welcome to Smarteye admin application!</h3>

                </div>
            </div>
        </div>

    </div>

</div>
@endsection

@section('js')
<script type="text/javascript">

</script>
@endsection
