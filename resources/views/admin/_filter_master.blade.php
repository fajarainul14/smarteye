<div class="clearfix"></div>
<div class="form-group row">
    <input type="text" placeholder="Description" class="form-control col-md-5" id="filter-description"/>
    <div class="col-md-1"></div>
    <select name="airportcode" id="filter-airportcode" class="form-control col-md-5">
        <option value="" selected>- Airport -</option>
        @foreach($airports as $key => $airport)
        <option value="{{ $airport->airportcode }}">{{ $airport->airportname }}</option>
        @endforeach
    </select>
</div>

<div id="please-wait">
    <span><p class="text-center">Please wait...</p></span>
</div>
