<div id="form-roles">

    <h5 class="card-title">Roles</h5>

    <div class="form-group">
        @foreach($roles as $key=>$role)

            <div class="form-check" style="padding-left: 0px;margin-top: 6px">
                <label>
                    <input type="checkbox" name="roles[]" value="{{$role->id}}" class="roles"> <span class="label-text" style="font-size: 16px">{{$role->name}}</span>
                </label>
            </div>

            <small class="form-text text-muted">
                {{$role->description}}
            </small>

        @endforeach

        {!! $errors->first('roles', '<p class="text-danger">:message</p>') !!}

    </div>

    <button class="btn btn-outline-danger" id="button-form-roles-prev" ><span>Prev</span>
    </button>

    <button class="btn btn-outline-primary" id="button-form-roles-next" disabled="disabled"><span>Next</span>
    </button>

</div>
