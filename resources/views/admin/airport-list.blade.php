@extends('admin/_layout')

@section('title', 'Smarteye - Airport List')

@section('css')

@endsection

@section('content')
<div class="content-wrapper">
    <h3 class="page-heading mb-4">Airport</h3>

    <div class="row">
        <div class="col-lg-6 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">

                    <div style="float:right; margin-bottom: 20px;">
                        <a href="#" class="btn btn-primary btn-md" id="button-add">Add
                            Airport</a>
                    </div>

                    <div class="clearfix"></div>
                    <div class="form-group row">
                        <input type="text" placeholder="Airport code" class="form-control col-md-5" id="filter-code"/>
                        <div class="col-md-1"></div>
                        <input type="text" placeholder="Aiport name" class="form-control col-md-5" id="filter-name"/>
                    </div>

                    <div id="please-wait">
                        <span><p class="text-center">Please wait...</p></span>
                    </div>

                    <div class="table-responsive">
                        <table class="table center-aligned-table data-table" id="tableAirport">
                            <thead>
                            <tr class="text-primary">
                                <th>Airport Code</th>
                                <th>Airport Name</th>
                            </tr>
                            </thead>
                            <tbody>

                            {{--@foreach($airports as $key => $airport)
                            <tr class="item-airport">
                                <td>{{ $airport->airportcode }}</td>
                                <td>{{ $airport->airportname }}</td>
                            </tr>

                            @endforeach--}}

                            </tbody>
                        </table>
                    </div>

                </div>

                <nav aria-label="pagination-poi" style="margin-right: 16px" id="pagination">
                    <span><p style="text-align: right"></p></span>
                    <ul class="pagination justify-content-end">

                    </ul>
                </nav>

            </div>
        </div>

        <div class="col-lg-6 grid-margin stretch-card" id="form-container">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Add Airport</h4>
                    <form action="#" method="post">
                        {{ csrf_field() }}

                        @include('form.input', ['label'=>'Airport Code', 'name'=>'airportcode', 'type'=>'text', 'placeholder'=>'Airport Code', 'required'=>'true', 'class'=>'col-md-12'])
                        @include('form.input', ['label'=>'Airport Name *', 'name'=>'airportname', 'type'=>'text', 'placeholder'=>'Airport Name', 'required'=>'true', 'class'=>'col-md-12'])
                        @include('form.input', ['name'=>'oldairportcode', 'type'=>'hidden', 'required'=>'true'])

                        <br>

                        <a href="#" class="btn btn-outline-danger" id="button-delete"><span>Delete</span></a>

                        <button class="btn btn-outline-primary" type="submit" id="button-form"><span>Save</span></button>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection

@section('js')
<script type="text/javascript">
    var airportcode;
    var airportname;
    var filterCode = '';
    var filterName = '';
    var pageNumber = 1;

    $(document).ready(function () {

        $('#please-wait').hide();
        $('#filter-code').val('');
        $('#filter-name').val('');

        $('#form-container').hide();

        getDataAirport();

        $('#button-add').click(function(){
            $('#form-container input[name="airportname"]').val('');
            $('#form-container input[name="airportcode"]').val('');
            $('#form-container input[name="oldairportcode"]').val('');

            $('#form-container').show()
            $('#form-container .card-title').text('Add Airport')
            $('#button-form').text('Save')
            $('#button-delete').hide()

            $('#form-container form').attr('action', "{{ action('Admin\AirportController@store') }}");
        })

        $(document).on('click', '.item-airport', function(event) {
            airportcode = $(this).find('td:eq(0)').text();
            airportname = $(this).find('td:eq(1)').text();

            $('#form-container input[name="airportname"]').val(airportname);
            $('#form-container input[name="airportcode"]').val(airportcode);
            $('#form-container input[name="oldairportcode"]').val(airportcode);

            $('#form-container').show();
            $('#form-container .card-title').text('Edit Airport');
            $('#button-form').text('Update');
            $('#button-delete').show();

            $('#form-container form').attr('action', "{{ action('Admin\AirportController@update') }}");

            var deleteUrl = '{{ url("/admin/airport/delete") }}';

            $('#button-delete').attr('href', deleteUrl+'/'+airportcode );

        });

        //delete data
        $('#button-delete').click(function(){

            return confirm("Are you sure, you want to delete airport "+ airportname + "("+airportcode+")" );

        })

        //FILTER
        $('#filter-code, #filter-name').keyup(delay(function (e) {
            filterCode = $('#filter-code').val();
            filterName = $('#filter-name').val();
            pageNumber = 1;
            getDataAirport();
        }, 3000));

        $(document).on('click', '#pagination li a', function() {
            pageNumber = $(this).data('pagenumber');
            getDataAirport();
        }) ;

    });//END DOCUMENT READY


    function getDataAirport(){

        var data = {_token: '{{csrf_token()}}', page_number:pageNumber};
        if(filterName!==''){
            data.airportname = filterName;
        }

        if(filterCode!=null){
            data.airportcode = filterCode;
        }

        console.log(data);

        $('#please-wait').show();
        var request = $.ajax({
            url: "{{action('Admin\\AirportController@getDataAirport')}}",
            type: "POST",
            data: data,
            dataType: "json"
        });

        request.done(function(response) {
            $('#please-wait').hide();
            console.log(response);

            if(response.success){
                var dataAirport = response.data;
                var totalPage = response.total_page;
                var currentPage = response.current_page;

                setDataResponseAirport(dataAirport);
                setPageNumber(totalPage, currentPage);

            }else{
                console.log("error occured");
            }

        });

        request.fail(function(jqXHR, textStatus) {
            $('#please-wait').hide();
            console.log( "Request failed: " + textStatus );
            console.log(jqXHR);
        });

    }

    function setDataResponseAirport(data){
        $('#tableAirport tbody').empty();
        for(var i=0; i< data.length; i++){
            var row = "<tr class=\"item-airport\">";
            row += "<td>"+data[i].airportcode+"</td>";
            row += "<td>"+data[i].airportname+"</td>";
            row += "</tr>";
            $('#tableAirport tbody').append(row)
        }
    }

</script>
@endsection
