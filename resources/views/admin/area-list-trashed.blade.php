@extends('admin/_layout')

@section('title', 'Smarteye - Area List Trashed')

@section('css')

@endsection

@section('content')
<div class="content-wrapper">
    <h3 class="page-heading mb-4">Area (Trash)</h3>

    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">

                    <div class="row">
                        <div class="col-md-6">
                            @include('admin._filter_master')
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table class="table center-aligned-table data-table table-fit" id="tableData">
                            <thead>
                            <tr class="text-primary">
                                <th>ID</th>
                                <th>Description</th>
                                <th>Airport</th>
                                <th>Created Date</th>
                                <th>Updated Date</th>
                                <th>Deleted Date</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>

                            {{--@foreach($areas as $key => $area)
                            <tr class="item-area">
                                <td>{{ $area->id }}</td>
                                <td>{{ $area->description }}</td>
                                <td>{{ $area->airport->airportname }}</td>
                                <td>{{ $area->created_at }}</td>
                                <td>{{ $area->updated_at }}</td>
                                <td>{{ $area->deleted_at }}</td>
                                <td>
                                    <a href="{{ action('Admin\AreaController@restore', $area->id) }}" data-id="{{$area->id}}" data-description="{{$area->description}}" class="btn btn-success btn-sm restore-button" >Restore</a>
                                    <a href="{{ action('Admin\AreaController@destroyPermanent', $area->id) }}" data-id="{{$area->id}}" data-description="{{$area->description}}" class="btn btn-danger btn-sm delete-permanent-button">Delete Permanent</a>
                                </td>
                            </tr>

                            @endforeach--}}

                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>

    </div>

</div>
@endsection

@section('js')
<script type="text/javascript">
    var id;
    var description;
    var isTrash = true;
    var url = "{{action('Admin\\AreaController@getTrashedDataArea')}}";
    var urlRestore = "{{ url('admin/area/restore/') }}";
    var urlDeletePermanent = "{{ url('admin/area/forcedelete/') }}";

    $(document).ready(function () {
        $('#please-wait').hide();
        $('#filter-description').val('');
        $('#filter-airportcode').val('');

        prepareGetData();

        $(document).on('click', '.restore-button', function(event) {
            id = $(this).data('id');
            description = $(this).data('description');

            return confirm("Are you sure, you want to restore area "+ description + "("+id+")" );
        });

        //delete data
        $(document).on('click', '.delete-permanent-button', function(event) {

            id = $(this).data('id');
            description = $(this).data('description');

            return confirm("Are you sure, you want to delete permanent area "+ description + "("+id+")" );

        })

    });//END DOCUMENT READY

</script>
@endsection
