<div id="form-airports">

    <h5 class="card-title">Airports</h5>
    <p>Please select airport(s) where this new user will be assigned</p>

    <div class="row">


        <div class="col-md-6">

            <input type="text" placeholder="Airport" class="form-control col-md-12" id="filter-airport"/>

        </div>
        <div class="clearfix"></div>

        <div class="col-md-12" style="margin-top: 24px;">

            @include('admin._please_wait')

            <div id="checkbox-airports">

                <div class="form-check" style="padding-left: 0px;margin-top: 6px">
                    <label>
                        <input type="checkbox" name="airports[]" value="-1" id="checkbox-admin-all"> <span class="label-text" style="font-size: 16px">Add as "Admin Pusat"</span>
                    </label>
                </div>

                <small class="form-text text-muted">
                    "Admin Pusat" can manage data on all airport
                </small>

                <div class="form-check" style="padding-left: 0px;margin-top: 6px;">
                    <label>
                        <input type="checkbox" id="checkbox-airport-all"> <span class="label-text" style="font-size: 16px">Select All</span>
                    </label>
                </div>

                <div class="row" style="height: 300px;overflow: auto; margin-top: 16px; margin-bottom: 16px">

                </div>

            </div>

        </div>

    </div>

    <button class="btn btn-outline-danger" id="button-form-airports-prev"><span>Prev</span>
    </button>

    <button class="btn btn-outline-primary" id="button-form-airports-next" disabled="disabled"><span>Next</span>
    </button>

</div>
