<!-- Modal -->
<div class="modal fade" id="editPoiModal" tabindex="-1" role="dialog" aria-labelledby="editPoiModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{ action('Admin\PoiController@update') }}" method="post" id="editPoiModal">
                {{ csrf_field() }}
                <div class="modal-header">
                    <h5 class="modal-title" id="editPoiModalLabel">Edit POI</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    @include('form.input', ['label'=>'', 'name'=>'id', 'type'=>'hidden', 'placeholder'=>'', 'required'=>'true', 'class'=>'col-md-12'])
                    @include('form.input', ['label'=>'POI Name *', 'name'=>'name', 'type'=>'text', 'placeholder'=>'POI Name', 'required'=>'true', 'class'=>'col-md-12'])
                    @include('form.input', ['label'=>'Coordinate X *', 'name'=>'x', 'type'=>'text', 'placeholder'=>'Coordinate X', 'required'=>'true', 'class'=>'col-md-12'])
                    @include('form.input', ['label'=>'Coordinate Y *', 'name'=>'y', 'type'=>'text', 'placeholder'=>'Coordinate Y', 'required'=>'true', 'class'=>'col-md-12'])
                    @include('form.input', ['label'=>'Coordinate Z *', 'name'=>'z', 'type'=>'text', 'placeholder'=>'Coordinate Z', 'required'=>'true', 'class'=>'col-md-12'])
                    @include('form.select', ['label'=>'Airport *', 'name'=>'airportcode', 'required'=>'true', 'placeholder'=>'Airport', 'class'=>'col-md-12', 'options'=> convertObject($master->grantedAirport, 'airportcode', 'airportname') ])
                    @include('form.select', ['label'=>'Area *', 'name'=>'areaid', 'required'=>'true', 'class'=>'col-md-12', 'placeholder'=>'Select Area', ])
                    @include('form.select', ['label'=>'Category *', 'name'=>'categoryid', 'required'=>'true', 'class'=>'col-md-12', 'placeholder'=>'Select Category', ])
                    @include('form.select', ['label'=>'Priority *', 'name'=>'priorityid', 'required'=>'true', 'class'=>'col-md-12', 'placeholder'=>'Select Priority', ])
                    @include('form.select', ['label'=>'Floor *', 'name'=>'floorid', 'required'=>'true', 'class'=>'col-md-12', 'placeholder'=>'Select Floor', ])
                    @include('form.select', ['label'=>'Icon *', 'name'=>'iconid', 'required'=>'true', 'class'=>'col-md-12', 'placeholder'=>'Select Icon', ])



                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Close</button>
                    <button class="btn btn-outline-primary" type="submit">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
