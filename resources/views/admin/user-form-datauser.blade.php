<div id="form-datauser">

    <h5 class="card-title">Data User</h5>

        @include('form.input', ['label'=>'Username *', 'name'=>'username', 'type'=>'text',
        'placeholder'=>'Username', 'required'=>'true', 'class'=>'col-md-12'])
        @include('form.input', ['label'=>'Email *', 'name'=>'email', 'type'=>'email',
        'placeholder'=>'Email', 'required'=>'true', 'class'=>'col-md-12'])
        @include('form.input', ['label'=>'Password *', 'name'=>'password', 'type'=>'text',
        'placeholder'=>'Password', 'required'=>'true', 'class'=>'col-md-12', 'value'=>'password'])
        @include('form.input', ['name'=>'userid', 'type'=>'hidden', 'required'=>'true'])

        {{--<a href="#" data-target="#selectAirportModal" data-toggle="modal">Select Airport</a>--}}
        {{--<br>--}}

        <a href="#" class="btn btn-outline-danger" id="button-delete"><span>Delete</span></a>

        <button class="btn btn-outline-primary" id="button-form-datauser-next"><span>Next</span>
        </button>

</div>
