<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>@yield('title')</title>

    <!-- plugins:css -->
    <link rel="stylesheet" href=" {{asset('bootstrap/vendors/iconfonts/mdi/css/materialdesignicons.min.css')}} ">
    <link rel="stylesheet" href=" {{asset('bootstrap/vendors/css/vendor.bundle.base.css')}}" >
    <link rel="stylesheet" href=" {{asset('bootstrap/vendors/css/vendor.bundle.addons.css')}}" >
    <!-- endinject -->
    <!-- plugin css for this page -->
    <link rel="stylesheet" href=" {{asset('bootstrap/vendors/iconfonts/font-awesome/css/font-awesome.css')}} ">
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href=" {{asset('bootstrap/css/style.css')}} ">


    <link rel="stylesheet" href=" {{asset('css/style.css')}} ">
    <!-- endinject -->
    <link rel="shortcut icon" href=" {{asset('bootstrap/images/favicon.png')}} " />
    @yield('css')

</head>

<body>
<div class="container-scroller">
    <!-- partial:../../partials/_navbar.html -->

    @include('admin/_navbar')

    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
        <!-- partial:../../partials/_sidebar.html -->

        @include('admin/_sidebar')

        <!-- partial -->
        <div class="main-panel">

            @yield('content')
            <!-- content-wrapper ends -->
            <!-- partial:../../partials/_footer.html -->

            @include('admin/_footer')

            <!-- partial -->
        </div>
        <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->

    @include('admin/_message')

</div>


<script type="text/javascript" src="{{ asset('js/jquery-3.1.1.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>

<!-- plugins:js -->
<script src="{{asset('bootstrap/vendors/js/vendor.bundle.base.js')}}"></script>
<script src="{{asset('bootstrap/vendors/js/vendor.bundle.addons.js')}}"></script>
<!-- endinject -->
<!-- inject:js -->
<script src="{{asset('bootstrap/js/off-canvas.js')}}"></script>
<script src="{{asset('bootstrap/js/misc.js')}}"></script>
<!-- endinject -->

@yield('js')

<script type="text/javascript">

    var filterDesc = '';
    var filterCode = '';
    var pageNumber = 1;

    $(document).ready(function(){
        $('.nav li a').each(function(){
            var $this = $(this);
            $this.removeClass('active');
            if($this.attr('href') === location.toString()){
                $this.addClass('active');
            }
        })


        //FILTER
        $('#filter-description').keyup(delay(function (e) {
            filterDesc = $('#filter-description').val();
            pageNumber = 1;
            prepareGetData();
        }, 3000));

        $('#filter-airportcode').on('change', function(){
            filterCode = $(this).val();
            pageNumber = 1;
            prepareGetData();
        });

        //pagination click
        $(document).on('click', '#pagination li a', function() {
            pageNumber = $(this).data('pagenumber');
            prepareGetData();
        }) ;

    });

    /* Sidebar menu */
    $('.main-menu > li').on('click', function(){
        var active_menu_height = $('.active > .sub-menu').height();
        var menu_height = $(this).find('.sub-menu').height();

        if((active_menu_height != 0) && ((menu_height == 0 || menu_height == null))){
            /* Menon-aktifkan menu lain yang aktif */
            $('.active > .sub-menu').animate({
                'height': '0px',
                'opacity': '0'
            }, 400);
            $('.active > .sub-menu').fadeOut();
            $('.active').removeClass('active');
        }

        /* Memunculkan submenu di sidebar */
        if(menu_height == 0){
            $(this).find('.sub-menu').show();
            $(this).find('.sub-menu').animate({
                'height': '70px',
                'opacity': '1'
            }, 400);
            $(this).addClass('active');
        }
        else{
            $(this).find('.sub-menu').animate({
                'height': '0px',
                'opacity': '0'
            }, 400);
            $(this).find('.sub-menu').fadeOut();
            $('.active').removeClass('active');
        }
    });

    // display notification if message not null
    if ($('.pop-up').children().length > 0) {
        $('.pop-up').animate({
            'opacity': 1,
            'right': "30px"
        }, 300).animate({ 'right': "15px" }, 400);

        // close notification by time
        setTimeout(close_pop_up, 7000);
    }
    // close notification function
    function close_pop_up(){
        var width = $('.pop-up').width();
        $('.pop-up').animate({
            'opacity': 0,
            'right': -width
        }, 300, function(){
            $('.pop-up').remove();
        });
    }

    // close notification
    $('.pop-up .close').click(close_pop_up);

    // remove notification when clicked out of target
    $(document).click(function(e) {
        var pop_up = $('.pop-up');
        if (!pop_up.is(e.target)) {
            close_pop_up();
        }
    });

    function prepareGetData(){
        var data = {_token: '{{csrf_token()}}', page_number:pageNumber};
        if(filterDesc!==''){
            data.description = filterDesc;
        }
        if(filterCode!=null && filterCode!==''){
            data.airportcode = [filterCode];
        }
        _getData(url, data);
    }

    function _getData(url, data){
        console.log(data);

        $('#please-wait').show();
        var request = $.ajax({
            url: url,
            type: "POST",
            data: data,
            dataType: "json"
        });

        request.done(function(response) {
            $('#please-wait').hide();
            console.log(response);

            if(response.success){
                var dataResult = response.data;
                var totalPage = response.total_page;
                var currentPage = response.current_page;

                setDataResponse(dataResult);
                setPageNumber(totalPage, currentPage);

            }else{
                alert("error occured, please try again");
                console.log("error occured");
            }

        });

        request.fail(function(jqXHR, textStatus) {
            $('#please-wait').hide();
            alert("error occured, please try again");
            console.log( "Request failed: " + textStatus );
            console.log(jqXHR);
        });
    }

    //function for pagination data master
    function setDataResponse(data){
        $('#tableData tbody').empty();
        if(isTrash){
            for(var i=0; i< data.length; i++) {
                var row = "<tr >";
                row += "<td>" + data[i].id + "</td>";
                row += "<td>" + data[i].description + "</td>";
                row += "<td>" + data[i].airport.airportname + "</td>";
                row += "<td>" + data[i].created_at + "</td>";
                row += "<td>" + data[i].updated_at + "</td>";
                row += "<td>" + data[i].deleted_at + "</td>";
                row += "<td>";
                row += "<td>";
                row += "<a href=\"" + urlRestore + "/" + data[i].id + "\" data-id=\"" + data[i].id + "\" data-description=\"" + data[i].description + "\" class=\"btn btn-success btn-sm restore-button\" >Restore</a> ";
                row += "<a href=\"" + urlDeletePermanent + "/" + data[i].id + "\" data-id=\"" + data[i].id + "\" data-description=\"" + data[i].description + "\" class=\"btn btn-danger btn-sm delete-permanent-button\" >Delete Permanent</a>";
                row += "</tr>";
                $('#tableData tbody').append(row)
            }

        }else{
            for(var i=0; i< data.length; i++){
                var row = "<tr class=\"item-_master\">";
                row += "<td>"+data[i].id+"</td>";
                row += "<td>"+data[i].description+"</td>";
                row += "<td>"+data[i].airport.airportname+"</td>";
                row += "</tr>";
                $('#tableData tbody').append(row)
            }
        }
    }

    function setPageNumber(totalPage, currentPage){
        $('#pagination ul li').remove();
        $('#pagination p').text("");
        var itemPage = '';

        var count = 5;
        var startPage = Math.max(1, parseInt(currentPage) - count);
        var endPage = Math.min( totalPage, parseInt(currentPage) + count);
        console.log("startpage "+startPage);
        console.log("endpage "+endPage);
        console.log("currentpage "+currentPage);
        if(currentPage>6){
            itemPage += "<li class=\"page-item\"> <a data-pagenumber=\"1\" class=\"page-link\" href=\"#\">First Page</a></li>";
        }

        for(i = startPage; i <= endPage; i++) {
            if(currentPage == i){
                console.log("true");
                itemPage += "<li class=\"page-item disabled\"> <a data-pagenumber="+i+" class=\"page-link\" href=\"#\" tabindex=\"-1\">"+i+"</a></li>";
            }else{
                console.log("false");
                itemPage += "<li class=\"page-item\"> <a data-pagenumber="+i+" class=\"page-link\" href=\"#\">"+i+"</a></li>";
            }
        }

        if(currentPage < totalPage-6 ){
            itemPage += "<li class=\"page-item\"> <a data-pagenumber="+totalPage+" class=\"page-link\" href=\"#\">Last Page</a></li>";
        }


        $('#pagination ul').append(itemPage);
        $('#pagination p').text("Showing page "+currentPage+" of "+ totalPage +" pages");

    }

    function delay(callback, ms) {
        var timer = 0;
        return function() {
            var context = this, args = arguments;
            clearTimeout(timer);
            timer = setTimeout(function () {
                callback.apply(context, args);
            }, ms || 0);
        };
    }

</script>

</body>

</html>
