@extends('admin/_layout')

@section('title', 'Smarteye - Promo List')

@section('css')
<style>
    .form-filter {
        margin-left: 8px;
        margin-right: 8px;
    }

    #filter-div {
        margin-top: 20px;
        margin-bottom: 20px;
        text-align: center;
    }

    .modal-content {
        background-color: #fff;
    }

    #promoimagemodal {
        display: block;
        margin-left: auto;
        margin-right: auto;
    }

</style>
@endsection

@section('content')
<div class="content-wrapper">
    <h3 class="page-heading mb-4">Promo</h3>

    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">

                    <form id="formDeletePromo" action="{{ action('Admin\PromoController@destroy') }}" method="post">
                        {{ csrf_field() }}

                    </form>

                    <div style="margin-bottom: 20px;">
                        <a href="#" class="btn btn-primary btn-md" id="button-add" data-toggle="modal"
                           data-target="#addPromoModal">Add Promo</a>

                        <a href="#" class="btn btn-danger btn-md" id="deletePromo">Delete</a>

                        <a href="#" class="btn btn-info btn-md" id="button-filter">Filter</a>

                    </div>

                    <div class="form-row" id="filter-div">
                        <div class="col col-md-3" style="margin-top: 26px">
                            @include('form.input', ['label'=>'', 'name'=>'name', 'type'=>'text', 'placeholder'=>'Name',
                            'required'=>'true', 'class'=>'col-md-12', 'id'=>'filter-name'])
                        </div>
                        <div class="col col-md-3">
                            @include('form.select', ['label'=>'', 'name'=>'airportcode', 'placeholder'=>'Airport',
                            'required'=>'true', 'class'=>'col-md-12', 'options'=> convertObject($master->grantedAirport, 'airportcode',
                            'airportname'), 'id'=>'filter-airport' ])
                        </div>
                        <div class="col col-md-3">
                            @include('form.select', ['label'=>'', 'name'=>'floorid', 'placeholder'=>'Floor',
                            'required'=>'true', 'class'=>'col-md-12', 'id'=>'filter-floor' ])
                        </div>
                    </div>

                    @include('admin/_please_wait')

                    <div class="table-responsive">
                        <table class="table center-aligned-table data-table table-fit" id="tablePromo">
                            <thead>
                            <tr class="text-primary">
                                <th></th>
                                <th>Airport</th>
                                <th>ID</th>
                                <th>Promo Name</th>
                                <th>Promo Text</th>
                                <th>Promo Image</th>
                                <th>Floor</th>
                                <th>Show Icon</th>
                                <th>Icon</th>
                                <th>X</th>
                                <th>Y</th>
                                <th>Z</th>
                            </tr>
                            </thead>
                            <tbody>

                            {{--@foreach($promos as $key => $promo)

                            <tr class="item-promo">
                                <td><input class="except"type="checkbox" value="{{$promo->id}}"/></td>
                                <td>{{ $promo->airport->airportname }}</td>
                                <td>{{ $promo->id }}</td>
                                <td>{{ $promo->promoname }}</td>
                                <td>{{ $promo->promotext }}</td>
                                <td class="promoimage" data-promoimage="{{ asset($promo->promoimage) }}" data-toggle="modal"
                                    data-target="#promoImageModal">Click to show image</td>
                                <td>{{ $promo->floor->description }}</td>
                                @if($promo->showicon)
                                    <td><span class="badge badge-success">True</span></td>
                                @else
                                    <td><span class="badge badge-warning">False</span></td>
                                @endif
                                <td>{{ $promo->icon->description }}</td>
                                <td>{{ $promo->x }}</td>
                                <td>{{ $promo->y }}</td>
                                <td>{{ $promo->z }}</td>
                            </tr>

                            @endforeach--}}

                            </tbody>
                        </table>
                    </div>

                </div>

                <nav aria-label="pagination-poi" style="margin-right: 16px" id="pagination">
                    <span><p style="text-align: right"></p></span>
                    <ul class="pagination justify-content-end">
                        {{--@if($totalPage>1)

                        <!--@if($totalPage<=6)

                            @for($i=1; $i<= $totalPage; $i++)

                                @if($currentPage == $i)
                                    <li class="page-item disabled"> <a data-pagenumber="{{$i}}" class="page-link" href="#" tabindex="-1">{{$i}}</a></li>
                                @else
                                    <li class="page-item"> <a data-pagenumber="{{$i}}" class="page-link" href="#">{{$i}}</a></li>

                                @endif

                            @endfor
                        @endif-->
                        @for($i=1; $i<= $totalPage; $i++)

                        @if($currentPage == $i)
                        <li class="page-item disabled"> <a data-pagenumber="{{$i}}" class="page-link" href="#" tabindex="-1">{{$i}}</a></li>
                        @else
                        <li class="page-item"> <a data-pagenumber="{{$i}}" class="page-link" href="#">{{$i}}</a></li>

                        @endif

                        @endfor

                        @endif--}}
                    </ul>
                </nav>
            </div>
        </div>

    </div>

</div>
@include('admin/promo-create-modal')
@include('admin/promo-edit-modal')
@include('admin/promo-image-modal')

@endsection

@section('js')
<script type="text/javascript">
    var filterName = '';
    var filterFloor;
    var filterAirport;
    var currentPage = 1;
    var pageNumber = 1;
    var selectedFloor;
    var selectedIcon;

    $('#please-wait').hide();
    $('#filter-name').val('');
    $('#filter-floor').val('');

    $(document).ready(function () {
        $('select[name="airportcode"]').val('');

        getDataPromo();

        $('#filter-div').hide();

        $('#button-filter').click(function () {

            if ($('#filter-div').is(":visible")) {

                $('#filter-div').hide();

            } else {

                $('#filter-div').show();

            }

        });

        //on select promoimage
        $("#promoimage").change(function() {
            readURL(this, "#image-preview");
        });

        //on select promoimage edit
        $("#promoimageedit").change(function() {
            readURL(this, "#image-preview-edit");
        });

        // preview image before upload
        function readURL(input, id_preview) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $(id_preview).attr('src', e.target.result);
                    $(id_preview).parent().animate({
                        opacity: 1,
                        height: "auto",
                        'margin-bottom': "15px"
                    }, 300);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        //confirmation before add Promo
        $('#addPromoModal').submit(function () {
            var promoName = $('#addPromoModal input[name="promoname"]').val();
            return confirm("Are you sure you want to save Promo ?\n \"(" + promoName + ")\"");
        });

        //show image promo
        $(document).on('click', '.promoimage', function(event) {
            var imageName = $(this).data('promoimage');
            $('#promoimagemodal').attr("src",imageName);
        }) ;

        //confirmation before edit Promo
        $('#editPromoModal').submit(function () {
            var promoName = $('#editPromoModal input[name="promoname"]').val();
            return confirm("Are you sure you want to save Promo ?\n \"(" + promoName + ")\"");
        });

        //onclick item promo
        $(document).on('click', '.item-promo', function(event) {
            if (!$(event.target).hasClass("except") && !$(event.target).hasClass("promoimage")) {

                //hide input image form
                $('#editPromoModal #promoimageedit').hide();

                var airportcode = $(this).find('td:eq(1)').attr('data-airportcode');
                var id = $(this).find('td:eq(2)').text();
                var promoname = $(this).find('td:eq(3)').text();
                var promotext = $(this).find('td:eq(4)').text();
                var promoimage = $(this).find('td:eq(5)').attr('data-promoimage');
                var floor = $(this).find('td:eq(6)').attr('data-floorid');
                var showicon = $(this).find('td:eq(7)').text();
                var icon = $(this).find('td:eq(8)').attr('data-iconid');
                var x = $(this).find('td:eq(9)').text();
                var y = $(this).find('td:eq(10)').text();
                var z = $(this).find('td:eq(11)').text();

                $('#editPromoModal').modal('show');

                $('#editPromoModal input[name="id"]').val(id);
                $('#editPromoModal input[name="promoname"]').val(promoname);
                $('#editPromoModal input[name="promotext"]').val(promotext);
                $('#editPromoModal select[name="airportcode"]').val(airportcode);
                // $('#editPromoModal select[name="floorid"]').val(floor);
                // $('#editPromoModal select[name="iconid"]').val(icon);
                $('#editPromoModal input[name="x"]').val(x);
                $('#editPromoModal input[name="y"]').val(y);
                $('#editPromoModal input[name="z"]').val(z);
                $('#editPromoModal #id_preview_edit').attr("src", promoimage);

                if(showicon==='True'){
                    $('#editPromoModal input[name="is_show_icon"]').attr('checked', true);
                }else{
                    $('#editPromoModal input[name="is_show_icon"]').attr('checked', false);
                }

                selectedFloor = floor;
                selectedIcon = icon;

                getDataByAirport('', airportcode, 'modal-edit');

            }
        }) ;

        //checkbox change image
        $(document).on('click', '#editPromoModal input[name="is_change_image"]', function(event) {
            if($(this).is(":checked")) {

                $('#editPromoModal #promoimageedit').show();

                $('#editPromoModal #id_preview_edit').hide();
                $('#editPromoModal input[name="promoimage"]').attr('required', true);

            }else{
                $('#editPromoModal #promoimageedit').hide();

                $('#editPromoModal #id_preview_edit').show();
                $('#editPromoModal input[name="promoimage"]').attr('required', false);
            }
        }) ;

        //delete promo
        $('#deletePromo').click(function () {
            var ids = [];
            var totalChecked;
            var listPromo = [];

            totalChecked = $('#tablePromo').find('input[type="checkbox"]:checked').length;

            $('#tablePromo').find('tr').each(function () {
                var row = $(this);
                if (row.find('input[type="checkbox"]').is(':checked')){

                    var id = row.find("input[type=checkbox]:checked").val();
                    var promoname = row.find("td:eq(3)").text();
                    console.log(promoname);
                    ids.push(id);
                    listPromo.push(promoname);

                }
            });

            if(ids.length>0){

                $('#formDeletePromo input[name="ids[]"]').remove();
                for (var i =0; i<ids.length ;i++){
                    $('#formDeletePromo').append("<input type=hidden  name=ids[] value="+ids[i]+" />");
                }

                console.log(totalChecked)
                if(totalChecked>2){
                    if(confirm("Are you sure you want to delete Promos")){
                        $('#formDeletePromo').submit();
                    }
                }else{
                    var message = '';

                    for (var i=0; i< totalChecked; i++ ){

                        message = message + "\""+listPromo[i] + "\"\n";
                    }

                    if(confirm("Are you sure you want to delete Promo? \n\n"+message)){
                        $('#formDeletePromo').submit();
                    }
                }
                // $('#formDeletePromo').submit();
            }

        });

        //FILTER
        $('#filter-name').keyup(delay(function (e) {
            filterName = $('#filter-name').val();
            pageNumber = 1;
            getDataPromo();
        }, 3000));

        $('#filter-floor').on('change', function() {
            filterFloor = this.value;
            pageNumber = 1;
            getDataPromo();
        });

        $('#filter-airport').on('change', function() {
            filterAirport = this.value;
            pageNumber = 1;
            getDataPromo();
            getDataByAirport(['floor'], this.value, 'filter');
        });

        $(document).on('click', '#pagination li a', function() {
            pageNumber = $(this).data('pagenumber');
            getDataPromo();
        }) ;

        //on change airport
        //get master
        $('#addPromoModal select[name="airportcode"]').change(function(){

            var airportcode = $(this).val();

            getDataByAirport('', airportcode, 'modal-add');

            $('#addPromoModal select').not(this).val('');

        })

        $(' #editPromoModal select[name="airportcode"]').change(function(){

            var airportcode = $(this).val();

            getDataByAirport('', airportcode, 'modal-edit');

            $(' #editPromoModal select').not(this).val('');

        })
    });//END DOCUMENT READY

    function getDataByAirport(masterType, airportcode, position){

        var containerFloorFilter = $('#filter-floor');
        var defaultValueFloorFilter = "<option value=\"\">- Floor -</option>";

        var containerFloor = $('#addPromoModal select[name="floorid"]');
        var defaultValueFloor = "<option value=\"\">- Select Floor -</option>";

        var containerIcon = $('#addPromoModal select[name="iconid"]');
        var defaultValueIcon = "<option value=\"\">- Select Icon -</option>";

        var containerFloorEdit = $('#editPromoModal select[name="floorid"]');
        var defaultValueFloorEdit = "<option value=\"\">- Select Floor -</option>";

        var containerIconEdit = $('#editPromoModal select[name="iconid"]');
        var defaultValueIconEdit = "<option value=\"\">- Select Icon -</option>";

        var data = {
            _token: '{{csrf_token()}}',
            airportcode : airportcode,
            filter : masterType
        };

        var request = $.ajax({
            url: "{{action('Admin\\MasterController@getDataByAirportCode')}}",
            type: "POST",
            data: data,
            dataType: "json"
        });

        console.log(data);

        request.done(function(response) {
            console.log(response);

            if(response.success){
                var dataFloor = response.data.floors;
                var dataIcon = response.data.icons;

                if(position==='filter'){
                    setDataMasterOption(dataFloor, containerFloorFilter, defaultValueFloorFilter);
                }else if(position === 'modal-edit'){
                    setDataMasterOption(dataFloor, containerFloorEdit, defaultValueFloorEdit, selectedFloor);
                    setDataMasterOption(dataIcon, containerIconEdit, defaultValueIconEdit, selectedIcon);
                }else{
                    setDataMasterOption(dataFloor, containerFloor, defaultValueFloor);
                    setDataMasterOption(dataIcon, containerIcon, defaultValueIcon);
                }

            }else{
                alert("error occured, please try again");
                console.log("error occured");
            }

        });

        request.fail(function(jqXHR, textStatus) {
            alert("error occured, please try again");
            console.log( "Request failed: " + textStatus );
            console.log(jqXHR);
        });

    }

    function setDataMasterOption(data, container, defaultValue, selectedId){
        container.empty().append(defaultValue);
        for(var i=0; i< data.length; i++) {
            var option = "";
            if(selectedId == data[i].id){
                option += "<option value=\""+data[i].id+"\" selected>" + data[i].description + "</option>";
            }else{
                option += "<option value=\""+data[i].id+"\">" + data[i].description + "</option>";
            }

            container.append(option);
        }
    }

    function getDataPromo(){

        var data = {_token: '{{csrf_token()}}', page_number:pageNumber};
        if(filterName!==''){
            data.promoname = filterName;
        }

        if(filterFloor!=null){
            data.floorid = filterFloor;
        }

        if(filterAirport!=null && filterAirport != ""){
            data.airportcode = [filterAirport];
        }

        console.log(data);

        $('#please-wait').show();
        var request = $.ajax({
            url: "{{action('Admin\\PromoController@getDataPromo')}}",
            type: "POST",
            data: data,
            dataType: "json"
        });

        request.done(function(response) {
            $('#please-wait').hide();
            console.log(response);

            if(response.success){
                var dataPromo = response.data;
                var totalPage = response.total_page;
                var currentPage = response.current_page;

                setDataResponsePromo(dataPromo);
                setPageNumberPromo(totalPage, currentPage);

            }else{
                alert("error occured, please try again");
                console.log("error occured");
            }

        });

        request.fail(function(jqXHR, textStatus) {
            $('#please-wait').hide();
            alert("error occured, please try again");
            console.log( "Request failed: " + textStatus );
            console.log(jqXHR);
        });

    }

    function setDataResponsePromo(data){
        console.log(data);
        $('#tablePromo tbody').empty();
        for(var i=0; i< data.length; i++){
            var row = "<tr class=\"item-promo\">";
            row += "<td><input class=\"except\"type=\"checkbox\" value="+data[i].id+" style=\"position:relative;right:0px\"/></td>";
            row += "<td data-airportcode=\""+data[i].airport.airportcode+"\">"+data[i].airport.airportname+"</td>";
            row += "<td>"+data[i].id+"</td>";
            row += "<td>"+data[i].promoname+"</td>";
            row += "<td>"+data[i].promotext+"</td>";
            row += "<td class=\"promoimage\" data-promoimage="+data[i].promoimage+" data-toggle=\"modal\" data-target=\"#promoImageModal\">Click to Show Image</td>";
            row += "<td data-floorid=\""+data[i].floor.id+"\">"+data[i].floor.description+"</td>";

            if(data[0].showicon){
                row += "<td ><span class=\"badge badge-success\">True</span></td>";
            }else{
                row += "<td><span class=\"badge badge-warning\">False</span></td>";
            }
            row += "<td  data-iconid=\""+data[i].icon.id+"\">"+data[i].icon.description+"</td>";
            row += "<td>"+data[i].x+"</td>";
            row += "<td>"+data[i].y+"</td>";
            row += "<td>"+data[i].z+"</td>";

            row += "</tr>";
            $('#tablePromo tbody').append(row)
        }
    };

    function setPageNumberPromo(totalPage, currentPage){
        $('#pagination ul li').remove();
        $('#pagination p').text("");
        var itemPage = '';

        var count = 5;
        var startPage = Math.max(1, parseInt(currentPage) - count);
        var endPage = Math.min( totalPage, parseInt(currentPage) + count);
        console.log("startpage "+startPage);
        console.log("endpage "+endPage);
        console.log("currentpage "+currentPage);
        if(currentPage>6){
            itemPage += "<li class=\"page-item\"> <a data-pagenumber=\"1\" class=\"page-link\" href=\"#\">First Page</a></li>";
        }

        for(i = startPage; i <= endPage; i++) {
            if(currentPage == i){
                console.log("true");
                itemPage += "<li class=\"page-item disabled\"> <a data-pagenumber="+i+" class=\"page-link\" href=\"#\" tabindex=\"-1\">"+i+"</a></li>";
            }else{
                console.log("false");
                itemPage += "<li class=\"page-item\"> <a data-pagenumber="+i+" class=\"page-link\" href=\"#\">"+i+"</a></li>";
            }
        }

        if(currentPage < totalPage-6 ){
            itemPage += "<li class=\"page-item\"> <a data-pagenumber="+totalPage+" class=\"page-link\" href=\"#\">Last Page</a></li>";
        }


        $('#pagination ul').append(itemPage);
        $('#pagination p').text("Showing page "+currentPage+" of "+ totalPage +" pages");

    }

    function delay(callback, ms) {
        var timer = 0;
        return function() {
            var context = this, args = arguments;
            clearTimeout(timer);
            timer = setTimeout(function () {
                callback.apply(context, args);
            }, ms || 0);
        };
    }

</script>
@endsection
