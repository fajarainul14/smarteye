@extends('admin/_layout')

@section('title', 'Smarteye - Floor List')

@section('css')

@endsection

@section('content')
<div class="content-wrapper">
    <h3 class="page-heading mb-4">Floor</h3>

    <div class="row">
        <div class="col-lg-6 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">

                    <div style="float:right; margin-bottom: 20px;">
                        <a href="#" class="btn btn-primary btn-md" id="button-add">Add
                            Floor</a>
                    </div>

                    @include('admin._filter_master')

                    <div class="table-responsive">
                        <table class="table center-aligned-table data-table table-fit" id="tableData">
                            <thead>
                            <tr class="text-primary">
                                <th>ID</th>
                                <th>Description</th>
                                <th>Airport</th>
                            </tr>
                            </thead>
                            <tbody>

                            {{--@foreach($floors as $key => $floor)

                            <tr class="item-_master">
                                <td>{{ $floor->id }}</td>
                                <td>{{ $floor->description }}</td>
                                <td>{{ $floor->airport->airportname }}</td>
                            </tr>

                            @endforeach--}}

                            </tbody>
                        </table>
                    </div>

                </div>

                @include('admin._pagination')

            </div>
        </div>

        <div class="col-lg-6 grid-margin stretch-card" id="form-container">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Add Floor</h4>
                    <form action="#" method="post">
                        {{ csrf_field() }}

                        @include('form.input', ['label'=>'Description *', 'name'=>'description', 'type'=>'text', 'placeholder'=>'Description', 'required'=>'true', 'class'=>'col-md-12'])
                        @include('form.input', ['name'=>'floor_id', 'type'=>'hidden', 'required'=>'true'])
                        @include('form.select', ['label'=>'', 'name'=>'airportcode', 'placeholder'=>'Airport', 'required'=>'true', 'options'=> convertObject($airports, 'airportcode', 'airportname') ])

                        <br>

                        <a href="#" class="btn btn-outline-danger" id="button-delete"><span>Delete</span></a>

                        <button class="btn btn-outline-primary" type="submit" id="button-form"><span>Save</span></button>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection

@section('js')
<script type="text/javascript">

    var id;
    var desc;
    var airportname;
    var isTrash = false;
    var url = "{{action('Admin\\FloorController@getDataFloor')}}";

    $(document).ready(function () {

        $('#please-wait').hide();
        $('#filter-description').val('');
        $('#filter-airportcode').val('');

        prepareGetData();

        $('#form-container').hide();

        $('#button-add').click(function(){
            $('#form-container input[name="description"]').val('');
            $('#form-container input[name="floor_id"]').val('');
            $('#form-container select[name="airportcode"]').val('');

            $('#form-container').show()
            $('#form-container .card-title').text('Add Floor')
            $('#button-form').text('Save')
            $('#button-delete').hide()

            $('#form-container form').attr('action', "{{ action('Admin\FloorController@store') }}");
        })

        $(document).on('click', '.item-_master', function(event) {
            id = $(this).find('td:eq(0)').text();
            desc = $(this).find('td:eq(1)').text();
            airportname = $(this).find('td:eq(2)').text();

            $('#form-container input[name="description"]').val(desc);
            $('#form-container input[name="floor_id"]').val(id);
            $('#form-container select[name="airportcode"] option').filter(function () {return $(this).text() === airportname;}).prop('selected', true);

            $('#form-container').show();
            $('#form-container .card-title').text('Edit Floor')
            $('#button-form').text('Update')
            $('#button-delete').show()

            $('#form-container form').attr('action', "{{ action('Admin\FloorController@update') }}");

            var deleteUrl = '{{ url("/admin/floor/delete") }}';

            $('#button-delete').attr('href', deleteUrl+'/'+id );

        });

        //delete data
        $('#button-delete').click(function(){

            return confirm("Are you sure, you want to delete floor "+ desc );

        });


    });//END DOCUMENT READY

</script>
@endsection
