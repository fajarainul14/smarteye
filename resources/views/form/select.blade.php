<div class="form-group">
	<label>{{ $label }}</label>

    @if(!isset($id))
        @php
            $id = "";
        @endphp
    @endif

	@if(!isset($class))
		@php
			$class = "";
		@endphp
	@endif

	@php
		if($required == "true"){
			$required = 'required=required';
		}
		else{
			$required = "";
		}
	@endphp
	
	@if(!isset($value))
		@php
			$value = old($name);
		@endphp
	@endif

	<select class="form-control {{ $class }}" name="{{ $name }}" {{ $required }} id="{{ $id }}">
		@if(isset($placeholder))
			<option value="" selected>- {{ $placeholder }} -</option>
		@endif
        @if(isset($options))
            @foreach($options as $option)
                <option value="{{ $option['value'] }}" {{ ($option['value']==$value) ? 'selected' : '' }}>{{ $option['text']}}</option>
            @endforeach
        @endif
	</select>
	{!! $errors->first($name, '<p class="text-danger">:message</p>') !!}
</div>
