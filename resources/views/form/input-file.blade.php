<div class="form-group">
	<label>{{ $label }}</label>

	@if(isset($old_file))
		<p class="text-muted">{{ $old_file }}</p>
		<p class="text-muted">Upload file baru untuk mengganti</p>
	@endif

	@if(!isset($id))
		@php
			$id = "";
		@endphp
	@endif

	@if(!isset($accept))
		@php
			$accept = "";
		@endphp
	@endif

	@php
		if($required == "true"){
			$required = 'required=required';
		}
		else{
			$required = "";
		}
	@endphp
	
	<input id="{{ $id }}" type="file" name="{{ $name }}" accept="{{ $accept }}" {{ $required }}>

	{!! $errors->first($name, '<p class="text-danger">:message</p>') !!}
</div>
