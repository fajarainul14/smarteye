<div class="form-group">
	<label>{{ $label }}</label>

	<figure class="img-preview text-center">
		<img src="" id="{{ $id_preview }}" alt="" style="width: 100%">
	</figure>

	@if($required == "true")
		<input id="{{ $id }}" type="file" name="{{ $name }}" accept="image/*" required="required">
	@else
		<input id="{{ $id }}" type="file" name="{{ $name }}" accept="image/*">
	@endif

	{!! $errors->first($name, '<p class="text-danger">:message</p>') !!}
</div>
