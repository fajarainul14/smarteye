<?php
/**
 * Created by IntelliJ IDEA.
 * User: OSD-1
 * Date: 9/23/2018
 * Time: 10:33 PM
 */

namespace App\Helpers;


use App\Models\LogAdminModel;
use App\Models\UserModel;

class LogAdminHelper
{

    /**
     * fungsi untuk mendapatkan detail update
     * @param $before array nilai field sebelum dilakukan perubahan (dapat bernilai null jika aksi merupakan ADD)
     * @param $after array nilai field setelah dilakukan perubahan
     * @return String mengembalikan detail, null jika $after bukan berupa array
     */
    public static function populateLogDetail($before, $after){

        if(!is_array($after)){
            return null;
        }else{

            if(empty($after)){
                return "Nothing changed";
            }else{

                $detail = '';

                foreach ($after as $key=>$value){

                    if($before==null){
                        $detail = $detail .' '.strtoupper($key).' : '.$value.' || ';
                    }else{
                        $detail = $detail .' '.strtoupper($key).' : '.$before[$key] .' ==> '.$value.' || ';
                    }

                }

                return $detail;

            }


        }


    }

    /**
     * fungsi untuk insert ke dalam tabel log admin
     * @param $user UserModel user yang melakukan aksi
     * @param $action String   aksi yang dilakukan
     * @param $detail String detail aksi
     * @return bool mengembalikan boolean hasil save()
     */
    public static function insertToLog($user, $action, $detail){

        $log = new LogAdminModel();
        $log->userid = $user->userid;
        $log->detail = $detail;
        $log->action = $action;
        return $log->save();

    }

}
