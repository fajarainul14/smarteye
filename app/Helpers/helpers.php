<?php

use App\Models\AgencyLevel2;
use App\Models\AgencyLevel3;

function isUrlContain($string){
    return (strpos(\Request::path(), $string) !== false);
}

function displayDate($date)
{
    if ($date != null && $date != '') {
        return date('d F Y', strtotime($date));
    }
    else{
        return "-";
    }
}

function displayTime($time)
{
    if ($time != null && $time != '') {
        return date('H:i', strtotime($time));
    }
    else{
        return "-";
    }
}


/* Admin */
function getAuth()
{
	return session('userdata');
}

//get token
function getToken(){
    $user = getAuth();
    return $user->token;
}

//check is user superAdmin?
function isSuperAdmin(){
    $user = getAuth();
    $roles = $user->roles;

    foreach ($roles as $role){
        if(strtolower($role->name) == strtolower('superadmin')){
            return true;
        }
    }

    return false;
}


//check is user admin poi?
function isAdminPoi(){
    $user = getAuth();
    $roles = $user->roles;

    foreach ($roles as $role){
        if(strtolower($role->name) == strtolower('adminpoi')){
            return true;
        }
    }

    return false;
}

//check is user admin icon?
function isAdminIcon(){
    $user = getAuth();
    $roles = $user->roles;

    foreach ($roles as $role){
        if(strtolower($role->name) == strtolower('adminicon')){
            return true;
        }
    }

    return false;
}

//check is user admin promo?
function isAdminPromo(){
    $user = getAuth();
    $roles = $user->roles;

    foreach ($roles as $role){
        if(strtolower($role->name) == strtolower('adminpromo')){
            return true;
        }
    }

    return false;
}

//check if user is Admin Pusat
//return true if list airpot = []
//otherwise, false
function isAdminPusat(){
    if(empty(getAirport())){
        return true;
    }else{
        return false;
    }
}
//get user roles
function getRoles(){
    $user = getAuth();
    return $user->roles;
}

//get user roles category
function getRolesCategory(){
    $user = getAuth();
    return $user->poicategories;
}

//get airport that assigned to user
function getAirport(){
    $user = getAuth();
    return $user->airportcodes;
}

function convertObject($array, $keyValue, $keyText){
    $result = array();
    foreach ($array as $key => $value){

        $result[$key] = [
            'value' => $value->$keyValue,
            'text'  => $value->$keyText
        ];

    }

    return $result;
}

function getListGrantedAirport($listData){
    $listAirport = [];
    $grantedAiports = getAirport();
    if(empty($grantedAiports)){
        $listAirport = $listData;
    }else{
        foreach ($listData as $key=>$airport) {

            if (in_array($airport->airportcode, $grantedAiports)) {
                array_push($listAirport, $airport);
            }

        }
    }
    return $listAirport;
}


/* Export */
function displayMonthYear($month, $year)
{
    if ($month == 0 && $year != 0) {
        return "- Tahun " . $year;
    }
    elseif ($month != 0 && $year != 0) {
    	return "- Bulan ". convertMonth($month) ." Tahun " . $year;
    }
    else{
        return "";
    }
}

// for file name when export
function displayMonthYearFileName($month, $year)
{
    if ($month == 0 && $year != 0) {
        return $year;
    }
    elseif ($month != 0 && $year != 0) {
        return convertMonth($month) ." " . $year;
    }
    else{
        return "All";
    }
}

function convertMonth($month)
{
    switch ($month) {
    	case '1':
    		return "Januari";
    		break;
    	case '2':
    		return "Februari";
    		break;
    	case '3':
    		return "Maret";
    		break;
    	case '4':
    		return "April";
    		break;
    	case '5':
    		return "Mei";
    		break;
    	case '6':
    		return "Juni";
    		break;
    	case '7':
    		return "Juli";
    		break;
    	case '8':
    		return "Agustus";
    		break;
    	case '9':
    		return "September";
    		break;
    	case '10':
    		return "Oktober";
    		break;
    	case '11':
    		return "November";
    		break;
    	case '12':
    		return "Desember";
    		break;
    }
}

