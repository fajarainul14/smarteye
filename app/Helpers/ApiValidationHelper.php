<?php
/**
 * Created by IntelliJ IDEA.
 * User: OSD-1
 * Date: 9/19/2018
 * Time: 7:44 AM
 */

namespace App\Helpers;


class ApiValidationHelper
{
    public static function getFirstErrorMessage($messageBag){
        $error = '';
        foreach ($messageBag as $field_key => $errorMessages) {

            foreach ($errorMessages as $errorMessage){
                $error = $errorMessage;
                break;
            }
            break;
        }

        return $error;
    }

}
