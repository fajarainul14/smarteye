<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class IconModel extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $table = 'icon';

    public function pois(){
        return $this->hasMany('App\Models\PoiModel', 'iconid', 'id');
    }

    public function promos(){
        return $this->hasMany('App\Models\PromoModel', 'iconid', 'id');
    }

    public function airport(){
        return $this->belongsTo('App\Models\AirportModel', 'airportcode', 'airportcode');
    }

}
