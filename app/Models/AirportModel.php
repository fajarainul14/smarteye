<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AirportModel extends Model
{
    use SoftDeletes;

    protected $table = 'airport';
    protected $dates = ['deleted_at'];

    public $incrementing = false;
    protected $primaryKey = 'airportcode';

    public function pois(){
        return $this->hasMany('App\Models\PoiModel', 'airportcode', 'airportcode');
    }

    public function promos(){
        return $this->hasMany('App\Models\PromoModel', 'airportcode', 'airportcode');
    }

}
