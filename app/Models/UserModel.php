<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserModel extends Model
{
    use SoftDeletes;

    protected $table = 'users';
    protected $primaryKey = 'userid';
    protected $hidden = ['tokenUser', 'pivot'];
    protected $dates = ['deleted_at'];
    protected $connection = 'pgsql';

    public function tokenUser(){
        return $this->hasMany('App\Models\TokenModel', 'userid', 'userid');
    }

    public function roles(){
        return $this->belongsToMany('App\Models\RoleModel', 'user_role', 'userid', 'roleid')->withTimestamps();;
    }

    public function logs(){
        return $this->hasMany('App\Models\LogAdminModel', 'userid', 'userid')->withTimestamps();
    }
}
