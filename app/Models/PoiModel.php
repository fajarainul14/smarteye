<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class PoiModel extends Model
{
    protected $table='poi';

    public function category(){
        return $this->belongsTo('App\Models\CategoryModel', 'categoryid', 'id')->withTrashed();
    }

    public function area(){
        return $this->belongsTo('App\Models\AreaModel', 'areaid', 'id')->withTrashed();
    }

    public function icon(){
        return $this->belongsTo('App\Models\IconModel', 'iconid', 'id')->withTrashed();
    }

    public function floor(){
        return $this->belongsTo('App\Models\FloorModel', 'floorid', 'id')->withTrashed();
    }

    public function priority(){
        return $this->belongsTo('App\Models\PriorityModel', 'priorityid', 'id')->withTrashed();
    }

    public function airport(){
        return $this->belongsTo('App\Models\AirportModel', 'airportcode', 'airportcode')->withTrashed();
    }
}
