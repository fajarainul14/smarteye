<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LogAdminModel extends Model
{
    protected $table = 'log_admin';
    protected $connection = 'pgsqllog';

    public function user(){
        return $this->belongsTo('App\Models\UserModel', 'userid', 'userid');
    }

}
