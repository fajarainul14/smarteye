<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RoleModel extends Model
{
    protected $table = 'role';
    protected $hidden = ['pivot'];

    public function users(){
        return $this->belongsToMany('App\Models\UserModel', 'user_role', 'roleid', 'userid');
    }
}
