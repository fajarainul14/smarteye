<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FloorModel extends Model
{
    use SoftDeletes;

    protected $table = 'floor';
    protected $dates = ['deleted_at'];

    public function pois(){
        return $this->hasMany('App\Models\PoiModel', 'floorid', 'id');
    }

    public function promos(){
        return $this->hasMany('App\Models\PromoModel', 'floorid', 'id');
    }

    public function airport(){
        return $this->belongsTo('App\Models\AirportModel', 'airportcode', 'airportcode');
    }
}
