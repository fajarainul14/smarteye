<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AreaModel extends Model
{
    use SoftDeletes;
    protected $table = 'area';

    protected $dates = ['deleted_at'];

    public function pois(){
        return $this->hasMany('App\Models\PoiModel', 'areaid', 'id');
    }

    public function airport(){
        return $this->belongsTo('App\Models\AirportModel', 'airportcode', 'airportcode');
    }

}
