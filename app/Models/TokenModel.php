<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TokenModel extends Model
{
    protected $table = 'token';

    public function user(){
        return $this->belongsTo('App\Models\UserModel', 'userid', 'userid');
    }
}
