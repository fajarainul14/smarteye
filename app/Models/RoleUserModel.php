<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RoleUserModel extends Model
{
    protected $table = 'user_role';
    protected $primaryKey = null;
    public $incrementing = false;

    public function role(){
        return $this->belongsTo(RoleModel::class, 'roleid', 'roleid');
    }

}
