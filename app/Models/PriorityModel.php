<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PriorityModel extends Model
{
    use SoftDeletes;
    protected $table = 'priority';
    protected $dates = ['deleted_at'];

    public function pois(){
        return $this->hasMany('App\Models\PoiModel', 'priorityid', 'id');
    }

    public function airport(){
        return $this->belongsTo('App\Models\AirportModel', 'airportcode', 'airportcode');
    }

}
