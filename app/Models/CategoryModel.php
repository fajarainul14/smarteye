<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CategoryModel extends Model
{
    use SoftDeletes;

    protected $table = 'category';
    protected $dates = ['deleted_at'];

    public function pois(){
        return $this->hasMany('App\Models\PoiModel', 'categoryid', 'id');
    }

    public function airport(){
        return $this->belongsTo('App\Models\AirportModel', 'airportcode', 'airportcode');
    }
    
}
