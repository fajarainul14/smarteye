<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PromoModel extends Model
{

    protected $table = 'promo';

    public function icon(){
        return $this->belongsTo('App\Models\IconModel', 'iconid', 'id')->withTrashed();
    }

    public function floor(){
        return $this->belongsTo('App\Models\FloorModel', 'floorid', 'id')->withTrashed();
    }

    public function airport(){
        return $this->belongsTo('App\Models\AirportModel', 'airportcode', 'airportcode')->withTrashed();
    }

}
