<?php

namespace App\Http\Controllers\Client;

use App\Helpers\ApiValidationHelper;
use App\models\PoiModel;
use App\Models\PromoModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ServiceController extends Controller
{
    public function getPOIData(Request $request){

        $validator = Validator::make($request->all(), [
            'airportcode'  => 'required',
            'date'  => 'required',
            'is_first' => 'boolean'
        ]);

        if($validator->fails()){
            $error = ApiValidationHelper::getFirstErrorMessage($validator->getMessageBag()->getMessages());
            return response()->json([
                'success' => false,
                'message' => $error
            ]);
        }

        $airportcode = $request->input('airportcode');
        $date = $request->input('date');

        $latestPromoUpdate = PromoModel::where('updated_at', '>=', $date)->orWhere('created_at', '>=', $date)->get();
        $latestPoiUpdate = PoiModel::where('updated_at', '>=', $date)->orWhere('created_at', '>=', $date)->get();

        if(sizeof($latestPromoUpdate)>=1 || sizeof($latestPoiUpdate)>=1 || $request->input('is_first')){

            $promos = PromoModel::where('airportcode', strtolower($airportcode))->get();
            foreach ($promos as $key => $promo){
                $promo->promoimage = asset($promo->promoimage);
            }

            $pois = PoiModel::where('airportcode', strtolower($airportcode))->get();

            return response()->json([
                'success' => true,
                'status' => 'OK',
                'airportcode' => $airportcode,
                'date' => $date,
                'promo' => $promos,
                'poi' => $pois
            ]);

        }else{

            return response()->json([
                'success' => true,
                'status' => 'NOUPDATE',
                'airportcode' => $airportcode,
                'date' => $date
            ]);

        }

    }
}
