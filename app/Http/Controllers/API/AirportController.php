<?php

namespace App\Http\Controllers\API;

use App\Helpers\ApiValidationHelper;
use App\Helpers\LogAdminHelper;
use App\Helpers\TokenLifeHelper;
use App\models\AirportModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class AirportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * add new airport
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {

        $request['airportcode'] = strtolower($request['airportcode']);

        $validator = Validator::make($request->all(), [
            'airportcode'  => 'required|unique:airport,airportcode',
            'airportname'  => 'required'
        ]);

        if($validator->fails()){
            $error = ApiValidationHelper::getFirstErrorMessage($validator->getMessageBag()->getMessages());
            return response()->json([
                'success' => false,
                'message' => $error
            ]);
        }

        DB::beginTransaction();

        $new_airport = new AirportModel();

        $new_airport->airportname = $request->input('airportname');
        $new_airport->airportcode = $request->input('airportcode');

        $after = $new_airport->getDirty();

        $success = $new_airport->save();

        if(!$success){
            DB::rollBack();
            return response()->json([
                'success' => false,
                'message' => trans('api.add_failed', ['model' => 'airport'])
            ]);
        }

        //insert into log admin
        $user = TokenLifeHelper::getUserByToken($request->header('token'));
        $detail = LogAdminHelper::populateLogDetail(null, $after);
        $action = 'Add Airport';
        LogAdminHelper::insertToLog($user, $action, $detail);

        DB::commit();

        return response()->json([
            'success' => true,
            'message' => trans('api.add_success', ['model' => 'airport'])
        ]);
    }

    /**
     * show airpot list
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request)
    {
        $per_page       = $request->per_page;
        $page_number       = $request->page_number;

        if(!isset($per_page)){
            $per_page = 10;
        }

        if(!isset($page_number)){
            $page_number = 1;
        }

        $airport  = (new AirportModel())->newQuery();

        if ($request->has('airportcode')){
            $airportcode = $request->input('airportcode');
            $airport->where('airportcode', 'ilike' ,"%$airportcode%");
        }

        if ($request->has('airportname')){
            $airportname = $request->input('airportname');
            $airport->where('airportname', 'ilike' ,"%$airportname%");
        }

        $query = $airport->orderBy('created_at', 'asc');

        $totalRow = $query->get()->count();
        $totalPage = ceil($totalRow / $per_page);

        $offset = ($page_number-1) * $per_page;
        $data = $query->offset($offset)->limit($per_page)->get();

        return response()->json([
            'success' => true,
            'data' => $data,
            'total_row' => $totalRow,
            'per_page' => $per_page,
            'total_page' => $totalPage,
            'current_page' => $page_number
        ]);

    }

    public function showTrashed(Request $request)
    {
        $per_page       = $request->per_page;
        $page_number       = $request->page_number;

        if(!isset($per_page)){
            $per_page = 10;
        }

        if(!isset($page_number)){
            $page_number = 1;
        }


        $airport  = (new AirportModel())->newQuery();

        if ($request->has('airportcode')){
            $airportcode = $request->input('airportcode');
            $airport->where('airportcode', 'ilike' ,"%$airportcode%");
        }

        if ($request->has('airportname')){
            $airportname = $request->input('airportname');
            $airport->where('airportname', 'ilike' ,"%$airportname%");
        }


        $query = $airport->orderBy('created_at', 'asc')->onlyTrashed();

        $totalRow = $query->get()->count();
        $totalPage = ceil($totalRow / $per_page);

        $offset = ($page_number-1) * $per_page;
        $data = $query->offset($offset)->limit($per_page)->get();

        return response()->json([
            'success' => true,
            'data' => $data,
            'total_row' => $totalRow,
            'per_page' => $per_page,
            'total_page' => $totalPage,
            'current_page' => $page_number
        ]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * update an existing airport
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request)
    {
        $request['airportcode'] = strtolower($request['airportcode']);
        $request['oldairportcode'] = strtolower($request['oldairportcode']);

        $validator = Validator::make($request->all(), [
            'airportcode'  =>   [
                                    'required',
                                    Rule::unique('airport')->ignore($request->input('airportcode'), 'airportcode'),
                                ],
            'airportname'  => 'required',
            'oldairportcode'           => 'required',
        ]);

        if($validator->fails()){
            $error = ApiValidationHelper::getFirstErrorMessage($validator->getMessageBag()->getMessages());
            return response()->json([
                'success' => false,
                'message' => $error
            ]);
        }

        if($request->input('oldairportcode')!=$request->input('airportcode')){
            $check_airport = AirportModel::find($request->input('airportcode'));
            if($check_airport!=null){
                return response()->json([
                    'success' => false,
                    'message' => 'The airportcode has already been taken.'
                ]);
            }
        }

        $update_airport = AirportModel::find($request->input('oldairportcode'));

        if($update_airport==null){
            return response()->json([
                'success' => false,
                'message' => trans('api.edit_failed', ['model' => 'airport', 'message' => trans('api._not_found', ['object'=>'airportcode'])])
            ]);
        }

        DB::beginTransaction();

        $before = $update_airport->getAttributes();

        $update_airport->airportname = $request->input('airportname');
        $update_airport->airportcode = $request->input('airportcode');

        $after = $update_airport->getDirty();

        $success = $update_airport->save();

        if(!$success){
            DB::rollBack();
            return response()->json([
                'success' => false,
                'message' => trans('api.edit_failed', ['model' => 'airport', 'message'=>''])
            ]);
        }

        //insert into log admin
        $user = TokenLifeHelper::getUserByToken($request->header('token'));
        $action = 'Update Airport';
        $detail = LogAdminHelper::populateLogDetail($before, $after);
        $detail = 'Airportcode = '.$request->oldairportcode.' || '.$detail;
        LogAdminHelper::insertToLog($user, $action, $detail);

        DB::commit();

        return response()->json([
            'success' => true,
            'message' => trans('api.edit_success', ['model' => 'airport'])
        ]);
    }

    /**
     * delete airport by airportcode
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'airportcode'  => 'required',
        ]);

        if($validator->fails()){
            $error = ApiValidationHelper::getFirstErrorMessage($validator->getMessageBag()->getMessages());
            return response()->json([
                'success' => false,
                'message' => $error
            ]);
        }

        $airport_code = strtolower($request->input('airportcode'));

        DB::beginTransaction();

        $airport = AirportModel::find($airport_code);

        if($airport==null){
            return response()->json([
                'success' => false,
                'message' => trans('api.delete_failed', ['model' => 'airport', 'message' => trans('api._not_found', ['object'=>'airportcode'])])
            ]);
        }

        $success = $airport->delete();


        if(!$success){
            DB::rollBack();

            return response()->json([
                'success' => false,
                'message' => trans('api.delete_failed', ['model' => 'airport', 'message' => ''])
            ]);
        }

        //insert into log admin
        $user = TokenLifeHelper::getUserByToken($request->header('token'));
        $action = 'Delete Airport';
        LogAdminHelper::insertToLog($user, $action, 'Delete Airport with airportcode = '.$airport_code);

        DB::commit();

        return response()->json([
            'success' => true,
            'message' => trans('api.delete_success', ['model' => 'airport'])
        ]);
    }

    public function destroyPermanent(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'airportcode'  => 'required',
        ]);

        if($validator->fails()){
            $error = ApiValidationHelper::getFirstErrorMessage($validator->getMessageBag()->getMessages());
            return response()->json([
                'success' => false,
                'message' => $error
            ]);
        }

        $airport_code = strtolower($request->input('airportcode'));

        DB::beginTransaction();

        $airport = AirportModel::onlyTrashed()->find($airport_code);

        if($airport==null){
            return response()->json([
                'success' => false,
                'message' => trans('api.delete_permanent_failed', ['model' => 'airport', 'message' => trans('api._not_found', ['object'=>'airportcode'])])
            ]);
        }

        $checkUsagePoi = $airport->pois;
        $checkUsagePromo = $airport->promos;

        if(sizeof($checkUsagePromo)>0 || sizeof($checkUsagePoi)>0){
            return response()->json([
                'success' => false,
                'message' => trans('api.delete_permanent_failed', ['model' => 'airport', 'message' => trans('api._still_used', ['object'=>'airportcode'])])
            ]);
        }

        $success = $airport->forceDelete();

        if(!$success){
            DB::rollBack();

            return response()->json([
                'success' => false,
                'message' => trans('api.delete_permanent_failed', ['model' => 'airport', 'message' => ''])
            ]);
        }

        //insert into log admin
        $user = TokenLifeHelper::getUserByToken($request->header('token'));
        $action = 'Delete Permanent Airport';
        LogAdminHelper::insertToLog($user, $action, 'Delete Permanent Airport with airportcode = '.$airport_code);

        DB::commit();

        return response()->json([
            'success' => true,
            'message' => trans('api.delete_permanent_success', ['model' => 'airport'])
        ]);
    }

    public function restore(Request $request){

        $validator = Validator::make($request->all(), [
            'airportcode'  => 'required',
        ]);

        if($validator->fails()){
            $error = ApiValidationHelper::getFirstErrorMessage($validator->getMessageBag()->getMessages());
            return response()->json([
                'success' => false,
                'message' => $error
            ]);
        }

        $airport_code = strtolower($request->input('airportcode'));

        DB::beginTransaction();

        $airport = AirportModel::onlyTrashed()->find($airport_code);

        if($airport==null){
            return response()->json([
                'success' => false,
                'message' => trans('api.restore_failed', ['model' => 'airport', 'message' => trans('api._not_found', ['object'=>'airportcode'])])
            ]);
        }

        $success = $airport->restore();

        if(!$success){
            DB::rollBack();

            return response()->json([
                'success' => false,
                'message' => trans('api.restore_failed', ['model' => 'airport', 'message' => ''])
            ]);
        }

        //insert into log admin
        $user = TokenLifeHelper::getUserByToken($request->header('token'));
        $action = 'Restore Airport';
        LogAdminHelper::insertToLog($user, $action, 'Restore Airport with airportcode = '.$airport_code);

        DB::commit();

        return response()->json([
            'success' => true,
            'message' => trans('api.restore_success', ['model' => 'airport'])
        ]);

    }
}
