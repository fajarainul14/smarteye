<?php

namespace App\Http\Controllers\API;

use App\Helpers\ApiValidationHelper;
use App\Helpers\LogAdminHelper;
use App\Helpers\TokenLifeHelper;
use App\Http\Controllers\Controller;
use App\models\PoiModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class PoiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * add POI
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'airportcode'  => 'required',
            'areaid'  => 'required',
            'name'  => 'required',
            'categoryid'  => 'required',
            'priorityid'  => 'required',
            'floorid'  => 'required',
            'iconid'  => 'required',
            'x'  => 'required',
            'y'  => 'required',
            'z'  => 'required',
        ]);


        if($validator->fails()){
            $error = ApiValidationHelper::getFirstErrorMessage($validator->getMessageBag()->getMessages());
            return response()->json([
                'success' => false,
                'message' => $error
            ]);
        }

        /*$check_id = PoiModel::where('id', $request->id)->first();

        if($check_id!=null){
            return response()->json([
                'success' => false,
                'message' => trans('api._already_exist', ['object' => 'Id'])
            ]);
        }*/

        $new_poi = new PoiModel();
        $new_poi->name = $request->input('name');
        $new_poi->airportcode = $request->input('airportcode');
        $new_poi->areaid = $request->input('areaid');
        $new_poi->categoryid = $request->input('categoryid');
        $new_poi->priorityid = $request->input('priorityid');
        $new_poi->floorid = $request->input('floorid');
        $new_poi->iconid = $request->input('iconid');
        $new_poi->x = $request->input('x');
        $new_poi->y = $request->input('y');
        $new_poi->z = $request->input('z');

        $after = $new_poi->getDirty();

        $success = $new_poi->save();

        if(!$success){
            return response()->json([
                'success' => false,
                'message' => trans('api.add_failed', ['model' => 'poi'])
            ]);
        }

        $user = TokenLifeHelper::getUserByToken($request->header('token'));
        $action = 'Add POI';
        $detail = LogAdminHelper::populateLogDetail(null, $after);
        LogAdminHelper::insertToLog($user, $action, $detail);

        return response()->json([
            'success' => true,
            'message' => trans('api.add_success', ['model' => 'poi'])
        ]);

    }

    /**
     * show pois
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request)
    {
        $per_page       = $request->per_page;
        $page_number       = $request->page_number;

        if(!isset($per_page)){
            $per_page = 10;
        }

        if(!isset($page_number)){
            $page_number = 1;
        }

        $poi  = (new PoiModel)->newQuery();

        if ($request->has('categoryid')) {
            $poi->where('categoryid', $request->input('categoryid'));
        }

        if ($request->has('airportcode')) {
            $poi->where('airportcode','ilike',$request->input('airportcode'));
        }

        if ($request->has('areaid')) {
            $poi->where('areaid', $request->input('areaid'));
        }

        if ($request->has('floorid')) {
            $poi->where('floorid', $request->input('floorid'));
        }

        if ($request->has('name')){
            $name = $request->input('name');
            $poi->where('name', 'ilike' ,"%$name%");
        }

        $query = $poi->with(['category', 'area', 'floor', 'airport', 'priority', 'icon']);

        $totalRow = $query->get()->count();
        $totalPage = ceil($totalRow / $per_page);

        $offset = ($page_number-1) * $per_page;
        $data = $query->offset($offset)->limit($per_page)->get();

        return response()->json([
            'success' => true,
            'data' => $data,
            'total_row' => $totalRow,
            'per_page' => $per_page,
            'total_page' => $totalPage,
            'current_page' => $page_number
        ]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * update data poi
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id'  => 'required',
            'airportcode'  => 'required',
            'areaid'  => 'required',
            'name'  => 'required',
            'categoryid'  => 'required',
            'priorityid'  => 'required',
            'floorid'  => 'required',
            'x'  => 'required|numeric',
            'y'  => 'required|numeric',
            'z'  => 'required|numeric',
            'iconid'  => 'required',
        ]);

        if($validator->fails()){
            $error = ApiValidationHelper::getFirstErrorMessage($validator->getMessageBag()->getMessages());
            return response()->json([
                'success' => false,
                'message' => $error
            ]);
        }

        DB::beginTransaction();

        $update_poi = PoiModel::find($request->id);

        if($update_poi==null){
            return response()->json([
                'success' => false,
                'message' => trans('api.edit_failed', ['model' => 'poi', 'message' => trans('api._not_found', ['object'=>'id poi'])])
            ]);
        }

        $before = $update_poi->getAttributes();

        $update_poi->name = $request->input('name');
        $update_poi->airportcode = $request->input('airportcode');
        $update_poi->areaid = $request->input('areaid');
        $update_poi->categoryid = $request->input('categoryid');
        $update_poi->priorityid = $request->input('priorityid');
        $update_poi->floorid = $request->input('floorid');
        $update_poi->iconid = $request->input('iconid');
        $update_poi->x = $request->input('x');
        $update_poi->y = $request->input('y');
        $update_poi->z = $request->input('z');

        $after = $update_poi->getDirty();

        $success = $update_poi->save();

        if(!$success){
            DB::rollBack();
            return response()->json([
                'success' => false,
                'message' => trans('api.edit_failed', ['model' => 'poi', 'message'=>''])
            ]);
        }

        $user = TokenLifeHelper::getUserByToken($request->header('token'));
        $action = 'Update POI';
        $detail = LogAdminHelper::populateLogDetail($before, $after);
        $detail = 'ID POI = '.$request->id.' || '.$detail;
        LogAdminHelper::insertToLog($user, $action, $detail);

        DB::commit();

        return response()->json([
            'success' => true,
            'message' => trans('api.edit_success', ['model' => 'poi'])
        ]);
    }

    /**
     * remove data poi
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'ids'  => 'required|array|min:1',
        ]);

        if($validator->fails()){
            $error = ApiValidationHelper::getFirstErrorMessage($validator->getMessageBag()->getMessages());
            return response()->json([
                'success' => false,
                'message' => $error
            ]);
        }

        $ids = $request->input('ids');

        DB::beginTransaction();

//        $pois = PoiModel::find($ids);
        $pois = PoiModel::whereIn('id', $ids);

        if(sizeof($pois->get())<=0){
            return response()->json([
                'success' => false,
                'message' => trans('api.delete_failed', ['model' => 'poi', 'message' => trans('api._not_found', ['object'=>'id poi'])])
            ]);
        }

        $success = $pois->delete();

        if(!$success){
            DB::rollBack();

            return response()->json([
                'success' => false,
                'message' => trans('api.delete_failed', ['model' => 'poi', 'message' => ''])
            ]);
        }

        //insert into log admin
        $user = TokenLifeHelper::getUserByToken($request->header('token'));
        $action = 'Delete POI';
        LogAdminHelper::insertToLog($user, $action, 'Delete POI with id '.implode($ids, ', '));

        DB::commit();

        return response()->json([
            'success' => true,
            'message' => trans('api.delete_success', ['model' => 'poi'])
        ]);


    }
}
