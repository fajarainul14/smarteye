<?php

namespace App\Http\Controllers\API;

use App\Helpers\ApiValidationHelper;
use App\Helpers\LogAdminHelper;
use App\Helpers\TokenLifeHelper;
use App\Models\PriorityModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class PriorityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * add new priority
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'description'  => 'required',
            'airportcode' => 'required'
        ]);

        if($validator->fails()){
            $error = ApiValidationHelper::getFirstErrorMessage($validator->getMessageBag()->getMessages());
            return response()->json([
                'success' => false,
                'message' => $error
            ]);
        }

        DB::beginTransaction();

        $new_priority = new PriorityModel();

        $new_priority->description = $request->input('description');
        $new_priority->airportcode = strtolower($request->input('airportcode'));

        $after = $new_priority->getDirty();

        $success = $new_priority->save();

        if(!$success){
            DB::rollBack();
            return response()->json([
                'success' => false,
                'message' => trans('api.add_failed', ['model' => 'priority'])
            ]);
        }

        //insert into log admin
        $user = TokenLifeHelper::getUserByToken($request->header('token'));
        $detail = LogAdminHelper::populateLogDetail(null, $after);
        $action = 'Add Priority';
        LogAdminHelper::insertToLog($user, $action, $detail);

        DB::commit();

        return response()->json([
            'success' => true,
            'message' => trans('api.add_success', ['model' => 'priority'])
        ]);
    }

    /**
     * show list priority
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request)
    {
        $per_page       = $request->per_page;
        $page_number       = $request->page_number;

        if(!isset($per_page)){
            $per_page = 10;
        }

        if(!isset($page_number)){
            $page_number = 1;
        }

        $priority  = (new PriorityModel())->newQuery();

        if ($request->has('id')){
            $id = $request->input('id');
            $priority->where('id', $id);
        }

        if ($request->has('description')){
            $desc = $request->input('description');
            $priority->where('description', 'ilike' ,"%$desc%");
        }

        if ($request->has('airportcode')){
            $airportCodes = $request->input('airportcode');
            if(!empty($airportCodes)){
                $priority->whereIn('airportcode',$airportCodes );
            }
        }

        $query =$priority->orderBy('airportcode', 'asc')->orderBy('id', 'asc');

        $totalRow = $query->get()->count();
        $totalPage = ceil($totalRow / $per_page);

        $offset = ($page_number-1) * $per_page;
        $data = $query->with('airport')->offset($offset)->limit($per_page)->get();

        return response()->json([
            'success' => true,
            'data' => $data,
            'total_row' => $totalRow,
            'per_page' => $per_page,
            'total_page' => $totalPage,
            'current_page' => $page_number
        ]);
    }

    public function showTrashed(Request $request)
    {
        $per_page       = $request->per_page;
        $page_number       = $request->page_number;

        if(!isset($per_page)){
            $per_page = 10;
        }

        if(!isset($page_number)){
            $page_number = 1;
        }

        $priority  = (new PriorityModel())->newQuery();

        if ($request->has('id')){
            $id = $request->input('id');
            $priority->where('id', $id);
        }

        if ($request->has('description')){
            $desc = $request->input('description');
            $priority->where('description', 'ilike' ,"%$desc%");
        }

        if ($request->has('airportcode')){
            $airportCodes = $request->input('airportcode');
            if(!empty($airportCodes)){
                $priority->whereIn('airportcode',$airportCodes );
            }
        }

        $query =$priority->orderBy('airportcode', 'asc')->orderBy('id', 'asc')->onlyTrashed();

        $totalRow = $query->get()->count();
        $totalPage = ceil($totalRow / $per_page);

        $offset = ($page_number-1) * $per_page;
        $data = $query->with('airport')->offset($offset)->limit($per_page)->get();

        return response()->json([
            'success' => true,
            'data' => $data,
            'total_row' => $totalRow,
            'per_page' => $per_page,
            'total_page' => $totalPage,
            'current_page' => $page_number
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * update priority
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'description'  => 'required',
            'id'            => 'required',
            'airportcode' => 'required'
        ]);

        if($validator->fails()){
            $error = ApiValidationHelper::getFirstErrorMessage($validator->getMessageBag()->getMessages());
            return response()->json([
                'success' => false,
                'message' => $error
            ]);
        }


        $update_priority = PriorityModel::find($request->input('id'));

        if($update_priority==null){
            return response()->json([
                'success' => false,
                'message' => trans('api.edit_failed', ['model' => 'priority', 'message' => trans('api._not_found', ['object'=>'id'])])
            ]);
        }

        DB::beginTransaction();

        $before = $update_priority->getAttributes();

        $update_priority->description = $request->input('description');
        $update_priority->airportcode = strtolower($request->input('airportcode'));

        $after = $update_priority->getDirty();

        $success = $update_priority->save();

        if(!$success){
            DB::rollBack();
            return response()->json([
                'success' => false,
                'message' => trans('api.edit_failed', ['model' => 'priority', 'message'=>''])
            ]);
        }

        //insert into log admin
        $user = TokenLifeHelper::getUserByToken($request->header('token'));
        $action = 'Update Priority';
        $detail = LogAdminHelper::populateLogDetail($before, $after);
        $detail = 'ID Priority = '.$request->id.' || '.$detail;
        LogAdminHelper::insertToLog($user, $action, $detail);

        DB::commit();

        return response()->json([
            'success' => true,
            'message' => trans('api.edit_success', ['model' => 'priority'])
        ]);
    }

    /**
     * delete priority
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'id'  => 'required',
        ]);

        if($validator->fails()){
            $error = ApiValidationHelper::getFirstErrorMessage($validator->getMessageBag()->getMessages());
            return response()->json([
                'success' => false,
                'message' => $error
            ]);
        }


        $id = $request->input('id');

        DB::beginTransaction();

        $priority = PriorityModel::find($id);

        if($priority==null){
            return response()->json([
                'success' => false,
                'message' => trans('api.delete_failed', ['model' => 'priority', 'message' => trans('api._not_found', ['object'=>'id'])])
            ]);
        }

        $success = $priority->delete();

        if(!$success){
            DB::rollBack();

            return response()->json([
                'success' => false,
                'message' => trans('api.delete_failed', ['model' => 'priority', 'message' => ''])
            ]);
        }

        //insert into log admin
        $user = TokenLifeHelper::getUserByToken($request->header('token'));
        $action = 'Delete Priority';
        LogAdminHelper::insertToLog($user, $action, 'Delete Priority with id = '.$id);

        DB::commit();

        return response()->json([
            'success' => true,
            'message' => trans('api.delete_success', ['model' => 'priority'])
        ]);
    }

    public function destroyPermanent(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'id'  => 'required',
        ]);

        if($validator->fails()){
            $error = ApiValidationHelper::getFirstErrorMessage($validator->getMessageBag()->getMessages());
            return response()->json([
                'success' => false,
                'message' => $error
            ]);
        }


        $id = $request->input('id');

        DB::beginTransaction();

        $priority = PriorityModel::onlyTrashed()->find($id);

        if($priority==null){
            return response()->json([
                'success' => false,
                'message' => trans('api.delete_permanent_failed', ['model' => 'priority', 'message' => trans('api._not_found', ['object'=>'id'])])
            ]);
        }

        $checkUsagePoi = $priority->pois;

        if(sizeof($checkUsagePoi)>0){
            return response()->json([
                'success' => false,
                'message' => trans('api.delete_permanent_failed', ['model' => 'airport', 'message' => trans('api._still_used', ['object'=>'airportcode'])])
            ]);
        }

        $success = $priority->forceDelete();

        if(!$success){
            DB::rollBack();

            return response()->json([
                'success' => false,
                'message' => trans('api.delete_permanent_failed', ['model' => 'priority', 'message' => ''])
            ]);
        }

        //insert into log admin
        $user = TokenLifeHelper::getUserByToken($request->header('token'));
        $action = 'Delete Permanent Priority';
        LogAdminHelper::insertToLog($user, $action, 'Delete Permanent Priority with id = '.$id);

        DB::commit();

        return response()->json([
            'success' => true,
            'message' => trans('api.delete_permanent_success', ['model' => 'priority'])
        ]);
    }

    public function restore(Request $request){

        $validator = Validator::make($request->all(), [
            'id'  => 'required',
        ]);

        if($validator->fails()){
            $error = ApiValidationHelper::getFirstErrorMessage($validator->getMessageBag()->getMessages());
            return response()->json([
                'success' => false,
                'message' => $error
            ]);
        }

        DB::beginTransaction();

        $id = $request->input('id');

        $priority = PriorityModel::onlyTrashed()->find($id);

        if($priority==null){
            return response()->json([
                'success' => false,
                'message' => trans('api.restore_failed', ['model' => 'priority', 'message' => trans('api._not_found', ['object'=>'id'])])
            ]);
        }

        $success = $priority->restore();

        if(!$success){
            DB::rollBack();

            return response()->json([
                'success' => false,
                'message' => trans('api.restore_failed', ['model' => 'priority', 'message' => ''])
            ]);
        }

        //insert into log admin
        $user = TokenLifeHelper::getUserByToken($request->header('token'));
        $action = 'Restore Priority';
        LogAdminHelper::insertToLog($user, $action, 'Restore Priority with id = '.$id);

        DB::commit();

        return response()->json([
            'success' => true,
            'message' => trans('api.restore_success', ['model' => 'priority'])
        ]);

    }

}
