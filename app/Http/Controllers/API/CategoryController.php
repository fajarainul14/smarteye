<?php

namespace App\Http\Controllers\API;

use App\Helpers\ApiValidationHelper;
use App\Helpers\LogAdminHelper;
use App\Helpers\TokenLifeHelper;
use App\Models\CategoryModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /***
     *
     * add new category
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'description'  => 'required',
            'airportcode' => 'required'
        ]);

        if($validator->fails()){
            $error = ApiValidationHelper::getFirstErrorMessage($validator->getMessageBag()->getMessages());
            return response()->json([
                'success' => false,
                'message' => $error
            ]);
        }

        DB::beginTransaction();

        $new_category = new CategoryModel();

        $new_category->description = $request->input('description');
        $new_category->airportcode = strtolower($request->input('airportcode'));

        $after = $new_category->getDirty();

        $success = $new_category->save();

        if(!$success){
            DB::rollBack();
            return response()->json([
                'success' => false,
                'message' => trans('api.add_failed', ['model' => 'category'])
            ]);
        }

        //insert into log admin
        $user = TokenLifeHelper::getUserByToken($request->header('token'));
        $detail = LogAdminHelper::populateLogDetail(null, $after);
        $action = 'Add Category';
        LogAdminHelper::insertToLog($user, $action, $detail);

        DB::commit();

        return response()->json([
            'success' => true,
            'message' => trans('api.add_success', ['model' => 'category'])
        ]);
    }

    /***
     * show list category
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request)
    {
        $per_page       = $request->per_page;
        $page_number       = $request->page_number;

        if(!isset($per_page)){
            $per_page = 10;
        }

        if(!isset($page_number)){
            $page_number = 1;
        }

        $category  = (new CategoryModel())->newQuery();

        if ($request->has('id')){
            $id = $request->input('id');
            $category->where('id', $id);
        }

        if ($request->has('description')){
            $desc = $request->input('description');
            $category->where('description', 'ilike' ,"%$desc%");
        }

        if ($request->has('airportcode')){
            $airportCodes = $request->input('airportcode');
            if(!empty($airportCodes)){
                $category->whereIn('airportcode',$airportCodes );
            }
        }

        $query = $category->orderBy('airportcode', 'asc')->orderBy('id', 'asc');

        $totalRow = $query->get()->count();
        $totalPage = ceil($totalRow / $per_page);

        $offset = ($page_number-1) * $per_page;
        $data = $query->with('airport')->offset($offset)->limit($per_page)->get();

        return response()->json([
            'success' => true,
            'data' => $data,
            'total_row' => $totalRow,
            'per_page' => $per_page,
            'total_page' => $totalPage,
            'current_page' => $page_number
        ]);
    }

    public function showTrashed(Request $request)
    {
        $per_page       = $request->per_page;
        $page_number       = $request->page_number;

        if(!isset($per_page)){
            $per_page = 10;
        }

        if(!isset($page_number)){
            $page_number = 1;
        }

        $category  = (new CategoryModel())->newQuery();

        if ($request->has('id')){
            $id = $request->input('id');
            $category->where('id', $id);
        }

        if ($request->has('description')){
            $desc = $request->input('description');
            $category->where('description', 'ilike' ,"%$desc%");
        }

        if ($request->has('airportcode')){
            $airportCodes = $request->input('airportcode');
            if(!empty($airportCodes)){
                $category->whereIn('airportcode',$airportCodes );
            }
        }

        $query = $category->orderBy('airportcode', 'asc')->orderBy('id', 'asc')->onlyTrashed();

        $totalRow = $query->get()->count();
        $totalPage = ceil($totalRow / $per_page);

        $offset = ($page_number-1) * $per_page;
        $data = $query->with('airport')->offset($offset)->limit($per_page)->get();

        return response()->json([
            'success' => true,
            'data' => $data,
            'total_row' => $totalRow,
            'per_page' => $per_page,
            'total_page' => $totalPage,
            'current_page' => $page_number
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * update category
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'description'  => 'required',
            'id'            => 'required',
            'airportcode' => 'required'
        ]);

        if($validator->fails()){
            $error = ApiValidationHelper::getFirstErrorMessage($validator->getMessageBag()->getMessages());
            return response()->json([
                'success' => false,
                'message' => $error
            ]);
        }


        $update_category = CategoryModel::find($request->input('id'));

        if($update_category==null){
            return response()->json([
                'success' => false,
                'message' => trans('api.edit_failed', ['model' => 'category', 'message' => trans('api._not_found', ['object'=>'id'])])
            ]);
        }

        DB::beginTransaction();

        $before = $update_category->getAttributes();

        $update_category->description = $request->input('description');
        $update_category->airportcode = strtolower($request->input('airportcode'));

        $after = $update_category->getDirty();

        $success = $update_category->save();

        if(!$success){
            DB::rollBack();
            return response()->json([
                'success' => false,
                'message' => trans('api.edit_failed', ['model' => 'category', 'message'=>''])
            ]);
        }

        //insert into log admin
        $user = TokenLifeHelper::getUserByToken($request->header('token'));
        $action = 'Update Category';
        $detail = LogAdminHelper::populateLogDetail($before, $after);
        $detail = 'ID Category = '.$request->id.' || '.$detail;
        LogAdminHelper::insertToLog($user, $action, $detail);

        DB::commit();

        return response()->json([
            'success' => true,
            'message' => trans('api.edit_success', ['model' => 'category'])
        ]);
    }

    /**
     * delete category
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'id'  => 'required',
        ]);

        if($validator->fails()){
            $error = ApiValidationHelper::getFirstErrorMessage($validator->getMessageBag()->getMessages());
            return response()->json([
                'success' => false,
                'message' => $error
            ]);
        }


        $id = $request->input('id');

        DB::beginTransaction();

        $category = CategoryModel::find($id);

        if($category==null){
            return response()->json([
                'success' => false,
                'message' => trans('api.delete_failed', ['model' => 'category', 'message' => trans('api._not_found', ['object'=>'id'])])
            ]);
        }

        $success = $category->delete();

        if(!$success){
            DB::rollBack();

            return response()->json([
                'success' => false,
                'message' => trans('api.delete_failed', ['model' => 'category', 'message' => ''])
            ]);
        }

        //insert into log admin
        $user = TokenLifeHelper::getUserByToken($request->header('token'));
        $action = 'Delete Category';
        LogAdminHelper::insertToLog($user, $action, 'Delete Category with id = '.$id);

        DB::commit();

        return response()->json([
            'success' => true,
            'message' => trans('api.delete_success', ['model' => 'category'])
        ]);
    }

    public function destroyPermanent(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'id'  => 'required',
        ]);

        if($validator->fails()){
            $error = ApiValidationHelper::getFirstErrorMessage($validator->getMessageBag()->getMessages());
            return response()->json([
                'success' => false,
                'message' => $error
            ]);
        }


        $id = $request->input('id');

        DB::beginTransaction();

        $category = CategoryModel::onlyTrashed()->find($id);

        if($category==null){
            return response()->json([
                'success' => false,
                'message' => trans('api.delete_permanent_failed', ['model' => 'category', 'message' => trans('api._not_found', ['object'=>'id'])])
            ]);
        }

        $checkUsagePoi = $category->pois;

        if(sizeof($checkUsagePoi)>0){
            return response()->json([
                'success' => false,
                'message' => trans('api.delete_permanent_failed', ['model' => 'airport', 'message' => trans('api._still_used', ['object'=>'airportcode'])])
            ]);
        }

        $success = $category->forceDelete();

        if(!$success){
            DB::rollBack();

            return response()->json([
                'success' => false,
                'message' => trans('api.delete_permanent_failed', ['model' => 'category', 'message' => ''])
            ]);
        }

        //insert into log admin
        $user = TokenLifeHelper::getUserByToken($request->header('token'));
        $action = 'Delete Permanent Category';
        LogAdminHelper::insertToLog($user, $action, 'Delete Permanent Category with id = '.$id);

        DB::commit();

        return response()->json([
            'success' => true,
            'message' => trans('api.delete_permanent_success', ['model' => 'category'])
        ]);
    }

    public function restore(Request $request){

        $validator = Validator::make($request->all(), [
            'id'  => 'required',
        ]);

        if($validator->fails()){
            $error = ApiValidationHelper::getFirstErrorMessage($validator->getMessageBag()->getMessages());
            return response()->json([
                'success' => false,
                'message' => $error
            ]);
        }

        DB::beginTransaction();

        $id = $request->input('id');

        $category = CategoryModel::onlyTrashed()->find($id);

        if($category==null){
            return response()->json([
                'success' => false,
                'message' => trans('api.restore_failed', ['model' => 'category', 'message' => trans('api._not_found', ['object'=>'id'])])
            ]);
        }

        $success = $category->restore();

        if(!$success){
            DB::rollBack();

            return response()->json([
                'success' => false,
                'message' => trans('api.restore_failed', ['model' => 'category', 'message' => ''])
            ]);
        }

        //insert into log admin
        $user = TokenLifeHelper::getUserByToken($request->header('token'));
        $action = 'Restore Category';
        LogAdminHelper::insertToLog($user, $action, 'Restore Category with id = '.$id);

        DB::commit();

        return response()->json([
            'success' => true,
            'message' => trans('api.restore_success', ['model' => 'category'])
        ]);

    }
}
