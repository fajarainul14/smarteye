<?php

namespace App\Http\Controllers\API;

use App\Helpers\ApiValidationHelper;
use App\Helpers\LogAdminHelper;
use App\Helpers\TokenLifeHelper;
use App\models\AreaModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class AreaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * add new Area
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     *
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'description'  => 'required',
            'airportcode' => 'required'
        ]);

        if($validator->fails()){
            $error = ApiValidationHelper::getFirstErrorMessage($validator->getMessageBag()->getMessages());
            return response()->json([
                'success' => false,
                'message' => $error
            ]);
        }

        DB::beginTransaction();

        $new_area = new AreaModel();

        $new_area->description = $request->input('description');
        $new_area->airportcode = strtolower($request->input('airportcode'));

        $after = $new_area->getDirty();

        $success = $new_area->save();

        if(!$success){
            DB::rollBack();
            return response()->json([
                'success' => false,
                'message' => trans('api.add_failed', ['model' => 'area'])
            ]);
        }

        //insert into log admin
        $user = TokenLifeHelper::getUserByToken($request->header('token'));
        $detail = LogAdminHelper::populateLogDetail(null, $after);
        $action = 'Add Area';
        LogAdminHelper::insertToLog($user, $action, $detail);

        DB::commit();

        return response()->json([
            'success' => true,
            'message' => trans('api.add_success', ['model' => 'area'])
        ]);
    }

    /**
     * show liist area
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request)
    {
        $per_page       = $request->per_page;
        $page_number       = $request->page_number;

        if(!isset($per_page)){
            $per_page = 10;
        }

        if(!isset($page_number)){
            $page_number = 1;
        }

        $area  = (new AreaModel())->newQuery();

        if ($request->has('id')){
            $id = $request->input('id');
            $area->where('id', $id);
        }

        if ($request->has('description')){
            $desc = $request->input('description');
            $area->where('description', 'ilike' ,"%$desc%");
        }

        if ($request->has('airportcode')){
            $airportCodes = $request->input('airportcode');
            if(!empty($airportCodes)){
                $area->whereIn('airportcode',$airportCodes );
            }
        }

        $query =$area->orderBy('airportcode', 'asc')->orderBy('id', 'asc');

        $totalRow = $query->get()->count();
        $totalPage = ceil($totalRow / $per_page);

        $offset = ($page_number-1) * $per_page;
        $data = $query->with('airport')->offset($offset)->limit($per_page)->get();

        return response()->json([
            'success' => true,
            'data' => $data,
            'total_row' => $totalRow,
            'per_page' => $per_page,
            'total_page' => $totalPage,
            'current_page' => $page_number
        ]);

    }

    public function showTrashed(Request $request)
    {
        $per_page       = $request->per_page;
        $page_number       = $request->page_number;

        if(!isset($per_page)){
            $per_page = 10;
        }

        if(!isset($page_number)){
            $page_number = 1;
        }

        $area  = (new AreaModel())->newQuery();

        if ($request->has('id')){
            $id = $request->input('id');
            $area->where('id', $id);
        }

        if ($request->has('description')){
            $desc = $request->input('description');
            $area->where('description', 'ilike' ,"%$desc%");
        }

        if ($request->has('airportcode')){
            $airportCodes = $request->input('airportcode');
            if(!empty($airportCodes)){
                $area->whereIn('airportcode',$airportCodes );
            }
        }

        $query =$area->orderBy('airportcode', 'asc')->orderBy('id', 'asc')->onlyTrashed();

        $totalRow = $query->get()->count();
        $totalPage = ceil($totalRow / $per_page);

        $offset = ($page_number-1) * $per_page;
        $data = $query->with('airport')->offset($offset)->limit($per_page)->get();

        return response()->json([
            'success' => true,
            'data' => $data,
            'total_row' => $totalRow,
            'per_page' => $per_page,
            'total_page' => $totalPage,
            'current_page' => $page_number
        ]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * update area
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'description'  => 'required',
            'id'            => 'required',
            'airportcode' => 'required'
        ]);

        if($validator->fails()){
            $error = ApiValidationHelper::getFirstErrorMessage($validator->getMessageBag()->getMessages());
            return response()->json([
                'success' => false,
                'message' => $error
            ]);
        }


        $update_area = AreaModel::find($request->input('id'));

        if($update_area==null){
            return response()->json([
                'success' => false,
                'message' => trans('api.edit_failed', ['model' => 'area', 'message' => trans('api._not_found', ['object'=>'id'])])
            ]);
        }

        DB::beginTransaction();

        $before = $update_area->getAttributes();

        $update_area->description = $request->input('description');
        $update_area->airportcode = strtolower($request->input('airportcode'));

        $after = $update_area->getDirty();

        $success = $update_area->save();

        if(!$success){
            DB::rollBack();
            return response()->json([
                'success' => false,
                'message' => trans('api.edit_failed', ['model' => 'area', 'message'=>''])
            ]);
        }

        //insert into log admin
        $user = TokenLifeHelper::getUserByToken($request->header('token'));
        $action = 'Update Area';
        $detail = LogAdminHelper::populateLogDetail($before, $after);
        $detail = 'ID Area = '.$request->id.' || '.$detail;
        LogAdminHelper::insertToLog($user, $action, $detail);

        DB::commit();

        return response()->json([
            'success' => true,
            'message' => trans('api.edit_success', ['model' => 'area'])
        ]);
    }

    /**
     * delete area
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'id'  => 'required',
        ]);

        if($validator->fails()){
            $error = ApiValidationHelper::getFirstErrorMessage($validator->getMessageBag()->getMessages());
            return response()->json([
                'success' => false,
                'message' => $error
            ]);
        }

        $id = $request->input('id');

        DB::beginTransaction();

        $area = AreaModel::find($id);

        if($area==null){
            return response()->json([
                'success' => false,
                'message' => trans('api.delete_failed', ['model' => 'area', 'message' => trans('api._not_found', ['object'=>'id'])])
            ]);
        }

        $success = $area->delete();

        if(!$success){
            DB::rollBack();

            return response()->json([
                'success' => false,
                'message' => trans('api.delete_failed', ['model' => 'area', 'message' => ''])
            ]);
        }


        //insert into log admin
        $user = TokenLifeHelper::getUserByToken($request->header('token'));
        $action = 'Delete Area';
        LogAdminHelper::insertToLog($user, $action, 'Delete Area with id = '.$id);

        DB::commit();

        return response()->json([
            'success' => true,
            'message' => trans('api.delete_success', ['model' => 'area'])
        ]);
    }

    public function destroyPermanent(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'id'  => 'required',
        ]);

        if($validator->fails()){
            $error = ApiValidationHelper::getFirstErrorMessage($validator->getMessageBag()->getMessages());
            return response()->json([
                'success' => false,
                'message' => $error
            ]);
        }

        $id = $request->input('id');

        DB::beginTransaction();

        $area = AreaModel::onlyTrashed()->find($id);

        if($area==null){
            return response()->json([
                'success' => false,
                'message' => trans('api.delete_permanent_failed', ['model' => 'area', 'message' => trans('api._not_found', ['object'=>'id'])])
            ]);
        }

        $checkUsagePoi = $area->pois;

        if(sizeof($checkUsagePoi)>0){
            return response()->json([
                'success' => false,
                'message' => trans('api.delete_permanent_failed', ['model' => 'airport', 'message' => trans('api._still_used', ['object'=>'airportcode'])])
            ]);
        }

        $success = $area->forceDelete();

        if(!$success){
            DB::rollBack();

            return response()->json([
                'success' => false,
                'message' => trans('api.delete_permanent_failed', ['model' => 'area', 'message' => ''])
            ]);
        }


        //insert into log admin
        $user = TokenLifeHelper::getUserByToken($request->header('token'));
        $action = 'Delete Area';
        LogAdminHelper::insertToLog($user, $action, 'Delete Area with id = '.$id);

        DB::commit();

        return response()->json([
            'success' => true,
            'message' => trans('api.delete_permanent_success', ['model' => 'area'])
        ]);
    }

    public function restore(Request $request){

        $validator = Validator::make($request->all(), [
            'id'  => 'required',
        ]);

        if($validator->fails()){
            $error = ApiValidationHelper::getFirstErrorMessage($validator->getMessageBag()->getMessages());
            return response()->json([
                'success' => false,
                'message' => $error
            ]);
        }

        DB::beginTransaction();

        $id = $request->input('id');

        $area = AreaModel::onlyTrashed()->find($id);

        if($area==null){
            return response()->json([
                'success' => false,
                'message' => trans('api.restore_failed', ['model' => 'area', 'message' => trans('api._not_found', ['object'=>'id'])])
            ]);
        }

        $success = $area->restore();

        if(!$success){
            DB::rollBack();

            return response()->json([
                'success' => false,
                'message' => trans('api.restore_failed', ['model' => 'area', 'message' => ''])
            ]);
        }

        //insert into log admin
        $user = TokenLifeHelper::getUserByToken($request->header('token'));
        $action = 'Restore Area';
        LogAdminHelper::insertToLog($user, $action, 'Restore Area with id = '.$id);

        DB::commit();

        return response()->json([
            'success' => true,
            'message' => trans('api.restore_success', ['model' => 'area'])
        ]);

    }
}
