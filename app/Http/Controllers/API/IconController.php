<?php

namespace App\Http\Controllers\API;

use App\Helpers\ApiValidationHelper;
use App\Helpers\LogAdminHelper;
use App\Helpers\TokenLifeHelper;
use App\Models\IconModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class IconController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * add new icon
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'description'  => 'required',
            'airportcode' => 'required'
        ]);

        if($validator->fails()){
            $error = ApiValidationHelper::getFirstErrorMessage($validator->getMessageBag()->getMessages());
            return response()->json([
                'success' => false,
                'message' => $error
            ]);
        }

        DB::beginTransaction();

        $new_icon = new IconModel();

        $new_icon->description = $request->input('description');
        $new_icon->airportcode = strtolower($request->input('airportcode'));

        $after = $new_icon->getDirty();

        $success = $new_icon->save();

        if(!$success){
            DB::rollBack();
            return response()->json([
                'success' => false,
                'message' => trans('api.add_failed', ['model' => 'icon'])
            ]);
        }

        //insert into log admin
        $user = TokenLifeHelper::getUserByToken($request->header('token'));
        $detail = LogAdminHelper::populateLogDetail(null, $after);
        $action = 'Add Icon';
        LogAdminHelper::insertToLog($user, $action, $detail);

        DB::commit();

        return response()->json([
            'success' => true,
            'message' => trans('api.add_success', ['model' => 'icon'])
        ]);
    }

    /**
     * show list icon
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request)
    {
        $per_page       = $request->per_page;
        $page_number       = $request->page_number;

        if(!isset($per_page)){
            $per_page = 10;
        }

        if(!isset($page_number)){
            $page_number = 1;
        }

        $icon  = (new IconModel())->newQuery();

        if ($request->has('id')){
            $id = $request->input('id');
            $icon->where('id', $id);
        }

        if ($request->has('description')){
            $desc = $request->input('description');
            $icon->where('description', 'ilike' ,"%$desc%");
        }

        if ($request->has('airportcode')){
            $airportCodes = $request->input('airportcode');
            if(!empty($airportCodes)){
                $icon->where('airportcode',$airportCodes );
            }
        }

        $query =$icon->orderBy('airportcode', 'asc')->orderBy('id', 'asc');

        $totalRow = $query->get()->count();
        $totalPage = ceil($totalRow / $per_page);

        $offset = ($page_number-1) * $per_page;
        $data = $query->with('airport')->offset($offset)->limit($per_page)->get();

        return response()->json([
            'success' => true,
            'data' => $data,
            'total_row' => $totalRow,
            'per_page' => $per_page,
            'total_page' => $totalPage,
            'current_page' => $page_number
        ]);
    }

    public function showTrashed(Request $request)
    {
        $per_page       = $request->per_page;
        $page_number       = $request->page_number;

        if(!isset($per_page)){
            $per_page = 10;
        }

        if(!isset($page_number)){
            $page_number = 1;
        }

        $icon  = (new IconModel())->newQuery();

        if ($request->has('id')){
            $id = $request->input('id');
            $icon->where('id', $id);
        }

        if ($request->has('description')){
            $desc = $request->input('description');
            $icon->where('description', 'ilike' ,"%$desc%");
        }

        if ($request->has('airportcode')){
            $airportCodes = $request->input('airportcode');
            if(!empty($airportCodes)){
                $icon->where('airportcode',$airportCodes );
            }
        }

        $query =$icon->orderBy('airportcode', 'asc')->orderBy('id', 'asc')->onlyTrashed();

        $totalRow = $query->get()->count();
        $totalPage = ceil($totalRow / $per_page);

        $offset = ($page_number-1) * $per_page;
        $data = $query->with('airport')->offset($offset)->limit($per_page)->get();

        return response()->json([
            'success' => true,
            'data' => $data,
            'total_row' => $totalRow,
            'per_page' => $per_page,
            'total_page' => $totalPage,
            'current_page' => $page_number
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * update icon
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'description'  => 'required',
            'id'            => 'required',
            'airportcode' => 'required'
        ]);

        if($validator->fails()){
            $error = ApiValidationHelper::getFirstErrorMessage($validator->getMessageBag()->getMessages());
            return response()->json([
                'success' => false,
                'message' => $error
            ]);
        }


        $update_icon = IconModel::find($request->input('id'));

        if($update_icon==null){
            return response()->json([
                'success' => false,
                'message' => trans('api.edit_failed', ['model' => 'icon', 'message' => trans('api._not_found', ['object'=>'id'])])
            ]);
        }

        DB::beginTransaction();

        $before = $update_icon->getAttributes();

        $update_icon->description = $request->input('description');
        $update_icon->airportcode = strtolower($request->input('airportcode'));

        $after = $update_icon->getDirty();

        $success = $update_icon->save();

        if(!$success){
            DB::rollBack();
            return response()->json([
                'success' => false,
                'message' => trans('api.edit_failed', ['model' => 'icon', 'message'=>''])
            ]);
        }

        //insert into log admin
        $user = TokenLifeHelper::getUserByToken($request->header('token'));
        $action = 'Update Icon';
        $detail = LogAdminHelper::populateLogDetail($before, $after);
        $detail = 'ID Icon = '.$request->id.' || '.$detail;
        LogAdminHelper::insertToLog($user, $action, $detail);

        DB::commit();

        return response()->json([
            'success' => true,
            'message' => trans('api.edit_success', ['model' => 'icon'])
        ]);
    }

    /**
     * delete icon
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'id'  => 'required',
        ]);

        if($validator->fails()){
            $error = ApiValidationHelper::getFirstErrorMessage($validator->getMessageBag()->getMessages());
            return response()->json([
                'success' => false,
                'message' => $error
            ]);
        }


        $id = $request->input('id');

        DB::beginTransaction();

        $icon = IconModel::find($id);

        if($icon==null){
            return response()->json([
                'success' => false,
                'message' => trans('api.delete_failed', ['model' => 'icon', 'message' => trans('api._not_found', ['object'=>'id'])])
            ]);
        }

        $success = $icon->delete();

        if(!$success){
            DB::rollBack();

            return response()->json([
                'success' => false,
                'message' => trans('api.delete_failed', ['model' => 'icon', 'message' => ''])
            ]);
        }

        //insert into log admin
        $user = TokenLifeHelper::getUserByToken($request->header('token'));
        $action = 'Delete Icon';
        LogAdminHelper::insertToLog($user, $action, 'Delete Icon with id = '.$id);

        DB::commit();

        return response()->json([
            'success' => true,
            'message' => trans('api.delete_success', ['model' => 'icon'])
        ]);
    }

    public function destroyPermanent(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'id'  => 'required',
        ]);

        if($validator->fails()){
            $error = ApiValidationHelper::getFirstErrorMessage($validator->getMessageBag()->getMessages());
            return response()->json([
                'success' => false,
                'message' => $error
            ]);
        }


        $id = $request->input('id');

        DB::beginTransaction();

        $icon = IconModel::onlyTrashed()->find($id);

        if($icon==null){
            return response()->json([
                'success' => false,
                'message' => trans('api.delete_permanent_failed', ['model' => 'icon', 'message' => trans('api._not_found', ['object'=>'id'])])
            ]);
        }

        $checkUsagePoi = $icon->pois;
        $checkUsagePromo = $icon->promos;

        if(sizeof($checkUsagePromo)>0 || sizeof($checkUsagePoi)>0){
            return response()->json([
                'success' => false,
                'message' => trans('api.delete_permanent_failed', ['model' => 'icon', 'message' => trans('api._still_used', ['object'=>'icon'])])
            ]);
        }

        $success = $icon->forceDelete();

        if(!$success){
            DB::rollBack();

            return response()->json([
                'success' => false,
                'message' => trans('api.delete_permanent_failed', ['model' => 'icon', 'message' => ''])
            ]);
        }

        //insert into log admin
        $user = TokenLifeHelper::getUserByToken($request->header('token'));
        $action = 'Delete Permanent Icon';
        LogAdminHelper::insertToLog($user, $action, 'Delete Permanent Icon with id = '.$id);

        DB::commit();

        return response()->json([
            'success' => true,
            'message' => trans('api.delete_permanent_success', ['model' => 'icon'])
        ]);
    }

    public function restore(Request $request){

        $validator = Validator::make($request->all(), [
            'id'  => 'required',
        ]);

        if($validator->fails()){
            $error = ApiValidationHelper::getFirstErrorMessage($validator->getMessageBag()->getMessages());
            return response()->json([
                'success' => false,
                'message' => $error
            ]);
        }

        DB::beginTransaction();

        $id = $request->input('id');

        $icon = IconModel::onlyTrashed()->find($id);

        if($icon==null){
            return response()->json([
                'success' => false,
                'message' => trans('api.restore_failed', ['model' => 'icon', 'message' => trans('api._not_found', ['object'=>'id'])])
            ]);
        }

        $success = $icon->restore();

        if(!$success){
            DB::rollBack();

            return response()->json([
                'success' => false,
                'message' => trans('api.restore_failed', ['model' => 'icon', 'message' => ''])
            ]);
        }

        //insert into log admin
        $user = TokenLifeHelper::getUserByToken($request->header('token'));
        $action = 'Restore Icon';
        LogAdminHelper::insertToLog($user, $action, 'Restore Icon with id = '.$id);

        DB::commit();

        return response()->json([
            'success' => true,
            'message' => trans('api.restore_success', ['model' => 'icon'])
        ]);

    }

}
