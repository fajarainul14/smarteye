<?php

namespace App\Http\Controllers\API;

use App\Helpers\ApiValidationHelper;
use App\Helpers\LogAdminHelper;
use App\Helpers\TokenLifeHelper;
use App\Models\PromoModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;

class PromoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * store new promo
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'airportcode'  => 'required',
            'promoname'  => 'required',
            'promotext'  => 'required',
            'promoimage'  => 'required',
            'floorid'  => 'required',
            'iconid'  => 'required',
            'showicon'  => 'required|boolean',
            'x'  => 'required',
            'y'  => 'required',
            'z'  => 'required',
        ]);


        if($validator->fails()){
            $error = ApiValidationHelper::getFirstErrorMessage($validator->getMessageBag()->getMessages());
            return response()->json([
                'success' => false,
                'message' => $error
            ]);
        }

        //save image
        $image = $request->input('promoimage');  //base64 image
        $base64_str = substr($image, strpos($image, ",")+1); // get the image code
        $imageName = str_slug($request->input('promoname'), '_').'-'.str_random(10).'.'.'png';
        $filePath = public_path('promo/'.$imageName);
        File::put($filePath, base64_decode($base64_str));

        $new_promo = new PromoModel();
        $new_promo->promoname = $request->input('promoname');
        $new_promo->promotext = $request->input('promotext');
        $new_promo->promoimage = 'promo/'.$imageName;
        $new_promo->airportcode = $request->input('airportcode');
        $new_promo->floorid = $request->input('floorid');
        $new_promo->iconid = $request->input('iconid');
        $new_promo->showicon = $request->input('showicon');
        $new_promo->x = $request->input('x');
        $new_promo->y = $request->input('y');
        $new_promo->z = $request->input('z');

        $after = $new_promo->getDirty();

        $success = $new_promo->save();

        if(!$success){
            return response()->json([
                'success' => false,
                'message' => trans('api.add_failed', ['model' => 'promo'])
            ]);
        }

        $user = TokenLifeHelper::getUserByToken($request->header('token'));
        $action = 'Add Promo';
        $detail = LogAdminHelper::populateLogDetail(null, $after);
        LogAdminHelper::insertToLog($user, $action, $detail);

        return response()->json([
            'success' => true,
            'message' => trans('api.add_success', ['model' => 'promo'])
        ]);

    }

    /**
     * show promos
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request)
    {
        $per_page       = $request->per_page;
        $page_number       = $request->page_number;

        if(!isset($per_page)){
            $per_page = 1;
        }

        if(!isset($page_number)){
            $page_number = 1;
        }

        $promo  = (new PromoModel())->newQuery();

        if ($request->has('floorid')) {
            $promo->where('floorid', $request->input('floorid'));
        }

        $promoName = $request->input('promoname');
        if ($request->has('promoname')) {
            $promo->where('promoname','ilike',"%$promoName%");
        }

        if ($request->has('airportcode')){
            $airportCodes = $request->input('airportcode');
            if(!empty($airportCodes)){
                $promo->whereIn('airportcode',$airportCodes );
            }
        }

        $query = $promo->with([ 'floor', 'icon', 'airport']);

        $totalRow = $query->get()->count();
        $totalPage = ceil($totalRow / $per_page);

        $offset = ($page_number-1) * $per_page;
        $data = $query->offset($offset)->limit($per_page)->get();

        foreach ($data as $key=>$promo){

            $promo->promoimage = asset($promo->promoimage);

        }

        return response()->json([
            'success' => true,
            'data' => $data,
            'total_row' => $totalRow,
            'per_page' => $per_page,
            'total_page' => $totalPage,
            'current_page' => $page_number
        ]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * update promo
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request)
    {
        {
            $validator = Validator::make($request->all(), [
                'airportcode'  => 'required',
                'promoname'  => 'required',
                'promotext'  => 'required',
                'promoimage' => 'required_if:is_change_image,true',
                'floorid'  => 'required',
                'iconid'  => 'required',
                'showicon'  => 'required|boolean',
                'x'  => 'required',
                'y'  => 'required',
                'z'  => 'required',
                'is_change_image' =>'required|boolean'
            ]);

            if ($validator->fails()) {
                $error = ApiValidationHelper::getFirstErrorMessage($validator->getMessageBag()->getMessages());
                return response()->json([
                    'success' => false,
                    'message' => $error
                ]);
            }

            DB::beginTransaction();

            $update_promo = PromoModel::find($request->id);

            if ($update_promo == null) {
                return response()->json([
                    'success' => false,
                    'message' => trans('api.edit_failed', ['model' => 'promo', 'message' => trans('api._not_found', ['object' => 'id promo'])])
                ]);
            }

            if($request->input('is_change_image')){

                //save new image
                $image = $request->input('promoimage');  //base64 image
                $base64_str = substr($image, strpos($image, ",")+1); // get the image code
                $imageName = str_slug($request->input('promoname'), '_').'-'.str_random(10).'.'.'png';
                $filePath = public_path('promo/'.$imageName);
                $saveImage = File::put($filePath, base64_decode($base64_str));

                if($saveImage){

                    File::delete(public_path($update_promo->promoimage));

                }
            }

            $before = $update_promo->getAttributes();

            $update_promo->promoname = $request->input('promoname');
            $update_promo->promotext = $request->input('promotext');

            if($request->input('is_change_image')){
                $update_promo->promoimage = 'promo/'.$imageName;
            }

            $update_promo->airportcode = $request->input('airportcode');
            $update_promo->floorid = $request->input('floorid');
            $update_promo->iconid = $request->input('iconid');
            $update_promo->showicon = $request->input('showicon');
            $update_promo->x = $request->input('x');
            $update_promo->y = $request->input('y');
            $update_promo->z = $request->input('z');

            $after = $update_promo->getDirty();

            $success = $update_promo->save();

            if (!$success) {
                DB::rollBack();
                return response()->json([
                    'success' => false,
                    'message' => trans('api.edit_failed', ['model' => 'promo', 'message' => ''])
                ]);
            }

            $user = TokenLifeHelper::getUserByToken($request->header('token'));
            $action = 'Update Promo';
            $detail = LogAdminHelper::populateLogDetail($before, $after);
            $detail = 'ID Promo = ' . $request->id . ' || ' . $detail;
            LogAdminHelper::insertToLog($user, $action, $detail);

            DB::commit();

            return response()->json([
                'success' => true,
                'message' => trans('api.edit_success', ['model' => 'promo'])
            ]);
        }
    }
    /**
     * destroy promo by id
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'ids'  => 'required|array|min:1',
        ]);

        if($validator->fails()){
            $error = ApiValidationHelper::getFirstErrorMessage($validator->getMessageBag()->getMessages());
            return response()->json([
                'success' => false,
                'message' => $error
            ]);
        }

        $ids = $request->input('ids');

        DB::beginTransaction();

        $promos = PromoModel::whereIn('id', $ids);

        if(sizeof($promos->get())<=0){
            return response()->json([
                'success' => false,
                'message' => trans('api.delete_failed', ['model' => 'promo', 'message' => trans('api._not_found', ['object'=>'id promo'])])
            ]);
        }

        $success = $promos->delete();

        if(!$success){
            DB::rollBack();

            return response()->json([
                'success' => false,
                'message' => trans('api.delete_failed', ['model' => 'promo', 'message' => ''])
            ]);
        }

        //insert into log admin
        $user = TokenLifeHelper::getUserByToken($request->header('token'));
        $action = 'Delete Promo';
        LogAdminHelper::insertToLog($user, $action, 'Delete Promo with id '.implode($ids, ', '));

        DB::commit();

        return response()->json([
            'success' => true,
            'message' => trans('api.delete_success', ['model' => 'promo'])
        ]);
    }
}
