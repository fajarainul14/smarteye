<?php

namespace App\Http\Controllers\API;

use App\Helpers\ApiValidationHelper;
use App\Helpers\LogAdminHelper;
use App\Helpers\TokenLifeHelper;
use App\Models\FloorModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class FloorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * add floor
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'description'  => 'required',
            'airportcode' => 'required'
        ]);

        if($validator->fails()){
            $error = ApiValidationHelper::getFirstErrorMessage($validator->getMessageBag()->getMessages());
            return response()->json([
                'success' => false,
                'message' => $error
            ]);
        }

        DB::beginTransaction();

        $new_floor = new FloorModel();

        $new_floor->description = $request->input('description');
        $new_floor->airportcode = strtolower($request->input('airportcode'));

        $after = $new_floor->getDirty();

        $success = $new_floor->save();

        if(!$success){
            DB::rollBack();
            return response()->json([
                'success' => false,
                'message' => trans('api.add_failed', ['model' => 'floor'])
            ]);
        }

        //insert into log admin
        $user = TokenLifeHelper::getUserByToken($request->header('token'));
        $detail = LogAdminHelper::populateLogDetail(null, $after);
        $action = 'Add Floor';
        LogAdminHelper::insertToLog($user, $action, $detail);

        DB::commit();

        return response()->json([
            'success' => true,
            'message' => trans('api.add_success', ['model' => 'floor'])
        ]);
    }

    /**
     * show list floor
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request)
    {
        $per_page       = $request->per_page;
        $page_number       = $request->page_number;

        if(!isset($per_page)){
            $per_page = 10;
        }

        if(!isset($page_number)){
            $page_number = 1;
        }

        $floor  = (new FloorModel())->newQuery();

        if ($request->has('id')){
            $id = $request->input('id');
            $floor->where('id', $id);
        }

        if ($request->has('description')){
            $desc = $request->input('description');
            $floor->where('description', 'ilike' ,"%$desc%");
        }

        if ($request->has('airportcode')){
            $airportCodes = $request->input('airportcode');
            if(!empty($airportCodes)){
                $floor->where('airportcode',$airportCodes );
            }
        }

        $query =$floor->orderBy('airportcode', 'asc')->orderBy('id', 'asc');

        $totalRow = $query->get()->count();
        $totalPage = ceil($totalRow / $per_page);

        $offset = ($page_number-1) * $per_page;
        $data = $query->with('airport')->offset($offset)->limit($per_page)->get();

        return response()->json([
            'success' => true,
            'data' => $data,
            'total_row' => $totalRow,
            'per_page' => $per_page,
            'total_page' => $totalPage,
            'current_page' => $page_number
        ]);
    }

    public function showTrashed(Request $request)
    {
        $per_page       = $request->per_page;
        $page_number       = $request->page_number;

        if(!isset($per_page)){
            $per_page = 10;
        }

        if(!isset($page_number)){
            $page_number = 1;
        }

        $floor  = (new FloorModel())->newQuery();

        if ($request->has('id')){
            $id = $request->input('id');
            $floor->where('id', $id);
        }

        if ($request->has('description')){
            $desc = $request->input('description');
            $floor->where('description', 'ilike' ,"%$desc%");
        }

        if ($request->has('airportcode')){
            $airportCodes = $request->input('airportcode');
            if(!empty($airportCodes)){
                $floor->where('airportcode',$airportCodes );
            }
        }

        $query =$floor->orderBy('airportcode', 'asc')->orderBy('id', 'asc')->onlyTrashed();

        $totalRow = $query->get()->count();
        $totalPage = ceil($totalRow / $per_page);

        $offset = ($page_number-1) * $per_page;
        $data = $query->with('airport')->offset($offset)->limit($per_page)->get();

        return response()->json([
            'success' => true,
            'data' => $data,
            'total_row' => $totalRow,
            'per_page' => $per_page,
            'total_page' => $totalPage,
            'current_page' => $page_number
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * update floor
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'description'  => 'required',
            'id'            => 'required',
            'airportcode'            => 'required'
        ]);

        if($validator->fails()){
            $error = ApiValidationHelper::getFirstErrorMessage($validator->getMessageBag()->getMessages());
            return response()->json([
                'success' => false,
                'message' => $error
            ]);
        }


        $update_floor = FloorModel::find($request->input('id'));

        if($update_floor==null){
            return response()->json([
                'success' => false,
                'message' => trans('api.edit_failed', ['model' => 'floor', 'message' => trans('api._not_found', ['object'=>'id'])])
            ]);
        }

        DB::beginTransaction();

        $before = $update_floor->getAttributes();

        $update_floor->description = $request->input('description');
        $update_floor->airportcode = strtolower($request->input('airportcode'));

        $after = $update_floor->getDirty();

        $success = $update_floor->save();

        if(!$success){
            DB::rollBack();
            return response()->json([
                'success' => false,
                'message' => trans('api.edit_failed', ['model' => 'floor', 'message'=>''])
            ]);
        }

        //insert into log admin
        $user = TokenLifeHelper::getUserByToken($request->header('token'));
        $action = 'Update Floor';
        $detail = LogAdminHelper::populateLogDetail($before, $after);
        $detail = 'ID Floor = '.$request->id.' || '.$detail;
        LogAdminHelper::insertToLog($user, $action, $detail);

        DB::commit();

        return response()->json([
            'success' => true,
            'message' => trans('api.edit_success', ['model' => 'floor'])
        ]);
    }

    /**
     * delete floor
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'id'  => 'required',
        ]);

        if($validator->fails()){
            $error = ApiValidationHelper::getFirstErrorMessage($validator->getMessageBag()->getMessages());
            return response()->json([
                'success' => false,
                'message' => $error
            ]);
        }


        $id = $request->input('id');

        DB::beginTransaction();

        $floor = FloorModel::find($id);

        if($floor==null){
            return response()->json([
                'success' => false,
                'message' => trans('api.delete_failed', ['model' => 'floor', 'message' => trans('api._not_found', ['object'=>'id'])])
            ]);
        }

        $success = $floor->delete();

        if(!$success){
            DB::rollBack();

            return response()->json([
                'success' => false,
                'message' => trans('api.delete_failed', ['model' => 'floor', 'message' => ''])
            ]);
        }

        //insert into log admin
        $user = TokenLifeHelper::getUserByToken($request->header('token'));
        $action = 'Delete Floor';
        LogAdminHelper::insertToLog($user, $action, 'Delete Floor with id = '.$id);

        DB::commit();

        return response()->json([
            'success' => true,
            'message' => trans('api.delete_success', ['model' => 'floor'])
        ]);
    }

    public function destroyPermanent(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'id'  => 'required',
        ]);

        if($validator->fails()){
            $error = ApiValidationHelper::getFirstErrorMessage($validator->getMessageBag()->getMessages());
            return response()->json([
                'success' => false,
                'message' => $error
            ]);
        }


        $id = $request->input('id');

        DB::beginTransaction();

        $floor = FloorModel::onlyTrashed()->find($id);

        if($floor==null){
            return response()->json([
                'success' => false,
                'message' => trans('api.delete_permanent_failed', ['model' => 'floor', 'message' => trans('api._not_found', ['object'=>'id'])])
            ]);
        }

        $checkUsagePoi = $floor->pois;
        $checkUsagePromo = $floor->promos;

        if(sizeof($checkUsagePromo)>0 || sizeof($checkUsagePoi)>0){
            return response()->json([
                'success' => false,
                'message' => trans('api.delete_permanent_failed', ['model' => 'airport', 'message' => trans('api._still_used', ['object'=>'airportcode'])])
            ]);
        }

        $success = $floor->forceDelete();

        if(!$success){
            DB::rollBack();

            return response()->json([
                'success' => false,
                'message' => trans('api.delete_permanent_failed', ['model' => 'floor', 'message' => ''])
            ]);
        }

        //insert into log admin
        $user = TokenLifeHelper::getUserByToken($request->header('token'));
        $action = 'Delete Permanent Floor';
        LogAdminHelper::insertToLog($user, $action, 'Delete Permanent Floor with id = '.$id);

        DB::commit();

        return response()->json([
            'success' => true,
            'message' => trans('api.delete_permanent_success', ['model' => 'floor'])
        ]);
    }

    public function restore(Request $request){

        $validator = Validator::make($request->all(), [
            'id'  => 'required',
        ]);

        if($validator->fails()){
            $error = ApiValidationHelper::getFirstErrorMessage($validator->getMessageBag()->getMessages());
            return response()->json([
                'success' => false,
                'message' => $error
            ]);
        }

        DB::beginTransaction();

        $id = $request->input('id');

        $floor = FloorModel::onlyTrashed()->find($id);

        if($floor==null){
            return response()->json([
                'success' => false,
                'message' => trans('api.restore_failed', ['model' => 'floor', 'message' => trans('api._not_found', ['object'=>'id'])])
            ]);
        }

        $success = $floor->restore();

        if(!$success){
            DB::rollBack();

            return response()->json([
                'success' => false,
                'message' => trans('api.restore_failed', ['model' => 'floor', 'message' => ''])
            ]);
        }

        //insert into log admin
        $user = TokenLifeHelper::getUserByToken($request->header('token'));
        $action = 'Restore Floor';
        LogAdminHelper::insertToLog($user, $action, 'Restore Floor with id = '.$id);

        DB::commit();

        return response()->json([
            'success' => true,
            'message' => trans('api.restore_success', ['model' => 'floor'])
        ]);

    }
}
