<?php

namespace App\Http\Controllers\API;

use App\Helpers\ApiValidationHelper;
use App\Helpers\TokenLifeHelper;
use App\Models\RoleUserModel;
use App\Models\TokenModel;
use App\Models\UserModel;
use DateTime;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{

    public function login(Request $request){

        $validator = Validator::make($request->all(), [
            'username'  => 'required',
            'password'  => 'required',
        ]);

        if($validator->fails()){
            $error = ApiValidationHelper::getFirstErrorMessage($validator->getMessageBag()->getMessages());
            return response()->json([
                'success' => false,
                'message' => $error
            ]);
        }

        $username       = $request->input('username');
        $password       = $request->input('password');

        $user = UserModel::where('username', $username)->where('password', md5($password))->first(['userid', 'username', 'email', 'status']);

        if($user!=null){

            //user belum aktivasi email
            if($user->status != 1){
                return response()->json([
                    'success' => false,
                    'message' => trans('api.not_active')
                ]);
            }

            //getting list category poi
            $userRolePoi = RoleUserModel::where('userid', $user->userid)->where('roleid', 2)->first();

            if($userRolePoi){
                $user['poicategories'] = array_map('trim', explode(',', $userRolePoi->poicategories));
            }else{
                $user['poicategories'] = [];
            }

            $userRole = RoleUserModel::where('userid', $user->userid)->first();
            //get airport code for this user
            if($userRole->airportcodes!=null){
                $user['airportcodes'] = array_map('trim', explode(',', $userRole->airportcodes));
            }else{
                $user['airportcodes'] = [];
            }

            $token_life = new TokenLifeHelper();

            if (!$token_life->checkLifetimeTokenById($user->userid)) {
                $token = TokenLifeHelper::getToken(32); // get new token

                /* Set transaction */
                DB::beginTransaction();

                $new_token               = new TokenModel();
                $new_token->token        = $token;
                $new_token->expired_date = date('Y-m-d H:i:s', time() + 36000);
                $new_token->userid       = $user->userid;
                $success                 = $new_token->save();

                if (!$success) {
                    /* Transsaction di rollback */
                    DB::rollBack();

                    return response()->json([
                        'success' => false,
                        'message' => trans('api.error_insert')
                    ]);

                }

                $user['token'] = $token;
                $user['roles'] = $user->roles;

                /* Save transaction ke DB */
                DB::commit();

            } else {

                $user['token'] = $user->tokenUser()->orderBy('id', 'desc')->first()->token;
                $user['roles'] = $user->roles;

            }

            return response()->json([
                'success' => true,
                'message' => trans('api.logged_in_as', ['name' => $user->username]),
                'data'    => $user
            ]);

        }else{
            return response()->json([
                'success' => false,
                'message' => trans('api.login_not_match')
            ]);
        }
    }

}
