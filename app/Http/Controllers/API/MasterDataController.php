<?php

namespace App\Http\Controllers\API;

use App\models\AirportModel;
use App\models\AreaModel;
use App\Models\CategoryModel;
use App\Models\FloorModel;
use App\Models\IconModel;
use App\Models\PriorityModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MasterDataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * @param Request $request
     */
    public function show(Request $request)
    {
        $filter = $request->input('filter');
        if(empty($filter)||is_null($filter)){
            $filter = ["category" ,"icon", "floor", "airport", "priority", "area"];
        }

        if(in_array(strtolower("category"), array_map('strtolower', $filter))){
            $category = CategoryModel::all();
            $data['categories'] = $category;
        }

        if(in_array(strtolower("icon"), array_map('strtolower', $filter))){
            $icon = IconModel::all();
            $data['icons'] = $icon;
        }

        if(in_array(strtolower("priority"), array_map('strtolower', $filter))){
            $priority = PriorityModel::all();
            $data['priorities'] = $priority;
        }

        if(in_array(strtolower("floor"), array_map('strtolower', $filter))){
            $floor = FloorModel::all();
            $data['floors'] = $floor;
        }

        if(in_array(strtolower("area"), array_map('strtolower', $filter))){
            $area = AreaModel::all();
            $data['areas'] = $area;
        }

        if(in_array(strtolower("airport"), array_map('strtolower', $filter))){
            $airport = AirportModel::all();
            $data['airports'] = $airport;
        }

        return response()->json([
            'success' => true,
            'data' => $data
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getDataByAirportCode(Request $request){

        $filter = $request->input('filter');
        if(empty($filter)||is_null($filter)){
            $filter = ["category" ,"icon", "floor", "priority", "area"];
        }

        if(in_array(strtolower("category"), array_map('strtolower', $filter))){
            $category = CategoryModel::where('airportcode', $request->airportcode)->get();;
            $data['categories'] = $category;
        }

        if(in_array(strtolower("icon"), array_map('strtolower', $filter))){
            $icon = IconModel::where('airportcode', $request->airportcode)->get();;
            $data['icons'] = $icon;
        }

        if(in_array(strtolower("priority"), array_map('strtolower', $filter))){
            $priority = PriorityModel::where('airportcode', $request->airportcode)->get();;
            $data['priorities'] = $priority;
        }

        if(in_array(strtolower("floor"), array_map('strtolower', $filter))){
            $floor = FloorModel::where('airportcode', $request->airportcode)->get();;
            $data['floors'] = $floor;
        }

        if(in_array(strtolower("area"), array_map('strtolower', $filter))){
            $area = AreaModel::where('airportcode', $request->airportcode)->get();;
            $data['areas'] = $area;
        }


        return response()->json([
            'success' => true,
            'data' => $data
        ]);
    }
}
