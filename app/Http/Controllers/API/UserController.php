<?php

namespace App\Http\Controllers\API;

use App\Helpers\ApiValidationHelper;
use App\Helpers\LogAdminHelper;
use App\Helpers\SendEmailHelper;
use App\Helpers\TokenLifeHelper;
use App\Jobs\EmailVerificationJob;
use App\Mail\EmailVerification;
use App\Models\RoleModel;
use App\Models\RoleUserModel;
use App\Models\TokenModel;
use App\Models\UserModel;
use Dacastro4\LaravelGmail\Facade\LaravelGmail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        $messages = LaravelGmail::message()->subject('test')->unread()->preload()->all();
//        dd($messages);
        $validator = Validator::make($request->all(), [
            'username'  => 'required|unique:users,username',
            'email'  => 'required|email|unique:users,email',
            'password'  => 'required',
            'roles'  => 'required|array|min:1',
            'categories' => 'array',
            'airports' => 'array'
        ]);

        if($validator->fails()){
            $error = ApiValidationHelper::getFirstErrorMessage($validator->getMessageBag()->getMessages());
            return response()->json([
                'success' => false,
                'message' => $error
            ]);
        }

        //convert categories array to string, divided by comma
        $categories = null;
        if($request->input('categories')){
            $categories = implode(",", $request->input('categories'));
        }

        $airports = null;
        if($request->input('airports')){
            $airports = implode(",", $request->input('airports'));
        }

        $username = $request->input('username');
        $password = $request->input('password');
        $email = $request->input('email');
        $roles = $request->input('roles');

        /* Set transaction */
        DB::beginTransaction();

        //token for email confirmation
        $token = TokenLifeHelper::getToken(32);

        $new_user                  = new UserModel();
        $new_user->username        = $username;
        $new_user->password        = md5($password);
        $new_user->email           = $email;
        $new_user->token           = $token;
        $new_user->token_lifetime  = date('Y-m-d H:i:s', time() + 3600 * 24);
        $new_user->token_status    = 1;

        $after = $new_user->getDirty();

        $success                   = $new_user->save();

        if (!$success) {
            /* Transsaction di rollback */
            DB::rollBack();

            return response()->json([
                'success' => false,
                'message' => trans('api.add_failed', ['model' => 'user'])
            ]);

        }

        $role_name = '';
        //insert roles for user
        foreach ($roles as $roleid){
            $check_role = RoleModel::where('id', $roleid);
            if($check_role!=null){
                $role_name = $check_role->first()->name.' | '.$role_name;

                $new_role_user = new RoleUserModel();
                $new_role_user->userid = $new_user->userid;;
                $new_role_user->roleid = $roleid;
                $new_role_user->airportcodes = $airports;
                if($roleid==2){
                    $new_role_user->poicategories = $categories;
                }
                $save = $new_role_user->save();

                if(!$save){
                    DB::rollBack();

                    return response()->json([
                        'success' => false,
                        'message' => trans('api.add_failed', ['model' => 'user'])
                    ]);
                }
            }
        }

        //insert into log admin
        $user = TokenLifeHelper::getUserByToken($request->header('token'));
        $detail = LogAdminHelper::populateLogDetail(null, $after);
        $detail = $detail.' with role(s) '.$role_name.', poi category(ies) '.$categories.', airports '.$airports;
        $action = 'Add User';
        LogAdminHelper::insertToLog($user, $action, $detail);

        /* Save transaction ke DB */
        DB::commit();

        //generate link
        $link = App::make('url')->to('/confirmation/'.$token);

        $dataEmail['link'] = $link;
        $dataEmail['target_email'] = $email;
        $dataEmail['username'] = $username;
        //send email to user
        EmailVerificationJob::dispatch($dataEmail);

        return response()->json([
            'success' => true,
            'message' => trans('api.add_success', ['model' => 'user'])
        ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $userid = $request->input('userid');

        //get all
        if($userid == null){
            $users = UserModel::get(['userid', 'username', 'email', 'status']);

            foreach ($users as $key=>$user){
                $users[$key]->roles = $user->roles;

                //get poi categories
                $userRolePoi = RoleUserModel::where('userid', $user->userid)->where('roleid', 2)->first();

                if($userRolePoi){
                    $users[$key]['poicategories'] = explode(',', $userRolePoi->poicategories);
                }else{
                    $users[$key]['poicategories'] = [];
                }

                $userRole = RoleUserModel::where('userid', $user->userid)->first();
                //get airport code for this user
                if($userRole->airportcodes!=null){
                    $users[$key]['airportcodes'] = explode(',', $userRole->airportcodes);
                }else{
                    $users[$key]['airportcodes'] = [];
                }

            }

            return response()->json([
                'success' => true,
                'data' => $users->toArray()
            ]);
        }

        //get only one user
        $user = UserModel::where('userid', $userid)->first(['userid', 'username', 'email', 'status']);
        $user->roles;

        //get poi categories
        $userRolePoi = RoleUserModel::where('userid', $userid)->where('roleid', 2)->first();
        if($userRolePoi){
            $user['poicategories'] = explode(',', $userRolePoi->poicategories);
        }else{
            $user['poicategories'] = [];
        }

        $userRole = RoleUserModel::where('userid', $user->userid)->first();
        //get airport code for this user
        if($userRole->airportcodes!=null){
            $user['airportcodes'] = explode(',', $userRole->airportcodes);
        }else{
            $user['airportcodes'] = [];
        }

        return response()->json([
            'success' => true,
            'data' => $user
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * update user roles
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userid'  => 'required',
            'roles'  => 'required|array|min:1',
            'categories' => 'array',
            'airports' => 'array'
        ]);

        if($validator->fails()){
            $error = ApiValidationHelper::getFirstErrorMessage($validator->getMessageBag()->getMessages());
            return response()->json([
                'success' => false,
                'message' => $error
            ]);
        }

        $userid = $request->input('userid');
        $roles = $request->input('roles');
        //convert categories array to string, divided by comma
        $categories = null;
        if($request->input('categories')){
            $categories = implode(",", $request->input('categories'));
        }

        $airports = null;
        if($request->input('airports')){
            $airports = implode(",", $request->input('airports'));
        }

        DB::beginTransaction();

        $user = UserModel::find($userid);

        //get roles before
        $role_before = '';
        foreach ($user->roles as $role){
            $role_before = $role->name.' category(ies) '.$role->poicategories.' | '.$role_before;
        }

        //delete existing user_role
        $delete_user_role = $user->roles()->detach();

        if(!$delete_user_role){

            DB::rollBack();

            return response()->json([
                'success' => false,
                'message' => trans('api.edit_failed', ['model' => 'user', 'message' => ''])
            ]);

        }

        //insert roles for user
        foreach ($roles as $roleid){
            $check_role = RoleModel::where('id', $roleid);
            if($check_role!=null){

                $new_role_user = new RoleUserModel();
                $new_role_user->userid = $userid;
                $new_role_user->roleid = $roleid;
                $new_role_user->airportcodes = $airports;
                if($roleid==2){
                    $new_role_user->poicategories = $categories;
                }
                $save = $new_role_user->save();

                if(!$save){
                    DB::rollBack();

                    return response()->json([
                        'success' => false,
                        'message' => trans('api.edit_failed', ['model' => 'user', 'message' => 'failed insert roles'])
                    ]);
                }
            }
        }


        //get roles after
        $user = UserModel::find($userid);
        $role_after = '';
        foreach ($user->roles as $role){
            $role_after = $role->name.' | '.$role_after;
        }

        //insert into log admin
        $user = TokenLifeHelper::getUserByToken($request->header('token'));
        $action = 'Update User role(s)';
        $detail = 'ID User = '.$userid.' || '.'update roles from : '.$role_before.' to : '.$role_after.' and category(ies) '.$categories;
        LogAdminHelper::insertToLog($user, $action, $detail);

        DB::commit();

        return response()->json([
            'success' => true,
            'message' => trans('api.edit_success', ['model' => 'user'])
        ]);
    }

    /**
     * delete user
     * membuat status user menjadi -1
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request){

        $userid = $request->input('userid');

        DB::beginTransaction();

        $user = UserModel::where('userid', $userid)->first();
        $success = $user->delete();

        if(!$success){
            DB::rollBack();

            return response()->json([
                'success' => false,
                'message' => trans('api.delete_failed', ['model' => 'user', 'message' => ''])
            ]);
        }

        //insert into log admin
        $user = TokenLifeHelper::getUserByToken($request->header('token'));
        $action = 'Delete User';
        LogAdminHelper::insertToLog($user, $action, 'Delete User with userid = '.$userid);

        DB::commit();

        return response()->json([
            'success' => true,
            'message' => trans('api.delete_success', ['model' => 'user'])
        ]);


    }


}
