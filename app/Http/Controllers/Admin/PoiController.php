<?php

namespace App\Http\Controllers\Admin;

use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class PoiController extends Controller
{
    /**
     * show all poi
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $client = new Client([
            'headers' => ['token' =>  getToken() ],
        ]);

//        $response = $client->post(BASE_URL_API.'/poi/retrieve', [
//            RequestOptions::JSON => [
//                'per_page' => 10,
//            ]
//        ]);

        $responseMaster = $client->post(BASE_URL_API.'/master/retrieve', [
            RequestOptions::JSON => [

            ]
        ]);

//        $body = $response->getBody()->getContents();
//        $body = json_decode($body);

        $bodyMaster = $responseMaster->getBody()->getContents();
        $bodyMaster = json_decode($bodyMaster);

        $grantedAiports = getAirport();

        if($bodyMaster->success){

//            $data['pois'] = $body->data;
//            $data['totalPage'] = $body->total_page;
//            $data['currentPage'] = $body->current_page;

            $data['master'] = $bodyMaster->data;
            $data['master']->grantedAirport= getListGrantedAirport($bodyMaster->data->airports);

        }else{
//            $data['pois'] = array();
//            $data['totalPage'] =0;
//            $data['currentPage'] = 0;
            $data['master'] = array();
            $data['master']->grantedAirport= [];

        }

        return view('admin/poi-list', $data);
    }

    /**
     * get data poi for filter & pagination
     * @param Request $request
     * @return false|string
     */
    public function getDataPoi(Request $request){
        $client = new Client([
            'headers' => ['token' =>  getToken() ],
        ]);

        $requestData['per_page'] = 10;

        if($request->input('categoryid')){
            $requestData['categoryid'] = $request->input('categoryid');
        }

        if($request->input('floorid')){
            $requestData['floorid'] = $request->input('floorid');
        }

        if($request->input('airportcode')){
            $requestData['airportcode'] = $request->input('airportcode');
        }

        if($request->input('areaid')){
            $requestData['areaid'] = $request->input('areaid');
        }

        if($request->input('name')){
            $requestData['name'] = $request->input('name');
        }

        if($request->input('page_number')){
            $requestData['page_number'] = $request->input('page_number');
        }

        $response = $client->post(BASE_URL_API.'/poi/retrieve', [
            RequestOptions::JSON => $requestData
        ]);

        $body = $response->getBody()->getContents();
        $body = json_decode($body);

        if($body->success){

            return json_encode($body);

        }else{
            return json_encode(['success'=>false]);
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
//        $client = new Client([
//            'headers' => ['token' =>  getToken() ],
//        ]);
//
//        $response = $client->post(BASE_URL_API.'/master/retrieve', [
//            RequestOptions::JSON => [
//
//            ]
//        ]);
//
//        $body = $response->getBody()->getContents();
//        $body = json_decode($body);
//
//        if($body->success){
//
//            $data['masters'] = $body->data;
//
//        }else{
//            $data['masters'] = array();
//        }
//
//        return view('admin.poi-create', $data);
    }

    /**
     * store new POI
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'airportcode'  => 'required',
            'areaid'  => 'required',
            'name'  => 'required',
            'categoryid'  => 'required',
            'priorityid'  => 'required',
            'floorid'  => 'required',
            'iconid'  => 'required',
            'x'  => 'required|numeric',
            'y'  => 'required|numeric',
            'z'  => 'required|numeric',
        ]);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $client = new Client([
            'headers' => ['token' =>  getToken() ],
        ]);

        $response = $client->post(BASE_URL_API.'/poi/create', [
            RequestOptions::JSON => [
                'airportcode' => $request->input('airportcode'),
                'areaid' => $request->input('areaid'),
                'name' => $request->input('name'),
                'categoryid' => $request->input('categoryid'),
                'priorityid' => $request->input('priorityid'),
                'floorid' => $request->input('floorid'),
                'iconid' => $request->input('iconid'),
                'x' => $request->input('x'),
                'y' => $request->input('y'),
                'z' => $request->input('z'),
            ]
        ]);

        $body = $response->getBody()->getContents();
        $body = json_decode($body);

        if($body->success){

            $success = $body->message;
            return redirect('/admin/poi')->withSuccess($success);

        }else{
            $error = $body->message;
            return redirect()->back()->withErrors($error)->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * update poi
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'airportcode'  => 'required',
            'areaid'  => 'required',
            'name'  => 'required',
            'categoryid'  => 'required',
            'priorityid'  => 'required',
            'floorid'  => 'required',
            'iconid'  => 'required',
            'x'  => 'required|numeric',
            'y'  => 'required|numeric',
            'z'  => 'required|numeric',
            'id'  => 'required',
        ]);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $client = new Client([
            'headers' => ['token' =>  getToken() ],
        ]);

        $response = $client->post(BASE_URL_API.'/poi/update', [
            RequestOptions::JSON => [
                'airportcode' => $request->input('airportcode'),
                'areaid' => $request->input('areaid'),
                'name' => $request->input('name'),
                'categoryid' => $request->input('categoryid'),
                'priorityid' => $request->input('priorityid'),
                'floorid' => $request->input('floorid'),
                'iconid' => $request->input('iconid'),
                'x' => $request->input('x'),
                'y' => $request->input('y'),
                'z' => $request->input('z'),
                'id' => $request->input('id'),
            ]
        ]);


        $body = $response->getBody()->getContents();
        $body = json_decode($body);

        if($body->success){

            $success = $body->message;
            return redirect('/admin/poi')->withSuccess($success);

        }else{
            $error = $body->message;
            return redirect()->back()->withErrors($error)->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request )
    {

        $validator = Validator::make($request->all(), [
            'ids'  => 'required|array|min:1',
        ]);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $client = new Client([
            'headers' => ['token' =>  getToken() ],
        ]);

        $response = $client->post(BASE_URL_API.'/poi/delete', [
            RequestOptions::JSON => [
                'ids' => $request->input('ids'),
            ]
        ]);


        $body = $response->getBody()->getContents();
        $body = json_decode($body);

        if($body->success){

            $success = $body->message;
            return redirect('/admin/poi')->withSuccess($success);

        }else{
            $error = $body->message;
            return redirect()->back()->withErrors($error)->withInput();
        }

    }
}
