<?php

namespace App\Http\Controllers\Admin;

use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MasterController extends Controller
{
    public function getAirport(){
        $client = new Client([
            'headers' => ['token' =>  getToken() ],
        ]);

        $responseMaster = $client->post(BASE_URL_API.'/master/retrieve', [
            RequestOptions::JSON => [
                "filter" => ["airport"]
            ]
        ]);

        $bodyMaster = $responseMaster->getBody()->getContents();
        $bodyMaster = json_decode($bodyMaster);

        if($bodyMaster->success){

            return json_encode($bodyMaster);

        }else{
            return json_encode(['success'=>false]);
        }

    }

    public function getDataByAirportCode(Request $request){
        $client = new Client([
            'headers' => ['token' =>  getToken() ],
        ]);

        $response = $client->post(BASE_URL_API.'/master/retrievebyairportcode', [
            RequestOptions::JSON => [
                "airportcode" => $request->airportcode,
                "filter" => $request->filter
            ]
        ]);

        $grantedCategory = getRolesCategory();
        $body = $response->getBody()->getContents();
        $body = json_decode($body);

        if($body->success){
            if(isset($body->data->categories)){
                $listCategory = [];
                if(!empty($grantedCategory)){
                    foreach ($body->data->categories as $key=>$category){
                        if(in_array($category->id, $grantedCategory, false)){
                            array_push($listCategory, $category);
                        }
                    }

                    $body->data->categories = $listCategory;
                }
            }

            return json_encode($body);

        }else{
            return json_encode(['success'=>false]);
        }
    }

    public function getCategoryByAirport(Request $request){
        $client = new Client([
            'headers' => ['token' =>  getToken() ],
        ]);

        $response = $client->post(BASE_URL_API.'/category/retrieve', [
            RequestOptions::JSON => [
                "per_page" => 1000,
                "page_number" => 1,
                "airportcode" => $request->airportcode
            ]
        ]);

        $body = $response->getBody()->getContents();
        $body = json_decode($body);

        if($body->success){

            return json_encode($body);

        }else{
            return json_encode(['success'=>false]);
        }

    }

}
