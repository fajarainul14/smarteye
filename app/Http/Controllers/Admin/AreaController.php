<?php

namespace App\Http\Controllers\Admin;

use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class AreaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $body = $this->prepareData();

        if($body->success){
            $data['airports'] = getListGrantedAirport($body->data->airports);
        }else{
            $data['airports'] = array();
        }

        return view('admin/area-list', $data);
    }

    public function prepareData(){
        $client = new Client([
            'headers' => ['token' =>  getToken() ],
        ]);

        $responseMaster = $client->post(BASE_URL_API.'/master/retrieve', [
            RequestOptions::JSON => [

            ]
        ]);

        $bodyMaster = $responseMaster->getBody()->getContents();
        $bodyMaster = json_decode($bodyMaster);

        return $bodyMaster;
    }

    /**
     * get data area for filter & pagination
     * @param Request $request
     * @return false|string
     */
    public function getDataArea(Request $request){
        $client = new Client([
            'headers' => ['token' =>  getToken() ],
        ]);

        $requestData['per_page'] = 10;

        if($request->input('description')){
            $requestData['description'] = $request->input('description');
        }

        $airportCode = $request->input('airportcode');
        if($airportCode==null || empty($airportCode)){
            $airportCode = getAirport();
        }
        $requestData['airportcode'] = $airportCode;

        if($request->input('page_number')){
            $requestData['page_number'] = $request->input('page_number');
        }

        $response = $client->post(BASE_URL_API.'/area/retrieve', [
            RequestOptions::JSON => $requestData
        ]);

        $body = $response->getBody()->getContents();
        $body = json_decode($body);

        if($body->success){

            return json_encode($body);

        }else{
            return json_encode(['success'=>false]);
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'description'  => 'required',
            'airportcode' => 'required'
        ]);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $client = new Client([
            'headers' => ['token' =>  getToken() ],
        ]);

        $response = $client->post(BASE_URL_API.'/area/create', [
            RequestOptions::JSON => [
                'description' => $request->input('description'),
                'airportcode' => $request->input('airportcode'),
            ]
        ]);

        $body = $response->getBody()->getContents();
        $body = json_decode($body);

        if($body->success){

            $success = $body->message;
            return redirect('/admin/area')->withSuccess($success);

        }else{
            $error = $body->message;
            return redirect()->back()->withErrors($error)->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'description'  => 'required',
            'area_id'  => 'required',
            'airportcode' => 'required'
        ]);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $client = new Client([
            'headers' => ['token' =>  getToken() ],
        ]);

        $response = $client->post(BASE_URL_API.'/area/update', [
            RequestOptions::JSON => [
                'description' => $request->input('description'),
                'id' => $request->input('area_id'),
                'airportcode' => $request->input('airportcode'),
            ]
        ]);

        $body = $response->getBody()->getContents();
        $body = json_decode($body);

        if($body->success){

            $success = $body->message;
            return redirect('/admin/area')->withSuccess($success);

        }else{
            $error = $body->message;
            return redirect()->back()->withErrors($error)->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $client = new Client([
            'headers' => ['token' =>  getToken() ],
        ]);

        $response = $client->post(BASE_URL_API.'/area/delete', [
            RequestOptions::JSON => [
                'id' => $id,
            ]
        ]);

        $body = $response->getBody()->getContents();
        $body = json_decode($body);

        if($body->success){

            $success = $body->message;
            return redirect('/admin/area')->withSuccess($success);

        }else{
            $error = $body->message;
            return redirect()->back()->withErrors($error)->withInput();
        }
    }

    public function showTrashed(){

        $bodyMaster = $this->prepareData();

        if($bodyMaster->success){
            $data['airports'] = getListGrantedAirport($bodyMaster->data->airports);
        }else{
            $data['airports'] = array();
        }


        return view('admin/area-list-trashed', $data);

    }

    public function getTrashedDataArea(Request $request){
        $client = new Client([
            'headers' => ['token' =>  getToken() ],
        ]);

        $requestData['per_page'] = 10;

        if($request->input('description')){
            $requestData['description'] = $request->input('description');
        }

        $airportCode = $request->input('airportcode');
        if($airportCode==null || empty($airportCode)){
            $airportCode = getAirport();
        }
        $requestData['airportcode'] = $airportCode;

        if($request->input('page_number')){
            $requestData['page_number'] = $request->input('page_number');
        }

        $response = $client->post(BASE_URL_API.'/area/retrievetrashed', [
            RequestOptions::JSON => $requestData
        ]);

        $body = $response->getBody()->getContents();
        $body = json_decode($body);

        if($body->success){

            return json_encode($body);

        }else{
            return json_encode(['success'=>false]);
        }
    }

    public function restore($id){
        $client = new Client([
            'headers' => ['token' =>  getToken() ],
        ]);

        $response = $client->post(BASE_URL_API.'/area/restore', [
            RequestOptions::JSON => [
                'id' => $id,
            ]
        ]);

        $body = $response->getBody()->getContents();
        $body = json_decode($body);

        if($body->success){

            $success = $body->message;
            return redirect('/admin/area/trash')->withSuccess($success);

        }else{
            $error = $body->message;
            return redirect()->back()->withErrors($error)->withInput();
        }
    }

    public function destroyPermanent($id)
    {
        $client = new Client([
            'headers' => ['token' =>  getToken() ],
        ]);

        $response = $client->post(BASE_URL_API.'/area/deletepermanent', [
            RequestOptions::JSON => [
                'id' => $id,
            ]
        ]);

        $body = $response->getBody()->getContents();
        $body = json_decode($body);

        if($body->success){

            $success = $body->message;
            return redirect('/admin/area/trash')->withSuccess($success);

        }else{
            $error = $body->message;
            return redirect()->back()->withErrors($error)->withInput();
        }
    }
}
