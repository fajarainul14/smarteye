<?php

namespace App\Http\Controllers\Admin;

use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class PromoController extends Controller
{
    /**
     * menampilkan promo
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $client = new Client([
            'headers' => ['token' =>  getToken() ],
        ]);

//        $response = $client->post(BASE_URL_API.'/promo/retrieve', [
//            RequestOptions::JSON => [
//                'per_page' => 20,
//            ]
//        ]);

        $responseMaster = $client->post(BASE_URL_API.'/master/retrieve', [
            RequestOptions::JSON => [

            ]
        ]);

//        $body = $response->getBody()->getContents();
//        $body = json_decode($body);

        $bodyMaster = $responseMaster->getBody()->getContents();
        $bodyMaster = json_decode($bodyMaster);

        $grantedAiports = getAirport();

        if($bodyMaster->success){

//            $data['promos'] = $body->data;
//            $data['totalPage'] = $body->total_page;
//            $data['currentPage'] = $body->current_page;

            $data['master'] = $bodyMaster->data;
            $data['master']->grantedAirport= getListGrantedAirport($bodyMaster->data->airports);

        }else{
//            $data['promos'] = array();
//            $data['totalPage'] =0;
//            $data['currentPage'] = 0;
            $data['master'] = array();
            $data['master']->grantedAirport= [];

        }
        return view('admin/promo-list', $data);
    }

    /**
     * get data poi for filter & pagination
     * @param Request $request
     * @return false|string
     */
    public function getDataPromo(Request $request){
        $client = new Client([
            'headers' => ['token' =>  getToken() ],
        ]);

        $requestData['per_page'] = 10;

        if($request->input('floorid')){
            $requestData['floorid'] = $request->input('floorid');
        }

        if($request->input('promoname')){
            $requestData['promoname'] = $request->input('promoname');
        }

        if($request->input('page_number')){
            $requestData['page_number'] = $request->input('page_number');
        }

        $airportCode = $request->input('airportcode');
        if($airportCode==null || empty($airportCode)){
            $airportCode = getAirport();
        }

        if($request->input('airportcode')){
            $requestData['airportcode'] = $airportCode;
        }

        $response = $client->post(BASE_URL_API.'/promo/retrieve', [
            RequestOptions::JSON => $requestData
        ]);

        $body = $response->getBody()->getContents();
        $body = json_decode($body);

        if($body->success){

            return json_encode($body);

        }else{
            return json_encode(['success'=>false]);
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * store new promo
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'airportcode'  => 'required',
            'promoname'  => 'required',
            'promotext'  => 'required',
            'promoimage'  => 'required',
            'floorid'  => 'required',
            'iconid'  => 'required',
            'is_show_icon'  => 'boolean',
            'x'  => 'required|numeric',
            'y'  => 'required|numeric',
            'z'  => 'required|numeric',
        ]);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }

        //convert image to base 64
        $file = Input::file('promoimage');

        $filename = str_random(10).'-'.$file->getClientOriginalName();

        $type = $file->getClientOriginalExtension();

        $uploadSuccess = $file->move(public_path('temp'), $filename);

        $imagedata = file_get_contents(public_path('temp/'.$filename));

        $base64Image = 'data:image/' . $type . ';base64,' . base64_encode($imagedata);

        File::delete(public_path('temp/'.$filename));

        //check showicon
        if($request->input('is_show_icon')){
            $showIcon = true;
        }else{
            $showIcon = false;
        }

        $client = new Client([
            'headers' => ['token' =>  getToken() ],
        ]);

        $response = $client->post(BASE_URL_API.'/promo/create', [
            RequestOptions::JSON => [
                'airportcode' => $request->input('airportcode'),
                'promoname' => $request->input('promoname'),
                'promotext' => $request->input('promotext'),
                'promoimage' => $base64Image,
                'floorid' => $request->input('floorid'),
                'showicon' => $showIcon,
                'iconid' => $request->input('iconid'),
                'x' => $request->input('x'),
                'y' => $request->input('y'),
                'z' => $request->input('z'),
            ]
        ]);

        $body = $response->getBody()->getContents();
        $body = json_decode($body);

        if($body->success){

            $success = $body->message;
            return redirect('/admin/promo')->withSuccess($success);

        }else{
            $error = $body->message;
            return redirect()->back()->withErrors($error)->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * update promo
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'airportcode'  => 'required',
            'promoname'  => 'required',
            'promotext'  => 'required',
            'promoimage'  => 'required_if:is_change_image,true',
            'floorid'  => 'required',
            'iconid'  => 'required',
            'is_show_icon'  => 'boolean',
            'is_change_image'  => 'boolean',
            'x'  => 'required|numeric',
            'y'  => 'required|numeric',
            'z'  => 'required|numeric',
        ]);


        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }


        //convert image to base 64
        if($request->input('is_change_image')){
            $file = Input::file('promoimage');

            $filename = str_random(10).'-'.$file->getClientOriginalName();

            $type = $file->getClientOriginalExtension();

            $uploadSuccess = $file->move(public_path('temp'), $filename);

            $imagedata = file_get_contents(public_path('temp/'.$filename));

            $base64Image = 'data:image/' . $type . ';base64,' . base64_encode($imagedata);

            File::delete(public_path('temp/'.$filename));
        }else{
            $base64Image = '';
        }


        //check showicon
        if($request->input('is_show_icon')){
            $showIcon = true;
        }else{
            $showIcon = false;
        }

        //check showicon
        if($request->input('is_change_image')){
            $isChangeImage = true;
        }else{
            $isChangeImage = false;
        }

        $client = new Client([
            'headers' => ['token' =>  getToken() ],
        ]);

        $response = $client->post(BASE_URL_API.'/promo/update', [
            RequestOptions::JSON => [
                'airportcode' => $request->input('airportcode'),
                'promoname' => $request->input('promoname'),
                'promotext' => $request->input('promotext'),
                'promoimage' => $base64Image,
                'floorid' => $request->input('floorid'),
                'showicon' => $showIcon,
                'iconid' => $request->input('iconid'),
                'x' => $request->input('x'),
                'y' => $request->input('y'),
                'z' => $request->input('z'),
                'id' => $request->input('id'),
                'is_change_image' => $isChangeImage,
            ]
        ]);


        $body = $response->getBody()->getContents();
        $body = json_decode($body);

        if($body->success){

            $success = $body->message;
            return redirect('/admin/promo')->withSuccess($success);

        }else{
            $error = $body->message;
            return redirect()->back()->withErrors($error)->withInput();
        }
    }

    /**
     * delete promos by id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'ids'  => 'required|array|min:1',
        ]);


        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $client = new Client([
            'headers' => ['token' =>  getToken() ],
        ]);

        $response = $client->post(BASE_URL_API.'/promo/delete', [
            RequestOptions::JSON => [
                'ids' => $request->input('ids'),
            ]
        ]);


        $body = $response->getBody()->getContents();
        $body = json_decode($body);

        if($body->success){

            $success = $body->message;
            return redirect('/admin/promo')->withSuccess($success);

        }else{
            $error = $body->message;
            return redirect()->back()->withErrors($error)->withInput();
        }

    }
}
