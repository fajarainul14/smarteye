<?php

namespace App\Http\Controllers\Admin;

use App\Models\RoleModel;
use App\Models\UserModel;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $client = new Client([
            'headers' => ['token' =>  getToken() ],
        ]);

        $response = $client->post(BASE_URL_API.'/user/retrieve', [
            RequestOptions::JSON => [
                'per_page' => 20,
            ]
        ]);

        $responseMaster = $client->post(BASE_URL_API.'/master/retrieve', [
            RequestOptions::JSON => [

            ]
        ]);

        $body = $response->getBody()->getContents();
        $body = json_decode($body);

        $bodyMaster = $responseMaster->getBody()->getContents();
        $bodyMaster = json_decode($bodyMaster);

        if($body->success){

            $data['users'] = $body->data;
            $data['roles'] = RoleModel::get();
            $data['categories'] = $bodyMaster->data->categories; //get only categories from master
            $data['airports'] = $bodyMaster->data->airports; //get only categories from master

        }else{
            $data['users'] = array();
            $data['roles'] = array();
            $data['categories'] = array();
            $data['airports'] = array();
        }

        return view('admin/user-list', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!$request->input('categories')){
            $request['categories'] = array();
        }

        $validator = Validator::make($request->all(), [
            'username'  => 'required',
            'password'  => 'required',
            'email'  => 'required|email',
            'roles'  => 'required|array|min:1',
            'categories' => 'array',
            'airports' => 'array'
        ]);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $client = new Client([
            'headers' => ['token' =>  getToken() ],
        ]);

        $airports = $request->input('airports');
        if($airports[0] == -1){
            $airports = [];
        }

        $response = $client->post(BASE_URL_API.'/user/create', [
            RequestOptions::JSON => [
                'username'  => $request->input('username'),
                'password'  => $request->input('password'),
                'email'  => $request->input('email'),
                'roles'  => $request->input('roles'),
                'categories'  => $request->input('categories'),
                'airports'  => $airports,
            ]
        ]);

        $body = $response->getBody()->getContents();
        $body = json_decode($body);

        if($body->success){

            $success = $body->message;
            return redirect('/admin/user')->withSuccess($success);

        }else{
            $error = $body->message;
            return redirect()->back()->withErrors($error)->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if(!$request->input('categories')){
            $request['categories'] = array();
        }

        $validator = Validator::make($request->all(), [
            'userid'  => 'required',
            'roles'  => 'required|array|min:1',
            'categories' => 'array',
            'airports' => 'array'
        ]);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $client = new Client([
            'headers' => ['token' =>  getToken() ],
        ]);

        $airports = $request->input('airports');
        if($airports[0] == -1){
            $airports = [];
        }

        $response = $client->post(BASE_URL_API.'/user/update', [
            RequestOptions::JSON => [
                'userid'  => $request->input('userid'),
                'roles'  => $request->input('roles'),
                'categories'  => $request->input('categories'),
                'airports' => $airports
            ]
        ]);

        $body = $response->getBody()->getContents();
        $body = json_decode($body);

        if($body->success){

            $success = $body->message;
            return redirect('/admin/user')->withSuccess($success);

        }else{
            $error = $body->message;
            return redirect()->back()->withErrors($error)->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $client = new Client([
            'headers' => ['token' =>  getToken() ],
        ]);

        $response = $client->post(BASE_URL_API.'/user/delete', [
            RequestOptions::JSON => [
                'userid' => $id,
            ]
        ]);


        $body = $response->getBody()->getContents();
        $body = json_decode($body);

        if($body->success){

            $success = $body->message;
            return redirect('/admin/user')->withSuccess($success);

        }else{
            $error = $body->message;
            return redirect()->back()->withErrors($error)->withInput();
        }
    }

    /**
     * fungsi untuk konfirmasi user
     * @param $token
     */
    public function confirmation($token){
        if($token==null || $token ==''){
            dd('token null');
        }

        //check token expired time
        $data_user = UserModel::where([
            ['token_lifetime', '>=', date('Y-m-d H:i:s', time())],
            ['token', $token],
            ['token_status', 1]
        ])->first();

        //jika tidak ditemukan
        if($data_user==null){
            $data['success'] = false;
            $data['message'] = 'User not found, please contact Admin';

            return view('confirmation-page', $data);
            //TODO : provide view for this condition
        }

        DB::beginTransaction();

        $data_user->status = 1;
        $data_user->token_status = 0;
        $success = $data_user->save();

        if(!$success){
            DB::rollBack();
            $data['success'] = false;
            $data['message'] = 'Failed update user data, please contact Admin';
            return view('confirmation-page');
            //TODO : provide view for this condition
        }

        DB::commit();

        $data['success'] = true;
        $data['message'] = 'You are now an Admin for this application';

        return view('confirmation-page', $data);
        //TODO : provide view for this condition

    }
}
