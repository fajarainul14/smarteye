<?php

namespace App\Http\Controllers\Admin;

use App\Models\UserModel;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(session()->has('userdata')){
            return redirect('/admin');
        }
        return view('login');
    }

    public function login(Request $request){

        $validator = Validator::make($request->all(), [
            'username'  => 'required',
            'password'  => 'required'
        ]);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $username = $request->input('username');
        $password = $request->input('password');

        $client = new Client();
        $response = $client->post(BASE_URL_API.'/login',  [
            RequestOptions::JSON => [
                'username' => $username,
                'password' => $password,
            ]
        ]);

        $body = $response->getBody()->getContents();
        $body = json_decode($body);
        if($body->success){
            session(['userdata' => $body->data]);

            // if successful, then redirect to their intended location
            $success = "Login Success, ".$body->message;
            return redirect('/admin')->withSuccess($success);
        }else{
            $error = "Login failed, ".$body->message;
            return redirect()->back()->withErrors($error)->withInput($request->only('username'));
        }

    }

    public function logout(){
        session()->forget('userdata');

        $success = "Logout success";
        return redirect('/admin/login')->withSuccess($success);
    }
}
