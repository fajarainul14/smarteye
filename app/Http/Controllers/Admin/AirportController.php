<?php

namespace App\Http\Controllers\Admin;

use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class AirportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $client = new Client([
            'headers' => ['token' =>  getToken() ],
        ]);

        $response = $client->post(BASE_URL_API.'/airport/retrieve', [
            RequestOptions::JSON => [
                'per_page' => 20,
            ]
        ]);

        $body = $response->getBody()->getContents();
        $body = json_decode($body);

        if($body->success){

            $data['airports'] = $body->data;

        }else{
            $data['airports'] = array();
        }

        return view('admin/airport-list', $data);
    }

    /**
     * get data airport for filter & pagination
     * @param Request $request
     * @return false|string
     */
    public function getDataAirport(Request $request){
        $client = new Client([
            'headers' => ['token' =>  getToken() ],
        ]);

        $requestData['per_page'] = 10;

        if($request->input('airportcode')){
            $requestData['airportcode'] = $request->input('airportcode');
        }

        if($request->input('airportname')){
            $requestData['airportname'] = $request->input('airportname');
        }

        if($request->input('page_number')){
            $requestData['page_number'] = $request->input('page_number');
        }

        $response = $client->post(BASE_URL_API.'/airport/retrieve', [
            RequestOptions::JSON => $requestData
        ]);

        $body = $response->getBody()->getContents();
        $body = json_decode($body);

        if($body->success){

            return json_encode($body);

        }else{
            return json_encode(['success'=>false]);
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'airportcode'  => 'required',
            'airportname'  => 'required',
        ]);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $client = new Client([
            'headers' => ['token' =>  getToken() ],
        ]);

        $response = $client->post(BASE_URL_API.'/airport/create', [
            RequestOptions::JSON => [
                'airportcode' => $request->input('airportcode'),
                'airportname' => $request->input('airportname'),
            ]
        ]);

        $body = $response->getBody()->getContents();
        $body = json_decode($body);

        if($body->success){

            $success = $body->message;
            return redirect('/admin/airport')->withSuccess($success);

        }else{
            $error = $body->message;
            return redirect()->back()->withErrors($error)->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'oldairportcode'  => 'required',
            'airportname'  => 'required',
            'airportcode'  => 'required',
        ]);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $client = new Client([
            'headers' => ['token' =>  getToken() ],
        ]);

        $response = $client->post(BASE_URL_API.'/airport/update', [
            RequestOptions::JSON => [
                'airportcode' => $request->input('airportcode'),
                'airportname' => $request->input('airportname'),
                'oldairportcode' => $request->input('oldairportcode'),
            ]
        ]);

        $body = $response->getBody()->getContents();
        $body = json_decode($body);

        if($body->success){

            $success = $body->message;
            return redirect('/admin/airport')->withSuccess($success);

        }else{
            $error = $body->message;
            return redirect()->back()->withErrors($error)->withInput();
        }
    }

    /**
     * @param String $airportcode
     */
    public function destroy($airportcode)
    {
        $client = new Client([
            'headers' => ['token' =>  getToken() ],
        ]);

        $response = $client->post(BASE_URL_API.'/airport/delete', [
            RequestOptions::JSON => [
                'airportcode' => $airportcode,
            ]
        ]);

        $body = $response->getBody()->getContents();
        $body = json_decode($body);

        if($body->success){

            $success = $body->message;
            return redirect('/admin/airport')->withSuccess($success);

        }else{
            $error = $body->message;
            return redirect()->back()->withErrors($error)->withInput();
        }
    }

    public function showTrashed(){

        $client = new Client([
            'headers' => ['token' =>  getToken() ],
        ]);

        $response = $client->post(BASE_URL_API.'/airport/retrievetrashed', [
            RequestOptions::JSON => [
                'per_page' => 20,
            ]
        ]);

        $body = $response->getBody()->getContents();
        $body = json_decode($body);

        if($body->success){

            $data['airports'] = $body->data;

        }else{
            $data['airports'] = array();
        }

        return view('admin/airport-list-trashed', $data);

    }

    public function restore($airportcode){
        $client = new Client([
            'headers' => ['token' =>  getToken() ],
        ]);

        $response = $client->post(BASE_URL_API.'/airport/restore', [
            RequestOptions::JSON => [
                'airportcode' => $airportcode,
            ]
        ]);

        $body = $response->getBody()->getContents();
        $body = json_decode($body);

        if($body->success){

            $success = $body->message;
            return redirect('/admin/airport/trash')->withSuccess($success);

        }else{
            $error = $body->message;
            return redirect()->back()->withErrors($error)->withInput();
        }
    }

    public function destroyPermanent($airportcode)
    {
        $client = new Client([
            'headers' => ['token' =>  getToken() ],
        ]);

        $response = $client->post(BASE_URL_API.'/airport/deletepermanent', [
            RequestOptions::JSON => [
                'airportcode' => $airportcode,
            ]
        ]);

        $body = $response->getBody()->getContents();
        $body = json_decode($body);

        if($body->success){

            $success = $body->message;
            return redirect('/admin/airport/trash')->withSuccess($success);

        }else{
            $error = $body->message;
            return redirect()->back()->withErrors($error)->withInput();
        }
    }

}
