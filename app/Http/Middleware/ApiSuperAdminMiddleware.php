<?php

namespace App\Http\Middleware;

use App\Models\RoleUserModel;
use App\Models\TokenModel;
use Closure;

class ApiSuperAdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $user = TokenModel::where('token', $request->header('token'))->first();
        $user = $user->user;

        $superadmin = $user->roles()->where('id', 1)->where('name', 'superadmin')->first();

        if ($superadmin!=null) {
            return $next($request);
        } else {
            return response()->json([
                'success'     => false,
                'message' => trans('api.not_allowed')
            ], 200);
        }
    }
}
