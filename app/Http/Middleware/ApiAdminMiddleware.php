<?php

namespace App\Http\Middleware;

use App\Models\TokenModel;
use Closure;

class ApiAdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, ...$roles)
    {
        $user = TokenModel::where('token', $request->header('token'))->first();
        $user = $user->user;
        $userRoles = $user->roles;

        foreach ($userRoles as $userRole){
            if(in_array($userRole->name, $roles)){
                return $next($request);
            }
        }

        return response()->json([
            'success'     => false,
            'message' => trans('api.not_allowed')
        ], 200);

    }
}
