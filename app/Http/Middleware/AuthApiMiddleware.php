<?php

namespace App\Http\Middleware;

use App\Helpers\TokenLifeHelper;
use Closure;
use Illuminate\Http\Request;

class AuthApiMiddleware
{
    /**
     * @param $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $tokenLife = new TokenLifeHelper();

        $token = $tokenLife->checkLifetimeTokenByToken($request->header('token'));

        if ($token) {
            return $next($request);
        } else {
            return response()->json([
                'res'     => false,
                'message' => trans('api.token_expired')
            ], 401);
        }
    }
}
