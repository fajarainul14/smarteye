<?php

namespace App\Http\Middleware;

use Closure;

class AdminSuperMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $admin = session('userdata');

        if($admin == null){
            $error = "Please login first.";
            return redirect('/admin/login')->withErrors($error);
        }else{

            $roles = $admin->roles;
            foreach ($roles as $role){

                if($role->id==1 && $role->name=='superadmin'){
                    return $next($request);
                }

            }

        }

        $error = 'You are not allowed for this action';
        return redirect('/admin')->withErrors($error);
    }
}
