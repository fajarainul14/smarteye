<?php

namespace App\Http\Middleware;

use App\Models\TokenModel;
use Closure;

class ApiAdminPoiMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = TokenModel::where('token', $request->header('token'))->first();
        $user = $user->user;

        $adminPoi = $user->roles()->where('id', 2)->where('name', 'adminpoi')->first();

        if ($adminPoi!=null) {
            return $next($request);
        } else {
            return response()->json([
                'success'     => false,
                'message' => trans('api.not_allowed')
            ], 200);
        }
    }
}
