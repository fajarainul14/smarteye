<?php

namespace App\Http\Middleware;

use Closure;

class AuthAdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $admin = session('userdata');

        if($admin == null){
            $error = "Please login first.";
            return redirect('/admin/login')->withErrors($error);
        }

        return $next($request);
    }
}
