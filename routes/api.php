<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::prefix('client')->group(function() {

    Route::post('getPOI', 'Client\ServiceController@getPOIData');

});

Route::post('login', 'API\AuthController@login');

//only for logged in user
Route::group(['middleware' => ['auth.api']], function () {

    Route::post('master/retrieve', 'API\MasterDataController@show');
    Route::post('master/retrievebyairportcode', 'API\MasterDataController@getDataByAirportCode');


    //FOR COMBINATION
    Route::group(['middleware' => ['auth.api.apiadmin:superadmin,adminicon']], function () {


    });

    //only for superadmin
    Route::group(['middleware' => ['auth.api.apiadmin:superadmin']], function () {

        //for manage user
        Route::post('user/retrieve', 'API\UserController@show');
        Route::post('user/create', 'API\UserController@store');
        Route::post('user/delete', 'API\UserController@destroy');
        Route::post('user/update', 'API\UserController@update');

        //for manage LOG
        Route::post('log/retrieve', 'API\LogController@show');

        //RESTORE TRASHED
        Route::post('airport/restore', 'API\AirportController@restore');
        Route::post('area/restore', 'API\AreaController@restore');
        Route::post('category/restore', 'API\CategoryController@restore');
        Route::post('floor/restore', 'API\FloorController@restore');
        Route::post('priority/restore', 'API\PriorityController@restore');
        Route::post('icon/restore', 'API\IconController@restore');

        //DELETE PERMANENT
        Route::post('airport/deletepermanent', 'API\AirportController@destroyPermanent');
        Route::post('category/deletepermanent', 'API\CategoryController@destroyPermanent');
        Route::post('area/deletepermanent', 'API\AreaController@destroyPermanent');
        Route::post('floor/deletepermanent', 'API\FloorController@destroyPermanent');
        Route::post('priority/deletepermanent', 'API\PriorityController@destroyPermanent');
        Route::post('icon/deletepermanent', 'API\IconController@destroyPermanent');

        //SHOW TRASHED
        Route::post('airport/retrievetrashed', 'API\AirportController@showTrashed');
        Route::post('category/retrievetrashed', 'API\CategoryController@showTrashed');
        Route::post('area/retrievetrashed', 'API\AreaController@showTrashed');
        Route::post('floor/retrievetrashed', 'API\FloorController@showTrashed');
        Route::post('priority/retrievetrashed', 'API\PriorityController@showTrashed');
        Route::post('icon/retrievetrashed', 'API\IconController@showTrashed');


    });

    //ONLY FOR ADMIN POI
    Route::group(['middleware' => ['auth.api.apiadmin:adminpoi']], function () {

        //for manage POI
        Route::post('poi/retrieve', 'API\PoiController@show');
        Route::post('poi/create', 'API\PoiController@store');
        Route::post('poi/delete', 'API\PoiController@destroy');
        Route::post('poi/update', 'API\PoiController@update');

    });

    //ONLY FOR ADMIN PROMO
    Route::group(['middleware' => ['auth.api.apiadmin:adminpromo']], function () {

        Route::post('promo/create', 'API\PromoController@store');
        Route::post('promo/retrieve', 'API\PromoController@show');
        Route::post('promo/update', 'API\PromoController@update');
        Route::post('promo/delete', 'API\PromoController@destroy');

    });

    //ONLY FOR ADMIN ICON
    Route::group(['middleware' => ['auth.api.apiadmin:adminicon']], function () {

        //for manage AIRPORT
        Route::post('airport/create', 'API\AirportController@store');
        Route::post('airport/retrieve', 'API\AirportController@show');
        Route::post('airport/update', 'API\AirportController@update');
        Route::post('airport/delete', 'API\AirportController@destroy');

        //for manage AREA
        Route::post('area/create', 'API\AreaController@store');
        Route::post('area/retrieve', 'API\AreaController@show');
        Route::post('area/update', 'API\AreaController@update');
        Route::post('area/delete', 'API\AreaController@destroy');

        //for manage CATEGORY
        Route::post('category/create', 'API\CategoryController@store');
        Route::post('category/retrieve', 'API\CategoryController@show');
        Route::post('category/update', 'API\CategoryController@update');
        Route::post('category/delete', 'API\CategoryController@destroy');

        //for manage FLOOR
        Route::post('floor/create', 'API\FloorController@store');
        Route::post('floor/retrieve', 'API\FloorController@show');
        Route::post('floor/update', 'API\FloorController@update');
        Route::post('floor/delete', 'API\FloorController@destroy');

        //for manage PRIORITY
        Route::post('priority/create', 'API\PriorityController@store');
        Route::post('priority/retrieve', 'API\PriorityController@show');
        Route::post('priority/update', 'API\PriorityController@update');
        Route::post('priority/delete', 'API\PriorityController@destroy');

        //for manage ICON
        Route::post('icon/create', 'API\IconController@store');
        Route::post('icon/retrieve', 'API\IconController@show');
        Route::post('icon/update', 'API\IconController@update');
        Route::post('icon/delete', 'API\IconController@destroy');

    });

});
