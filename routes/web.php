<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;



Route::get('confirmation/{token}', 'Admin\UserController@confirmation');

Route::prefix('admin')->group(function() {

    Route::get('/login', 'Admin\AuthController@index');
    Route::post('/login', 'Admin\AuthController@login');
    Route::get('/logout', 'Admin\AuthController@logout');

    Route::middleware(['auth.admin'])->group(function() {

        Route::get('/', function(){
            return view('admin.dashboard');
        });

        Route::post('/master/airport', 'Admin\MasterController@getAirport');
        Route::post('/master/categorybyairport', 'Admin\MasterController@getCategoryByAirport');
        Route::post('/master/databyairport', 'Admin\MasterController@getDataByAirportCode');

        Route::middleware(['auth.admin.superadmin'])->group(function() {

            //USER
            Route::get('/user', 'Admin\UserController@index');
            Route::get('/user/create', 'Admin\UserController@create');
            Route::post('/user/create', 'Admin\UserController@store');
            Route::get('/user/edit', 'Admin\UserController@edit');
            Route::post('/user/edit', 'Admin\UserController@update');
            Route::get('/user/delete/{id}', 'Admin\UserController@destroy');

            //LOG
            Route::get('/log', 'Admin\LogController@index');

            //TRASHED
            Route::get('/airport/trash', 'Admin\AirportController@showTrashed');
            Route::get('/icon/trash', 'Admin\IconController@showTrashed');
            Route::post('/icon/trash', 'Admin\IconController@getTrashedDataIcon');
            Route::get('/floor/trash', 'Admin\FloorController@showTrashed');
            Route::post('/floor/trash', 'Admin\FloorController@getTrashedDataFloor');
            Route::get('/category/trash', 'Admin\CategoryController@showTrashed');
            Route::post('/category/trash', 'Admin\CategoryController@getTrashedDataCategory');
            Route::get('/area/trash', 'Admin\AreaController@showTrashed');
            Route::post('/area/trash', 'Admin\AreaController@getTrashedDataArea');
            Route::get('/priority/trash', 'Admin\PriorityController@showTrashed');
            Route::post('/priority/trash', 'Admin\PriorityController@getTrashedDataPriority');

            //RESTORE TRASHED
            Route::get('/airport/restore/{airportcode}', 'Admin\AirportController@restore');
            Route::get('/icon/restore/{id}', 'Admin\IconController@restore');
            Route::get('/floor/restore/{id}', 'Admin\FloorController@restore');
            Route::get('/category/restore/{id}', 'Admin\CategoryController@restore');
            Route::get('/area/restore/{id}', 'Admin\AreaController@restore');
            Route::get('/priority/restore/{id}', 'Admin\PriorityController@restore');

            //FORCE DELETE
            Route::get('/airport/forcedelete/{airportcode}', 'Admin\AirportController@destroyPermanent');
            Route::get('/icon/forcedelete/{id}', 'Admin\IconController@destroyPermanent');
            Route::get('/floor/forcedelete/{id}', 'Admin\FloorController@destroyPermanent');
            Route::get('/category/forcedelete/{id}', 'Admin\CategoryController@destroyPermanent');
            Route::get('/area/forcedelete/{id}', 'Admin\AreaController@destroyPermanent');
            Route::get('/priority/forcedelete/{id}', 'Admin\PriorityController@destroyPermanent');

        });

        Route::middleware(['auth.admin.adminicon'])->group(function() {

            //CATEGORY
            Route::get('/category', 'Admin\CategoryController@index');
            Route::get('/category/create', 'Admin\CategoryController@create');
            Route::post('/category/create', 'Admin\CategoryController@store');
            Route::get('/category/edit', 'Admin\CategoryController@edit');
            Route::post('/category/edit', 'Admin\CategoryController@update');
            Route::get('/category/delete/{id}', 'Admin\CategoryController@destroy');
            Route::post('/category/getdatacategory', 'Admin\CategoryController@getDataCategory');

            //AIRPORT
            Route::get('/airport', 'Admin\AirportController@index');
            Route::get('/airport/create', 'Admin\AirportController@create');
            Route::post('/airport/create', 'Admin\AirportController@store');
            Route::get('/airport/edit', 'Admin\AirportController@edit');
            Route::post('/airport/edit', 'Admin\AirportController@update');
            Route::get('/airport/delete/{airportcode}', 'Admin\AirportController@destroy');
            Route::post('/poi/getdataairport', 'Admin\AirportController@getDataAirport');

            //PRIORITY
            Route::get('/priority', 'Admin\PriorityController@index');
            Route::get('/priority/create', 'Admin\PriorityController@create');
            Route::post('/priority/create', 'Admin\PriorityController@store');
            Route::get('/priority/edit', 'Admin\PriorityController@edit');
            Route::post('/priority/edit', 'Admin\PriorityController@update');
            Route::get('/priority/delete/{id}', 'Admin\PriorityController@destroy');
            Route::post('/priority/getdatapriority', 'Admin\PriorityController@getDataPriority');

            //FLOOR
            Route::get('/floor', 'Admin\FloorController@index');
            Route::get('/floor/create', 'Admin\FloorController@create');
            Route::post('/floor/create', 'Admin\FloorController@store');
            Route::get('/floor/edit', 'Admin\FloorController@edit');
            Route::post('/floor/edit', 'Admin\FloorController@update');
            Route::get('/floor/delete/{id}', 'Admin\FloorController@destroy');
            Route::post('/floor/getdatafloor', 'Admin\FloorController@getDataFloor');

            //ICON
            Route::get('/icon', 'Admin\IconController@index');
            Route::get('/icon/create', 'Admin\IconController@create');
            Route::post('/icon/create', 'Admin\IconController@store');
            Route::get('/icon/edit', 'Admin\IconController@edit');
            Route::post('/icon/edit', 'Admin\IconController@update');
            Route::get('/icon/delete/{id}', 'Admin\IconController@destroy');
            Route::post('/icon/getdataicon', 'Admin\IconController@getDataIcon');

            //AREA
            Route::get('/area', 'Admin\AreaController@index');
            Route::get('/area/create', 'Admin\AreaController@create');
            Route::post('/area/create', 'Admin\AreaController@store');
            Route::get('/area/edit', 'Admin\AreaController@edit');
            Route::post('/area/edit', 'Admin\AreaController@update');
            Route::get('/area/delete/{id}', 'Admin\AreaController@destroy');
            Route::post('/area/getdataarea', 'Admin\AreaController@getDataArea');



        });

        Route::middleware(['auth.admin.adminpoi'])->group(function() {

            //POI
            Route::get('/poi', 'Admin\PoiController@index');
            Route::get('/poi/create', 'Admin\PoiController@create');
            Route::post('/poi/create', 'Admin\PoiController@store');
            Route::get('/poi/edit', 'Admin\PoiController@edit');
            Route::post('/poi/edit', 'Admin\PoiController@update');
            Route::post('/poi/delete', 'Admin\PoiController@destroy');
            Route::post('/poi/getdatapoi', 'Admin\PoiController@getDataPoi');

        });

        Route::middleware(['auth.admin.adminpromo'])->group(function() {

            //PROMO
            Route::get('/promo', 'Admin\PromoController@index');
            Route::get('/promo/create', 'Admin\PromoController@create');
            Route::post('/promo/create', 'Admin\PromoController@store');
            Route::get('/promo/edit', 'Admin\PromoController@edit');
            Route::post('/promo/edit', 'Admin\PromoController@update');
            Route::post('/promo/delete', 'Admin\PromoController@destroy');
            Route::post('/promo/getdatapromo', 'Admin\PromoController@getDataPromo');
        });


    });

});
